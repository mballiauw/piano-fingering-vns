/*
 * Move3.cpp
 *
 *  Created on: 30-nov.-2013
 *      Author: Matteo
 */

#include "Move3.h"
#include "Move.h"
#include "score.h"
#include <vector>
#include <cstdlib>
#include <time.h>
#include <algorithm>
#include <iostream>
#include <sys/time.h>

using namespace std;

/*
//sorting function quicksort
int compareQS3 (const void * a, const void * b) {
	if ( (*(Move3*)a).diff <  (*(Move3*)b).diff) {return -1;}
	else if ( (*(Move3*)a).diff == (*(Move3*)b).diff  ) {return 0;}
	//else if ( (*(Move3*)a).diff >  (*(Move3*)b).diff ) {return 1;}
	else {return 1;}
}
*/



// note not starting earlier or ending later
bool exclusiveNote (int nootid, int begin, int end /*einde inclusief*/, vector<note> &allnotes){
	int start = allnotes.at(nootid-1).start;
	int duration = allnotes.at(nootid-1).duration;
	if (start >= begin && (start+duration-1) <= end){
		return true;
	}
	else {return false;}
}


//function to find first note in allnotes that is exclusively in part
int findFirst (char hand, int begin, int end, vector<note> &allnotes){
	int first = 0;
	for (unsigned int a = 0; a < allnotes.size(); a++){
		if (exclusiveNote((a+1), begin, end, allnotes) == 1 && allnotes.at(a).hand == hand){
			first = (a+1);
			break;
		}
	}
	return first; // geeft ID (uit allnotes) van eerste noot die voldoet
}

//function to find last note in allnotes that is exclusively in part
int findLast (char hand, int begin, int end, vector<note> &allnotes){
	int last = 0;
	for (unsigned int a = 0; a < allnotes.size(); a++){
		if (exclusiveNote((a+1), begin, end, allnotes) == 1 && allnotes.at(a).hand == hand){
			last = (a+1);
		}
	}
	return last; // geeft ID (uit allnotes) van laatste noot die voldoet
}

double rightPerturbPart (int begin, int end, vector<note> &allnotes, MusicInfo &info){ // begin & end: slicenr's

	/*Random seed initialization - DO NOT DELETE.*/
	timeval t1;
	gettimeofday(&t1, NULL);
	srand(t1.tv_usec * t1.tv_sec);

	vector <int> fixfingerslice; // per slice active fingering from non-exclusive notes
	vector <vector <int> > fixfinger; // contains previous info for all slices (begin to end)

	fixfingerslice.clear();
	fixfinger.clear();

	// set all exclusive notes to 0
	for (int counter = begin; counter <= end; counter ++){
		for (int a = 0; a <5; a++){
			if (info.right.at(counter).at(a)!=0 && exclusiveNote (info.right.at(counter).at(a), begin, end, allnotes) == true){
				allnotes.at(info.right.at(counter).at(a)-1).fingering = 0;
			}
		}
	}

	// collect for each slice the used fingers by non-excl notes
	for (int counter = begin; counter <= end; counter ++){
		for (int a = 0; a <5; a++){
			if (info.right.at(counter).at(a)!=0 && exclusiveNote (info.right.at(counter).at(a), begin, end, allnotes) == false){
				fixfingerslice.push_back(allnotes.at(info.right.at(counter).at(a)-1).fingering); // put used fingering (from non-excl) in vector
			}
		}
		// after a slice: put the slice in the matrix and clear the vector
		fixfinger.push_back(fixfingerslice);
		fixfingerslice.clear();
	}

	// assign to excl. notes a random finger (except those used by non-excl notes and other already in use in the slice)
	for (int counter = begin; counter <= end; counter ++){
		for (int a = 0; a <5; a++){
			// exclusive note (enkel als fingering = 0 en geen id 0), not a 0 and fingering = 0
			if (info.right.at(counter).at(a)!=0 /*&& exclusiveNote (info.right[counter][a], begin, end, allnotes) == true*/ && allnotes.at(info.right.at(counter).at(a)-1).fingering == 0){
				int duur = allnotes.at(info.right.at(counter).at(a)-1).duration; // duration of the note => now the number of active slices
				bool usedfingers[5] = {false,false,false,false,false};
				// get all used fingers in these active slices so far
				for (int slicecounter = (counter-begin); slicecounter < (counter-begin+duur); slicecounter++){
					int ffsize = fixfinger.at(slicecounter).size();
					for (int inslco = 0; inslco < ffsize; inslco++){
						usedfingers[fixfinger.at(slicecounter).at(inslco)-1] = true; // e.g. finger 1 stores to usedfingers[0]
					}
				}
				//count number of false in array of fingers
				int numberfree = count(usedfingers, usedfingers+5, false);

				int randomfinger; // de nieuwe fingering

				// generate random number that selects one from the false (= unused) fingers
				//srand(time(NULL));
				int rnr = rand() %numberfree;
				int control = 0;
				for (int arcount=0;arcount<5;arcount++){
					if(usedfingers[arcount]==false && control == rnr){
						randomfinger = (arcount+1);
						break;
					}
					else if(usedfingers[arcount]==false && control != rnr){
						control++;
					}
					else {
						continue;
					}
				}

				//assign finger
				allnotes.at(info.right.at(counter).at(a)-1).fingering = randomfinger;

				//add this new finger to the active slices of the note
				for (int slicecounter = (counter-begin); slicecounter < (counter-begin+duur); slicecounter++){
					fixfinger.at(slicecounter).push_back(randomfinger);
				}
			}
		}
	}

	return rightScore (info, allnotes);

}

double leftPerturbPart (int begin, int end, vector<note> &allnotes, MusicInfo &info){ // begin & end: slicenr's

	/*Random seed initialization - DO NOT DELETE.*/
	timeval t1;
	gettimeofday(&t1, NULL);
	srand(t1.tv_usec * t1.tv_sec);

	vector <int> fixfingerslice; // per slice active fingering from non-exclusive notes
	vector <vector <int> > fixfinger; // contains previous info for all slices (begin to end)

	fixfingerslice.clear();
	fixfinger.clear();

	// set all exclusive notes to 0
	for (int counter = begin; counter <= end; counter ++){
		for (int a = 0; a <5; a++){
			if (info.left.at(counter).at(a)!=0 && exclusiveNote (info.left.at(counter).at(a), begin, end, allnotes) == true){
				allnotes.at(info.left.at(counter).at(a)-1).fingering = 0;
			}
		}
	}

	// collect for each slice the used fingers by non-excl notes
	for (int counter = begin; counter <= end; counter ++){
		for (int a = 0; a <5; a++){
			if (info.left.at(counter).at(a)!=0 && exclusiveNote (info.left.at(counter).at(a), begin, end, allnotes) == false){
				fixfingerslice.push_back(allnotes.at(info.left.at(counter).at(a)-1).fingering); // put used fingering (from non-excl) in vector
			}
		}
		// after a slice: put the slice in the matrix and clear the vector
		fixfinger.push_back(fixfingerslice);
		fixfingerslice.clear();
	}

	// assign to excl. notes a random finger (except those used by non-excl notes and other in the slice)
	for (int counter = begin; counter <= end; counter ++){
		for (int a = 0; a <5; a++){
			// exclusive note (enkel als fingering = 0 en geen id 0), not a 0 and fingering = 0
			if (info.left.at(counter).at(a)!=0 /*&& exclusiveNote (info.left[counter][a], begin, end, allnotes) == true*/ && allnotes.at(info.left.at(counter).at(a)-1).fingering == 0){
				int duur = allnotes.at(info.left.at(counter).at(a)-1).duration; // duration of the note => now the number of active slices
				bool usedfingers[5] = {false,false,false,false,false};
				// get all used fingers in these active slices so far
				for (int slicecounter = (counter-begin); slicecounter < (counter-begin+duur); slicecounter++){
					int ffsize = fixfinger.at(slicecounter).size();
					for (int inslco = 0; inslco < ffsize; inslco++){
						usedfingers[fixfinger.at(slicecounter).at(inslco)-1] = true; // finger 1 stores to usedfingers[0]
					}
				}
				//count number of false in array of fingers
				int numberfree = count(usedfingers, usedfingers+5, false);

				int randomfinger; // de nieuwe fingering

				// generate random number that selects one from the false (= unused) fingers
				//srand(time(NULL));
				int rnr = rand() %numberfree;
				int control = 0;
				for (int arcount=0;arcount<5;arcount++){
					if(usedfingers[arcount]==false && control == rnr){
						randomfinger = (arcount+1);
						break;
					}
					else if(usedfingers[arcount]==false && control != rnr){
						control++;
					}
					else {
						continue;
					}
				}

				//assign finger
				allnotes.at(info.left.at(counter).at(a)-1).fingering = randomfinger;

				//add this new finger to the active slices of the note
				for (int slicecounter = (counter-begin); slicecounter < (counter-begin+duur); slicecounter++){
					fixfinger.at(slicecounter).push_back(randomfinger);
				}
			}
		}
	}

	return leftScore (info, allnotes);

}

/*
//create function that can be looped inside itself. => go through all relevant notes and change (boomstructuur)
int changeNoteSet (int begin, int end, int first, int last, char hand, vector<note> &allnotes, MusicInfo &info, vector <M3Note> &changes, Move3 &swapmeasurebest){

	M3Note m3note; // change within 1 element of NB.

	if (first != 0 && last != 0){

		for (int f = 1; f < 6; f++){

			//store info of change in m3note
			m3note.finger = f;
			m3note.id = first;
			changes.push_back(m3note);

			for (int next = first + 1 ; next <= last; next++){ // <= throuws std::bad_alloc
				if (exclusiveNote(next, begin, end, allnotes) == 1 && allnotes[(next-1)].hand == hand){
					changeNoteSet (begin, end, next, last, hand, allnotes, info, changes, swapmeasurebest);
					if (next == last){
						vector <note> adaptnotes = allnotes; // kopie allnotes maken
						int diff;
						//Move3 swapmeasure; // elementen van NB

						// execute changes on adaptnotes
						if (changes.size()!=0){  // check niet leeg
							for (unsigned int counter = 0; counter < changes.size(); counter++){
								adaptnotes[(changes[counter].id-1)].fingering = changes[counter].finger;
							}
						}

						if (hand == 'R' && rightFeasibility(info, adaptnotes) == 1){
							int start = 0;
							int prevlength;
							int endprt = 0;
							int nextlength;
							for (int backskipper = begin-1; backskipper > 0; backskipper--){
								if (info.right[begin][0] != info.right[backskipper][0] || info.right[begin][1] != info.right[backskipper][1] ||
									info.right[begin][2] != info.right[backskipper][2] || info.right[begin][3] != info.right[backskipper][3] || info.right[begin][4] != info.right[backskipper][4]){
									prevlength = maximum(allnotes[(info.right[backskipper][0]-1)].duration, allnotes[(info.right[backskipper][1]-1)].duration, allnotes[(info.right[backskipper][2]-1)].duration, allnotes[(info.right[backskipper][3]-1)].duration, allnotes[(info.right[backskipper][4]-1)].duration);
									break;
								}
								else {continue;}
							}

							for (unsigned int skipper = end+1; skipper < info.right.size(); skipper++){
								if (info.right[end][0] != info.right[skipper][0] || info.right[end][1] != info.right[skipper][1] ||
									info.right[end][2] != info.right[skipper][2] || info.right[end][3] != info.right[skipper][3] || info.right[end][4] != info.right[skipper][4]){
									nextlength = maximum(allnotes[(info.right[skipper][0]-1)].duration, allnotes[(info.right[skipper][1]-1)].duration, allnotes[(info.right[skipper][2]-1)].duration, allnotes[(info.right[skipper][3]-1)].duration, allnotes[(info.right[skipper][4]-1)].duration);
									break;
								}
								else {continue;}
							}

							int start_vwd = begin - prevlength - 1; // start die negatief kan worden
							start = max (0, start_vwd); // beperk tot 0
							int end_vwd = end + nextlength + 1; // einde die voorbij einde kan gaan
							int sizeofright = info.right.size(); endprt = min(sizeofright , end_vwd);  // beperkt tot einde

							diff =  rightScoreP(info, adaptnotes, start, endprt) - rightScoreP(info, allnotes, start, endprt); //bereken moeilijkheid rechts (deel)
						}

						else if (hand == 'L' && leftFeasibility(info, adaptnotes) == 1){
							int start = 0;
							int prevlength;
							int endprt = 0;
							int nextlength;
							for (int backskipper = begin-1; backskipper > 0; backskipper--){
								if (info.left[begin][0] != info.left[backskipper][0] || info.left[begin][1] != info.left[backskipper][1] ||
									info.left[begin][2] != info.left[backskipper][2] || info.left[begin][3] != info.left[backskipper][3] || info.left[begin][4] != info.left[backskipper][4]){
									prevlength = maximum(allnotes[(info.left[backskipper][0]-1)].duration, allnotes[(info.left[backskipper][1]-1)].duration, allnotes[(info.left[backskipper][2]-1)].duration, allnotes[(info.left[backskipper][3]-1)].duration, allnotes[(info.left[backskipper][4]-1)].duration);
									break;
								}
								else {continue;}
							}

							for (unsigned int skipper = end+1; skipper < info.left.size(); skipper++){
								if (info.left[end][0] != info.left[skipper][0] || info.left[end][1] != info.left[skipper][1] ||
									info.left[end][2] != info.left[skipper][2] || info.left[end][3] != info.left[skipper][3] || info.left[end][4] != info.left[skipper][4]){
									nextlength = maximum(allnotes[(info.left[skipper][0]-1)].duration, allnotes[(info.left[skipper][1]-1)].duration, allnotes[(info.left[skipper][2]-1)].duration, allnotes[(info.left[skipper][3]-1)].duration, allnotes[(info.left[skipper][4]-1)].duration);
									break;
								}
								else {continue;}
							}

							int start_vwd = begin - prevlength - 1; // start die negatief kan worden
							start = max (0, start_vwd); // beperk tot 0
							int end_vwd = end + nextlength + 1; // einde die voorbij einde kan gaan
							int sizeofleft = info.left.size(); endprt = min(sizeofleft , end_vwd);  // beperkt tot einde

							diff =  leftScoreP(info, adaptnotes, start, endprt) - leftScoreP(info, allnotes, start, endprt); //bereken moeilijkheid links (deel)
						}
						if (diff < 0 && diff < swapmeasurebest.diff) {
							swapmeasurebest.diff = diff;
							swapmeasurebest.changes = changes;
							// break; //=> first improving (als je 2e vwd verwijdert)
						} // enkel verbetering
					}
					changes.pop_back();
				}
				//else {continue;}
			}
		}
	}

	return 0;
}

double rightSwapMeasure (int begin, int end, vector<note> &allnotes, MusicInfo &info){ // begin & end: slicenr's

	//vector<Move3> NBswapmeasure; // contains neighbourhood
	Move3 swapmeasurebest; // contains best el of neighbourhood
	char hand = 'R';

	int first = findFirst (hand, begin, end, allnotes);
	int last = findLast (hand, begin, end, allnotes);


	vector <M3Note> changes;
	changes.clear();

	//NBswapmeasure = changeNoteSet(first, last, hand, allnotes, begin, end, info);
	changeNoteSet(begin, end, first, last, hand, allnotes, info, changes, swapmeasurebest); // swapmeasurebest (beste el in NB)

	// sorteer neighbourhood: laagste diff eerst.
	//qsort(&NBswapmeasure[0], NBswapmeasure.size(), sizeof(Move3), compareQS3);


	// select first value (is lowest difficulty) and execute changes
	if (swapmeasurebest.changes.size()!=0 && swapmeasurebest.diff < 0){  // check niet leeg + beter
		for (unsigned int counter = 0; counter < swapmeasurebest.changes.size(); counter++){
			allnotes[(swapmeasurebest.changes[counter].id-1)].fingering = swapmeasurebest.changes[counter].finger;
		}
	}


	return rightScore (info, allnotes);

}

double leftSwapMeasure (int begin, int end, vector<note> &allnotes, MusicInfo &info){

	//vector<Move3> NBswapmeasure; // contains neighbourhood
	Move3 swapmeasurebest; // contains best el of neighbourhood
	char hand = 'L';

	int first = findFirst (hand, begin, end, allnotes);
	int last = findLast (hand, begin, end, allnotes);

	vector <M3Note> changes;
	changes.clear();

	//NBswapmeasure = changeNoteSet(first, last, hand, allnotes, begin, end, info);
	changeNoteSet(begin, end, first, last, hand, allnotes, info, changes, swapmeasurebest); // swapmeasurebest (beste el in NB)

	// sorteer neighbourhood: laagste diff eerst.
	//qsort(&NBswapmeasure[0], NBswapmeasure.size(), sizeof(Move3), compareQS3);


	// select first value (is lowest difficulty) and execute changes
	if (swapmeasurebest.changes.size()!=0){  // check niet leeg
		for (unsigned int counter = 0; counter < swapmeasurebest.changes.size(); counter++){
			allnotes[(swapmeasurebest.changes[counter].id-1)].fingering = swapmeasurebest.changes[counter].finger;
		}
	}


	return leftScore (info, allnotes);

}
*/
