#ifdef WIN32
#define _CRT_SECURE_NO_DEPRECATE
#endif
#include <string.h>
#include "xmlParser.h"
#include "note.h"
#include <vector>
#include <string>
#include <iostream>


using namespace std;


XMLNode importNode(string filename){

	XMLNode NodeMain = XMLNode::openFileHelper(filename.c_str(),
			"score-partwise");
	return NodeMain;
}


vector<note> importMusicXML(XMLNode &NodeMain) {
	//later filename mee doorgeven

	// vector<string> header;
	vector<note> part;
	note n(0, 0, 0, 0, '0', 0, 0);
	int count, i, j, id2;
	id2 = 1;
	int used;

	/* //store header info in a vector
	 header.clear();
	 */

	//open and parse the XML file


	/* //get:
	 //work -> work-title
	 header.push_back(NodeMain.getChildNode("work").getChildNode("work-title").getText());

	 //identification -> creator (type: composer)
	 XMLNode NodeWork=NodeMain.getChildNode("work");
	 //later: credits
	 //part-list -> not necessary to know right now */

	//attributes

//zoek pianopart
	string piano;
	int instraantal = NodeMain.getChildNode("part-list").nChildNode("score-part");
	int inr;
	if (instraantal == 1){inr = 0;
		piano = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id");}
	else {
		for (inr = 0; inr < instraantal; inr++) {
		string instrnaam = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getChildNode("part-name").getText();
		if ( instrnaam == "Piano" ) {
			// cout << NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id"); //oke
			piano = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id");
		}
		}
	}

	// 1 instrument => gebruik deze. Anders: zoek piano-deel

	int pt;

	// enkel in piano deel werken
	for (pt = 0; pt < instraantal; pt++) {
		string instrumentpart = NodeMain.getChildNode("part", pt).getAttribute("id");
		if ( instrumentpart == piano) {

			//count number of measures

			i = NodeMain.getChildNode("part", pt).nChildNode("measure");
			//out << "i: " << i;

			/*else{
			 cout << "This is not a piano score only";
			 //return 0;
			 }*/

			char m;
			string step;
			int pitch2 = 1;
			int duration2 = 1;
			int start2 = 0;
			int start3 = 0;
			char hand2 = 'R';
			int voice2 = 1;
			int rvoice=1;

			part.clear();
			int div4 = atoi(NodeMain.getChildNode("part", pt).getChildNode(
									"measure", 0).getChildNode("attributes").getChildNode(
									"divisions").getText()); // hoeveel per 4e noot
			int teller = atoi(NodeMain.getChildNode("part", pt).getChildNode(
												"measure", 0).getChildNode("attributes").getChildNode(
												"time").getChildNode("beats").getText());
			int noemer = atoi(NodeMain.getChildNode("part", pt).getChildNode(
												"measure", 0).getChildNode("attributes").getChildNode(
												"time").getChildNode("beat-type").getText());
			int divs = (div4 * 4 * teller) / noemer; // hoeveel divisies per maat


			for (j = 0; j < i; j++) {


				// aantal noten per maat
				count =
						NodeMain.getChildNode("part", pt).getChildNode("measure", j).nChildNode(
								"note");


				//for each measure, put all notes in a vector
				for (int k = 0; k < count; k++) {

					if (NodeMain.getChildNode("part", pt).getChildNode("measure", j).getChildNode("note", k).nChildNode("rest") == 0
					//if (NodeMain.getChildNode("part", pt).getChildNode("measure", j).getChildNode("note", k).getAttribute("print-object") == "no") {continue;} // deze wordt if, andere: else if
					){
// enkel noten tellen
					XMLNode NodeNote =
							NodeMain.getChildNode("part", pt).getChildNode(
									"measure", j).getChildNode("note", k).getChildNode(
									"pitch");

					//add the note to a simple vector

					//voice
					voice2 = atoi(NodeMain.getChildNode("part", pt).getChildNode("measure", j).getChildNode("note", k).getChildNode("voice").getText());


					//pitch

					step = NodeNote.getChildNode("step").getText();
					m = step[0];

					switch (m) {

					case 'A':
						pitch2 = 10;
						break;
					case 'B':
						pitch2 = 12;
						break;
					case 'C':
						pitch2 = 1;
						break;
					case 'D':
						pitch2 = 3;
						break;
					case 'E':
						pitch2 = 5;
						break;
					case 'F':
						pitch2 = 6;
						break;
					case 'G':
						pitch2 = 8;
						break;
					}

					//adjust for sharps and flats

					if (NodeNote.nChildNode("alter") >= 1) {
						pitch2 = pitch2	+ atoi(NodeNote.getChildNode("alter").getText());
					}

					//modulus 12 naar 14

					if (pitch2 >= 6 && pitch2 <= 12)
						pitch2++; // after E: one tone higher
					else if (pitch2 < 1)
						pitch2--; // before C: one tone lower
					else if (pitch2 > 12)
						pitch2 = pitch2 + 2; // after B: two tones higher

					//octave

					pitch2 = pitch2 + (14* (atoi(NodeNote.getChildNode("octave").getText())));

					//duration

					duration2 = atoi(NodeMain.getChildNode("part", pt).getChildNode("measure", j).getChildNode("note", k).getChildNode("duration").getText());

					//start

					//akkoord
					if (NodeMain.getChildNode("part", pt).getChildNode("measure", j).getChildNode("note", k).nChildNode("chord")>0){start2 = start3;}//akkoord neemt vorige duur over

					else {
					int previous = 0;
					if (k > 0) {
						 for (int l = 0; l < k; l++) {
							if (NodeMain.getChildNode("part", pt).getChildNode("measure", j).getChildNode("note", l).nChildNode("chord")==0){
							 previous =
									previous
											+ atoi(
													NodeMain.getChildNode(
															"part", pt).getChildNode(
															"measure", j).getChildNode(
															"note", l).getChildNode(
															"duration").getText());}
							else {previous = previous + 0;}
						 }
						}


					start2 = j * divs + previous - (voice2 - 1)*divs; // j = measure number // divs = number of divisions, check measure 1 // previous = aantal noten ervoor
					// corrigeer per voice (per voice: previous opnieuw op 0 zetten)


					if(voice2 < 5) {rvoice = min(4,max(used,voice2));}
					else {
					start2 = start2 + (4-rvoice)*divs; // correctie indien niet alle voices tussen 1 en 4 zijn gebruikt.
					}


					}

					// hand bepalen
					if (atoi(
							NodeMain.getChildNode("part", pt).getChildNode(
									"measure", j).getChildNode("note", k).getChildNode(
									"staff").getText()) == 1)
						hand2 = 'R';
					else
						hand2 = 'L';

					//cout << pitch2 << ", " << duration2 << ", " << start2 << ", " << id2 << ", " << hand2 << ", " << voice2 << "\n";


					n.pitch = pitch2;
					n.duration = duration2;
					n.start = start2;
					// n.beat = 1;
					n.id = id2;
					//n.tied = 0; // nodig om te verbinden ?
					n.hand = hand2;
					n.voice = voice2;
					part.push_back(n);
					used = voice2;
					start3 = start2;
					id2++; // rusten krijgen geen volgnummer

				}
					else {voice2 = atoi(NodeMain.getChildNode("part", pt).getChildNode("measure", j).getChildNode("note", k).getChildNode("voice").getText());
					used = voice2;
					if(voice2 < 5) {rvoice = min(4,max(used,voice2));}}
			} }
		}
	}

	NodeMain = XMLNode::emptyNode(); // Make NodeMain empty again during program

	return part;
}

int aantalmaten (string filename) {
	XMLNode NodeMain = XMLNode::openFileHelper(filename.c_str(),
				"score-partwise");

	int maten = 0;

	//zoek pianopart
		string piano;
		int instraantal = NodeMain.getChildNode("part-list").nChildNode(
				"score-part");
		int inr;
		if (instraantal == 1){inr = 0;
			piano = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id");}
		else { for (inr = 0; inr < instraantal; inr++) {
			string instrnaam = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getChildNode("part-name").getText();
			if ( instrnaam == "Piano" ) {
				// cout << NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id"); //oke
				piano = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id");
			}
			else if (NodeMain.getChildNode("part-list").nChildNode("score-part")==1){piano = NodeMain.getChildNode("part-list").getChildNode("score-part").getAttribute("id");}
		} }
		// 1 instrument => gebruik deze. Anders: zoek piano-deel

		int pt;

		// enkel in piano deel werken
		for (pt = 0; pt < instraantal; pt++) {
			string instrumentpart = NodeMain.getChildNode("part", pt).getAttribute("id");
			if ( instrumentpart == piano) {

				//count number of measures

				maten = NodeMain.getChildNode("part", pt).nChildNode("measure");
				}
		}
	return maten;

}

int divpermaat (string filename) {
	XMLNode NodeMain = XMLNode::openFileHelper(filename.c_str(),
				"score-partwise");

	int divs = 0;

	//zoek pianopart
		string piano;
		int instraantal = NodeMain.getChildNode("part-list").nChildNode(
				"score-part");
		int inr;
		if (instraantal == 1){inr = 0;
			piano = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id");}
		else { for (inr = 0; inr < instraantal; inr++) {
			string instrnaam = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getChildNode("part-name").getText();
			if ( instrnaam == "Piano" ) {
				// cout << NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id"); //oke
				piano = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id");
			}
			else if (NodeMain.getChildNode("part-list").nChildNode("score-part")==1){piano = NodeMain.getChildNode("part-list").getChildNode("score-part").getAttribute("id");}
		} }
		// 1 instrument => gebruik deze. Anders: zoek piano-deel

		int pt;

		// enkel in piano deel werken
		for (pt = 0; pt < instraantal; pt++) {
			string instrumentpart = NodeMain.getChildNode("part", pt).getAttribute("id");
			if ( instrumentpart == piano) {

				//count number of divisions

				int div4 = atoi(NodeMain.getChildNode("part", pt).getChildNode(
										"measure", 0).getChildNode("attributes").getChildNode(
										"divisions").getText()); // hoeveel per 4e noot
				int teller = atoi(NodeMain.getChildNode("part", pt).getChildNode(
													"measure", 0).getChildNode("attributes").getChildNode(
													"time").getChildNode("beats").getText());
				int noemer = atoi(NodeMain.getChildNode("part", pt).getChildNode(
													"measure", 0).getChildNode("attributes").getChildNode(
													"time").getChildNode("beat-type").getText());
				divs = (div4 * 4 * teller) / noemer; // hoeveel divisies per maat
				}
		}
	return divs;

}

int testOnemeasuretype (string filename) {
	XMLNode NodeMain = XMLNode::openFileHelper(filename.c_str(),
				"score-partwise");

	int result = 0;

	//zoek pianopart
			string piano;
			int instraantal = NodeMain.getChildNode("part-list").nChildNode(
					"score-part");
			int inr;
			if (instraantal == 1){inr = 0;
				piano = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id");}
			else { for (inr = 0; inr < instraantal; inr++) {
				string instrnaam = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getChildNode("part-name").getText();
				if ( instrnaam == "Piano" ) {
					// cout << NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id"); //oke
					piano = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id");
				}
				else if (NodeMain.getChildNode("part-list").nChildNode("score-part")==1){piano = NodeMain.getChildNode("part-list").getChildNode("score-part").getAttribute("id");}
			} }
			// 1 instrument => gebruik deze. Anders: zoek piano-deel

			int pt;

			// enkel in piano deel werken
			for (pt = 0; pt < instraantal; pt++) {
				string instrumentpart = NodeMain.getChildNode("part", pt).getAttribute("id");
				if ( instrumentpart == piano) {
					//count number of measures

					int i = NodeMain.getChildNode("part", pt).nChildNode("measure");

					for (int j = 0; j < i; j++) {

						// aantal attribute veranderingen ... NIET CORRECT!!!
						if (NodeMain.getChildNode("part", pt).getChildNode("measure", j).nChildNode("attributes")>0)
							{result++;}
					}
				}
			}
			return result;
}
