/*
 * Move3.h
 *
 *  Created on: 30-nov.-2013
 *      Author: Matteo
 */

#ifndef MOVE3_H_
#define MOVE3_H_

#include "score.h"
#include <vector>
#include "MusicInfo.h"

using namespace std;

/*
class M3Change {
public:
	int id;
	int finger;
};*/

/*
class Move3 {
public:
	vector <M3Note> changes;
	signed int diff;
};*/

//int compareQS3 (const void * a, const void * b);

bool exclusiveNote (int nootid, int begin, int end, vector<note> &allnotes);
int findFirst (char hand, int begin, int end, vector<note> &allnotes);
int findLast (char hand, int begin, int end, vector<note> &allnotes);
//Move3 changeNoteSet (int first, int last, char hand, vector<note> &allnotes, int begin, int end, MusicInfo &info);
//int changeNoteSet (int begin, int end, int first, int last, char hand, vector<note> &allnotes, MusicInfo &info, vector <M3Note> &changes, Move3 &swapmeasurebest);
double rightPerturbPart (int begin, int end, vector<note> &allnotes, MusicInfo &info);
double leftPerturbPart (int begin, int end, vector<note> &allnotes, MusicInfo &info);

#endif /* MOVE3_H_ */
