/*
 * Move5.h
 *
 *  Created on: 24-feb.-2014
 *      Author: Matteo
 */

#ifndef MOVE5_H_
#define MOVE5_H_

#include <vector>
#include "MusicInfo.h"
#include "score.h"
#include <algorithm>

using namespace std;

class Move5 {

public:
	int id1;
	int id2;
	int finger1;
	int finger2;
	double diff;
	Move5 (int,int,int,int,double);
};


double rightChangetwo(MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure);
double leftChangetwo(MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure);



#endif /* MOVE5_H_ */
