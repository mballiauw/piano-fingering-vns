for typeofpert in 0 1 2; do
	for nrofperturbations in 0 1 5 10 20; do
		for nbh_change in 0 1; do
			for nbh_swap in 0 1; do
				for nbh_spart in 0 1; do
					for nrofpartspert in 1 2 4 8; do
						for percentagepert in 5 10 20 50 100; do
							for tabupercent in 0 5 10 20; do
								for firstdescent in 0 1; do
									#run
./apf -typeofpert $typeofpert -nrofperturbations $nrofperturbations -nbh_change $nbh_change -nbh_swap $nbh_swap -nbh_spart $nbh_spart -nrofpartspert $nrofpartspert -percentagepert $percentagepert -tabupercent $tabupercent -tabupermove $tabupermove -firstdescent $firstdescent -bestand sonate.xml -experimentmode 1

								done
							done
						done
					done
				done
			done
		done
	done
done