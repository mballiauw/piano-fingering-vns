/*
 * Move4.h
 *
 *  Created on: 10-dec.-2013
 *      Author: Matteo
 */

#ifndef MOVE4_H_
#define MOVE4_H_

#include <vector>
#include "MusicInfo.h"
#include "score.h"
#include <algorithm>

using namespace std;

class Move4 {
public:
	int ID1;
	int f;
	vector <int> ID2s;
	int g;
	double diff;
	Move4 (int, int, vector<int>, int, double);
};

// steepest
double rightSwapNote(MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure);
double leftSwapNote(MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure);



#endif /* MOVE4_H_ */
