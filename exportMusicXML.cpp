#ifdef WIN32
#define _CRT_SECURE_NO_DEPRECATE
#endif

#include <string.h>
#include "xmlParser.h"
#include "note.h"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sstream>
#include <stdlib.h>



using namespace std;

void exportMusicXML(vector<note> &allnotes, XMLNode &NodeMain, string &bestand) {

	int noteid = 0;

	//zoek pianopart
	string piano;
	int instraantal = NodeMain.getChildNode("part-list").nChildNode("score-part");
	int inr;
	if (instraantal == 1){
		inr = 0;
		piano = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id");
	}
	else {
		for (inr = 0; inr < instraantal; inr++) {
			string instrnaam = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getChildNode("part-name").getText();
			if ( instrnaam == "Piano" ) {
				// cout << NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id"); //oke
				piano = NodeMain.getChildNode("part-list").getChildNode("score-part", inr).getAttribute("id");
			}
		}
	}

	// 1 instrument => gebruik deze. Anders: zoek piano-deel

	int pt;

	// enkel in piano deel werken
	for (pt = 0; pt < instraantal; pt++) {
		string instrumentpart = NodeMain.getChildNode("part", pt).getAttribute("id");
		if ( instrumentpart == piano) {

			//count number of measures

			int nmeas = NodeMain.getChildNode("part", pt).nChildNode("measure");

			for (int meas = 0; meas < nmeas; meas++) {


				// aantal noten per maat
				int nnotes = NodeMain.getChildNode("part", pt).getChildNode("measure", meas).nChildNode("note");

				//for each measure, go through all notes
				for (int note = 0; note < nnotes; note++) {

					// ignore rests
					if (NodeMain.getChildNode("part", pt).getChildNode("measure", meas).getChildNode("note", note).nChildNode("rest") == 0){
					//if (NodeMain.getChildNode("part", pt).getChildNode("measure", j).getChildNode("note", k).getAttribute("print-object") == "no") {continue;} // deze wordt if, andere: else if

						int fingering = allnotes[noteid].fingering;
						// fingering: int to stringstream
						stringstream fingerconvert;
						fingerconvert << fingering;

						//new node prepare: begin, vingerzetting, node afsluiten.
						string newnode, newnodebegin, newnodefinger, newnodeend;
						newnodebegin = "<notations><technical><fingering>";
						newnodefinger = fingerconvert.str(); // stringstream to string
						newnodeend = "</fingering></technical></notations>";
						newnode = newnodebegin + newnodefinger + newnodeend; // string united

						// zet om in nieuwe fingering node
						XMLNode fingerNode=XMLNode::parseString(newnode.c_str());

						// voeg node toe aan main
						NodeMain.getChildNode("part", pt).getChildNode("measure", meas).getChildNode("note", note).addChild(fingerNode,NodeMain.getChildNode("part", pt).getChildNode("measure", meas).getChildNode("note", note).positionOfChildNode("end"));

						noteid ++;
					}
				}
			}
		}
	}

	// create a file "output.xml" containing the information in NodeMain (XML Node)
	NodeMain.writeToFile("output.xml"/*,"ISO-8859-1"*/);

	NodeMain = XMLNode::emptyNode(); // gain memory

}
