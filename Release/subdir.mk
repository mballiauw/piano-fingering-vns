################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Move.cpp \
../Move2.cpp \
../Move3.cpp \
../Move4.cpp \
../Move5.cpp \
../MusicInfo.cpp \
../exportMusicXML.cpp \
../importMusicXML.cpp \
../main.cpp \
../note.cpp \
../score.cpp \
../xmlParser.cpp 

OBJS += \
./Move.o \
./Move2.o \
./Move3.o \
./Move4.o \
./Move5.o \
./MusicInfo.o \
./exportMusicXML.o \
./importMusicXML.o \
./main.o \
./note.o \
./score.o \
./xmlParser.o 

CPP_DEPS += \
./Move.d \
./Move2.d \
./Move3.d \
./Move4.d \
./Move5.d \
./MusicInfo.d \
./exportMusicXML.d \
./importMusicXML.d \
./main.d \
./note.d \
./score.d \
./xmlParser.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


