Afstandenmatrix
duo	MinPrac	MinRel	MaxRel	MaxPrac
1-2:
	-10	1	6	11
1-3:
	-8	3	9	15
1-4:
	-6	5	11	16
1-5:
	-2	7	12	18
2-3:
	1	1	2	7
2-4:
	1	3	4	8
2-5:
	2	5	6	12
3-4:
	1	1	2	4
3-5:
	1	3	4	8
4-5:
	1	1	2	6
