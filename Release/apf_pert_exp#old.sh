for nrofruns in 1; do (10 runs)
	for typeofpert in 1; do
		for nrofperturbations in 1 4 7 10; do // or 1 5 10?
			for nbh_change in 1; do
				for nbh_swap in 1; do
					for nbh_spart in 1; do
						for nbh_change2 in 1; do
							for nrofpartspert in 1 2 4 10; do
								for percentagepert in 5 10 15 20; do
									for firstdescent in 0; do
										for tabupercent in 0; do
											for tabupermove in 0; do
												#run
./apf -typeofpert $typeofpert -nrofperturbations $nrofperturbations -nbh_change $nbh_change -nbh_swap $nbh_swap -nbh_spart $nbh_spart -nbh_change2 $nbh_change2 -nrofpartspert $nrofpartspert -percentagepert $percentagepert -tabupercent $tabupercent -tabupermove $tabupermove -firstdescent $firstdescent -bestand piece4.xml -experimentmode 1

											done
										done
									done
								done
							done
						done
					done
				done
			done
		done
	done
done
