/*
 * Move5.cpp
 *
 *  Created on: 24-feb.-2014
 *      Author: Matteo
 */

#include "Move5.h"
#include <vector>
#include "score.h"
#include <cstdlib>
#include <time.h>
#include "Move.h"
#include <iostream>
#include <fstream>
#include <limits>

using namespace std;

Move5::Move5(int a, int b, int c, int d, double e){
	id1 = a;
	id2 = b;
	finger1 = c;
	finger2 = d;
	diff = e;
}

double rightChangetwo(MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure){
	//construct neighbourhood
	Move5 changetwo(1,2,1,1,0); // elements of neighbourhood changetwo
	vector<note> adaptnotes;

	changetwo.diff = std::numeric_limits<double>::infinity();
	if (firstdescend == true){
		changetwo.diff = 0;
	}

	/*//test of tabulijst aanwezig is
	bool tabuactive = false;
	if (tabutenure>0){tabuactive = true;}*/

	bool stop = false; // used for firstdescent => stop all other loops

	for(unsigned int id1 = 1; id1 < allnotes.size(); id1 ++){//alle eerste afgaan

		//zoek voor elke id1 welke volgende id2 er binnen het bereik vallen:
		int startslicen1 = allnotes.at(id1-1).start;
		int endslicen1 = startslicen1 + allnotes.at(id1-1).duration;

		unsigned int lastid2 = id1+1;

		for (unsigned int scroller = id1+1; scroller <= allnotes.size(); scroller++ ){
			if ((allnotes.at(scroller-1).start <= startslicen1 && (allnotes.at(scroller-1).start+allnotes.at(scroller-1).duration) >= startslicen1) // eerder starten v n2; einde2 ten vroegste net voor start1
					|| (allnotes.at(scroller-1).start >= startslicen1 && (allnotes.at(scroller-1).start+allnotes.at(scroller-1).duration) <= endslicen1 ) // start2 tgl of na start1; einde2 voor of tgl met einde1
					|| ((allnotes.at(scroller-1).start+allnotes.at(scroller-1).duration) >= endslicen1 && allnotes.at(scroller-1).start <= endslicen1) //einde2 tgl of na einde1 => start2 net na of eerder dan einde1
					){
				lastid2 = scroller;
			}
		}

		// end define lastid2

		for (unsigned int id2 = id1+1; id2 <= lastid2; id2++){//alle tweede noten afgaan
			//cout << "\n"<< id2 << "," << lastid2;
			if (id1 < id2 && allnotes.at(id1-1).hand == 'R' && allnotes.at(id2-1).hand == 'R'){// rechter hand en slechts een keer (permutatie)
				adaptnotes = allnotes;
				int usedf1, usedf2;
				usedf1 = allnotes.at(id1-1).fingering;
				usedf2 = allnotes.at(id2-1).fingering;

				for (int a = 1; a <= 5; a++){ // go through all fingers for first
					if (a!= usedf1) { // for the four other fingers:
						adaptnotes.at(id1-1).fingering = a; // change fingering of first

						for (int b = 1; b <= 5; b++){ // go through all fingers for second
							if (b!= usedf2) { // for the four other fingers:
								adaptnotes.at(id2-1).fingering = b; // change fingering of second

								// check possibility
								bool possible = rightFeasibility(info, adaptnotes);

								// variabelen voor begin en einde score noot 1 en 2
								int fragstart1;
								int fragend1;
								int fragstart2;
								int fragend2;

								{//bepaal begin en eindslice voor gedeeltelijke score (noot 1)
								int prevlength = 0;
								int crlength = allnotes.at(id1-1).duration;
								int nextlength = 0;

								int begin1 = allnotes.at(id1-1).start;
								int end1 = (begin1 + allnotes.at(id1-1).duration - 1);

								for (int backskipper = allnotes.at(id1-1).start-1; backskipper >= 0; backskipper--){
									if (info.right.at(begin1).at(0) != info.right.at(backskipper).at(0) || info.right.at(begin1).at(1) != info.right.at(backskipper).at(1) ||
										info.right.at(begin1).at(2) != info.right.at(backskipper).at(2) || info.right.at(begin1).at(3) != info.right.at(backskipper).at(3) || info.right.at(begin1).at(4) != info.right.at(backskipper).at(4)){

										int dur0 = 0;
										int dur1 = 0;
										int dur2 = 0;
										int dur3 = 0;
										int dur4 = 0;

										if (info.right.at(backskipper).at(0) >0) {
											dur0 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
										}
										if (info.right.at(backskipper).at(1) >0) {
											dur1 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
										}
										if (info.right.at(backskipper).at(2) >0) {
											dur2 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
										}
										if (info.right.at(backskipper).at(3) >0) {
											dur3 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
										}
										if (info.right.at(backskipper).at(4) >0) {
											dur4 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
										}

										prevlength = maximum(dur0, dur1, dur2, dur3, dur4);

										break;
									}
									else {continue;}
								}

								for (unsigned int skipper = allnotes.at(id1-1).start+crlength; skipper < info.right.size(); skipper++){
									if (info.right.at(end1).at(0) != info.right.at(skipper).at(0) || info.right.at(end1).at(1) != info.right.at(skipper).at(1) ||
										info.right.at(end1).at(2) != info.right.at(skipper).at(2) || info.right.at(end1).at(3) != info.right.at(skipper).at(3) || info.right.at(end1).at(4) != info.right.at(skipper).at(4)){

										int dur0 = 0;
										int dur1 = 0;
										int dur2 = 0;
										int dur3 = 0;
										int dur4 = 0;

										if (info.right.at(skipper).at(0) >0) {
											dur0 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
										}
										if (info.right.at(skipper).at(1) >0) {
											dur1 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
										}
										if (info.right.at(skipper).at(2) >0) {
											dur2 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
										}
										if (info.right.at(skipper).at(3) >0) {
											dur3 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
										}
										if (info.right.at(skipper).at(4) >0) {
											dur4 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
										}

										nextlength = maximum(dur0, dur1, dur2, dur3, dur4);

										break;
									}
									else {continue;}
								}

								int start_vwd = begin1 - prevlength - crlength-1; // start die negatief kan worden
								fragstart1 = max (0, start_vwd); // beperk tot 0
								int end_vwd = end1 + crlength + nextlength+1; // einde die voorbij einde kan gaan
								int sizeofright = info.right.size();
								fragend1 = min(sizeofright , end_vwd);  // beperkt tot einde
								}

								{//bepaal begin en eindslice voor gedeeltelijke score (noot 2)
								int prevlength = 0;
								int crlength = allnotes.at((id2-1)).duration;
								int nextlength = 0;

								int begin2 = allnotes.at(id2-1).start;
								int end2 = (begin2 + allnotes.at(id2-1).duration - 1);

								for (int backskipper = allnotes.at(id2-1).start-1; backskipper >= 0; backskipper--){
									if (info.right.at(begin2).at(0) != info.right.at(backskipper).at(0) || info.right.at(begin2).at(1) != info.right.at(backskipper).at(1) ||
										info.right.at(begin2).at(2) != info.right.at(backskipper).at(2) || info.right.at(begin2).at(3) != info.right.at(backskipper).at(3) || info.right.at(begin2).at(4) != info.right.at(backskipper).at(4)){

										int dur0 = 0;
										int dur1 = 0;
										int dur2 = 0;
										int dur3 = 0;
										int dur4 = 0;

										if (info.right.at(backskipper).at(0) >0) {
											dur0 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
										}
										if (info.right.at(backskipper).at(1) >0) {
											dur1 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
										}
										if (info.right.at(backskipper).at(2) >0) {
											dur2 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
										}
										if (info.right.at(backskipper).at(3) >0) {
											dur3 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
										}
										if (info.right.at(backskipper).at(4) >0) {
											dur4 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
										}

										prevlength = maximum(dur0, dur1, dur2, dur3, dur4);

										break;
									}
									else {continue;}
								}

								for (unsigned int skipper = allnotes.at(id2-1).start+crlength; skipper < info.right.size(); skipper++){
									if (info.right.at(end2).at(0) != info.right.at(skipper).at(0) || info.right.at(end2).at(1) != info.right.at(skipper).at(1) ||
										info.right.at(end2).at(2) != info.right.at(skipper).at(2) || info.right.at(end2).at(3) != info.right.at(skipper).at(3) || info.right.at(end2).at(4) != info.right.at(skipper).at(4)){

										int dur0 = 0;
										int dur1 = 0;
										int dur2 = 0;
										int dur3 = 0;
										int dur4 = 0;

										if (info.right.at(skipper).at(0) >0) {
											dur0 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
										}
										if (info.right.at(skipper).at(1) >0) {
											dur1 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
										}
										if (info.right.at(skipper).at(2) >0) {
											dur2 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
										}
										if (info.right.at(skipper).at(3) >0) {
											dur3 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
										}
										if (info.right.at(skipper).at(4) >0) {
											dur4 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
										}

										nextlength = maximum(dur0, dur1, dur2, dur3, dur4);

										break;
									}
									else {continue;}
								}

								int start_vwd = begin2 - prevlength - crlength-1; // start die negatief kan worden
								fragstart2 = max (0, start_vwd); // beperk tot 0
								int end_vwd = end2 + crlength + nextlength+1; // einde die voorbij einde kan gaan
								int sizeofright = info.right.size();
								fragend2 = min(sizeofright , end_vwd);  // beperkt tot einde
								}

								// bepaal nu effectief fragstart en fragend
								int fragstart = min (fragstart1, fragstart2); // vroegste start
								int fragend = max (fragend1, fragend2); // verste einde

								//bepaal verschil in score en speelbaarheid

								double diff = rightScoreP(info,adaptnotes,fragstart, fragend)-rightScoreP(info,allnotes,fragstart, fragend);

								//als tabulijst actief is; test of id erin zit
								bool tabucontains = false;
								//if (tabuactive == true){
									if (itcounttabu < tabulist.at(id1-1).at(a-1) or itcounttabu < tabulist.at(id2-1).at(b-1)){tabucontains = true;}
								//}

								if (tabucontains == false){ // als de move mag volgens de tabulijst
									if (diff < changetwo.diff && possible == true){ // slaag beste element NB op (wnr verbetering)
										changetwo.diff = diff;
										changetwo.finger1 = a;
										changetwo.finger2 = b;
										changetwo.id1 = id1;
										changetwo.id2 = id2;
										//first descending => break wanneer actief
										if (firstdescend == true){
											stop = true;
											break;
										}
									}
								}

							}
							//first descending => break wanneer actief
							if (firstdescend == true && stop == true){break;}
						}

					}
					//first descending => break wanneer actief
					if (firstdescend == true && stop == true){break;}

				}

			}
			//first descending => break wanneer actief
			if (firstdescend == true && stop == true){break;}
		}
		//first descending => break wanneer actief
		if (firstdescend == true && stop == true){break;}
	}

	// execute changes
	if (tabutenure != 0){
	//if (changetwo.diff < 0) {
		allnotes.at(changetwo.id1-1).fingering = changetwo.finger1;
		allnotes.at(changetwo.id2-1).fingering = changetwo.finger2;
		//update
		tabulist.at(changetwo.id1-1).at(changetwo.finger1-1) = itcounttabu + tabutenure;
		tabulist.at(changetwo.id2-1).at(changetwo.finger2-1) = itcounttabu + tabutenure;
		//increase
		itcounttabu++;
	//}
	}
	else{
		if (changetwo.diff < 0) {
			allnotes.at(changetwo.id1-1).fingering = changetwo.finger1;
			allnotes.at(changetwo.id2-1).fingering = changetwo.finger2;
			//update
			tabulist.at(changetwo.id1-1).at(changetwo.finger1-1) = itcounttabu + tabutenure;
			tabulist.at(changetwo.id2-1).at(changetwo.finger2-1) = itcounttabu + tabutenure;
			//increase
			itcounttabu++;
		}
	}

	return rightScore(info, allnotes);

}

double leftChangetwo(MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure){
	//construct neighbourhood
	Move5 changetwo(1,2,1,1,0); // elements of neighbourhood changetwo
	vector<note> adaptnotes;

	changetwo.diff = std::numeric_limits<double>::infinity();
	if (firstdescend == true){
		changetwo.diff = 0;
	}

	/*//test of tabulijst aanwezig is
	bool tabuactive = false;
	if (tabutenure>0){tabuactive = true;}*/

	bool stop = false; // used for firstdescent => stop all other loops

	for(unsigned int id1 = 1; id1 < allnotes.size(); id1 ++){//alle eerste afgaan

		//zoek voor elke id1 welke volgende id2 er binnen het bereik vallen:
		int startslicen1 = allnotes.at(id1-1).start;
		int endslicen1 = startslicen1 + allnotes.at(id1-1).duration;

		unsigned int lastid2 = id1+1;

		for (unsigned int scroller = id1+1; scroller <= allnotes.size(); scroller++ ){
			if ((allnotes.at(scroller-1).start <= startslicen1 && (allnotes.at(scroller-1).start+allnotes.at(scroller-1).duration) >= startslicen1) // eerder starten v n2; einde2 ten vroegste net voor start1
					|| (allnotes.at(scroller-1).start >= startslicen1 && (allnotes.at(scroller-1).start+allnotes.at(scroller-1).duration) <= endslicen1 ) // start2 tgl of na start1; einde2 voor of tgl met einde1
					|| ((allnotes.at(scroller-1).start+allnotes.at(scroller-1).duration) >= endslicen1 && allnotes.at(scroller-1).start <= endslicen1) //einde2 tgl of na einde1 => start2 net na of eerder dan einde1
					){
				lastid2 = scroller;
			}
		}

		// end define lastid2

		for (unsigned int id2 = id1+1; id2 <= lastid2; id2++){//alle tweede noten afgaan
			if (id1 < id2 && allnotes.at(id1-1).hand == 'L' && allnotes.at(id2-1).hand == 'L'){// rechter hand en slechts een keer (permutatie)
				adaptnotes = allnotes;
				int usedf1, usedf2;
				usedf1 = allnotes.at(id1-1).fingering;
				usedf2 = allnotes.at(id2-1).fingering;

				for (int a = 1; a <= 5; a++){ // go through all fingers for first
					if (a!= usedf1) { // for the four other fingers:
						adaptnotes.at(id1-1).fingering = a; // change fingering of first

						for (int b = 1; b <= 5; b++){ // go through all fingers for second
							if (b!= usedf2) { // for the four other fingers:
								adaptnotes.at(id2-1).fingering = b; // change fingering of second

								// check possibility
								bool possible = leftFeasibility(info, adaptnotes);

								// variabelen voor begin en einde score noot 1 en 2
								int fragstart1;
								int fragend1;
								int fragstart2;
								int fragend2;

								{//bepaal begin en eindslice voor gedeeltelijke score (noot 1)
								int prevlength = 0;
								int crlength = allnotes.at(id1-1).duration;
								int nextlength = 0;

								int begin1 = allnotes.at(id1-1).start;
								int end1 = (begin1 + allnotes.at(id1-1).duration - 1);

								for (int backskipper = allnotes.at(id1-1).start-1; backskipper >= 0; backskipper--){
									if (info.left.at(begin1).at(0) != info.left.at(backskipper).at(0) || info.left.at(begin1).at(1) != info.left.at(backskipper).at(1) ||
										info.left.at(begin1).at(2) != info.left.at(backskipper).at(2) || info.left.at(begin1).at(3) != info.left.at(backskipper).at(3) || info.left.at(begin1).at(4) != info.left.at(backskipper).at(4)){

										int dur0 = 0;
										int dur1 = 0;
										int dur2 = 0;
										int dur3 = 0;
										int dur4 = 0;

										if (info.left.at(backskipper).at(0) >0) {
											dur0 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
										}
										if (info.left.at(backskipper).at(1) >0) {
											dur1 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
										}
										if (info.left.at(backskipper).at(2) >0) {
											dur2 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
										}
										if (info.left.at(backskipper).at(3) >0) {
											dur3 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
										}
										if (info.left.at(backskipper).at(4) >0) {
											dur4 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
										}

										prevlength = maximum(dur0, dur1, dur2, dur3, dur4);

										break;
									}
									else {continue;}
								}

								for (unsigned int skipper = allnotes.at(id1-1).start+crlength; skipper < info.left.size(); skipper++){
									if (info.left.at(end1).at(0) != info.left.at(skipper).at(0) || info.left.at(end1).at(1) != info.left.at(skipper).at(1) ||
										info.left.at(end1).at(2) != info.left.at(skipper).at(2) || info.left.at(end1).at(3) != info.left.at(skipper).at(3) || info.left.at(end1).at(4) != info.left.at(skipper).at(4)){

										int dur0 = 0;
										int dur1 = 0;
										int dur2 = 0;
										int dur3 = 0;
										int dur4 = 0;

										if (info.left.at(skipper).at(0) >0) {
											dur0 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
										}
										if (info.left.at(skipper).at(1) >0) {
											dur1 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
										}
										if (info.left.at(skipper).at(2) >0) {
											dur2 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
										}
										if (info.left.at(skipper).at(3) >0) {
											dur3 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
										}
										if (info.left.at(skipper).at(4) >0) {
											dur4 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
										}

										nextlength = maximum(dur0, dur1, dur2, dur3, dur4);

										break;
									}
									else {continue;}
								}

								int start_vwd = begin1 - prevlength - crlength-1; // start die negatief kan worden
								fragstart1 = max (0, start_vwd); // beperk tot 0
								int end_vwd = end1 + crlength + nextlength+1; // einde die voorbij einde kan gaan
								int sizeofleft = info.left.size();
								fragend1 = min(sizeofleft , end_vwd);  // beperkt tot einde
								}

								{//bepaal begin en eindslice voor gedeeltelijke score (noot 2)
								int prevlength = 0;
								int crlength = allnotes.at((id2-1)).duration;
								int nextlength = 0;

								int begin2 = allnotes.at(id2-1).start;
								int end2 = (begin2 + allnotes.at(id2-1).duration - 1);

								for (int backskipper = allnotes.at(id2-1).start-1; backskipper >= 0; backskipper--){
									if (info.left.at(begin2).at(0) != info.left.at(backskipper).at(0) || info.left.at(begin2).at(1) != info.left.at(backskipper).at(1) ||
										info.left.at(begin2).at(2) != info.left.at(backskipper).at(2) || info.left.at(begin2).at(3) != info.left.at(backskipper).at(3) || info.left.at(begin2).at(4) != info.left.at(backskipper).at(4)){

										int dur0 = 0;
										int dur1 = 0;
										int dur2 = 0;
										int dur3 = 0;
										int dur4 = 0;

										if (info.left.at(backskipper).at(0) >0) {
											dur0 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
										}
										if (info.left.at(backskipper).at(1) >0) {
											dur1 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
										}
										if (info.left.at(backskipper).at(2) >0) {
											dur2 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
										}
										if (info.left.at(backskipper).at(3) >0) {
											dur3 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
										}
										if (info.left.at(backskipper).at(4) >0) {
											dur4 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
										}

										prevlength = maximum(dur0, dur1, dur2, dur3, dur4);

										break;
									}
									else {continue;}
								}

								for (unsigned int skipper = allnotes.at(id2-1).start+crlength; skipper < info.left.size(); skipper++){
									if (info.left.at(end2).at(0) != info.left.at(skipper).at(0) || info.left.at(end2).at(1) != info.left.at(skipper).at(1) ||
										info.left.at(end2).at(2) != info.left.at(skipper).at(2) || info.left.at(end2).at(3) != info.left.at(skipper).at(3) || info.left.at(end2).at(4) != info.left.at(skipper).at(4)){

										int dur0 = 0;
										int dur1 = 0;
										int dur2 = 0;
										int dur3 = 0;
										int dur4 = 0;

										if (info.left.at(skipper).at(0) >0) {
											dur0 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
										}
										if (info.left.at(skipper).at(1) >0) {
											dur1 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
										}
										if (info.left.at(skipper).at(2) >0) {
											dur2 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
										}
										if (info.left.at(skipper).at(3) >0) {
											dur3 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
										}
										if (info.left.at(skipper).at(4) >0) {
											dur4 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
										}

										nextlength = maximum(dur0, dur1, dur2, dur3, dur4);

										break;
									}
									else {continue;}
								}

								int start_vwd = begin2 - prevlength - crlength-1; // start die negatief kan worden
								fragstart2 = max (0, start_vwd); // beperk tot 0
								int end_vwd = end2 + crlength + nextlength+1; // einde die voorbij einde kan gaan
								int sizeofleft = info.left.size();
								fragend2 = min(sizeofleft , end_vwd);  // beperkt tot einde
								}

								// bepaal nu effectief fragstart en fragend
								int fragstart = min (fragstart1, fragstart2); // vroegste start
								int fragend = max (fragend1, fragend2); // verste einde

								//bepaal verschil in score en speelbaarheid

								double diff = leftScoreP(info,adaptnotes,fragstart, fragend)-leftScoreP(info,allnotes,fragstart, fragend);

								//als tabulijst actief is; test of id erin zit
								bool tabucontains = false;
								//if (tabuactive == true){
									if (itcounttabu < tabulist.at(id1-1).at(a-1) or itcounttabu < tabulist.at(id2-1).at(b-1)){tabucontains = true;}
								//}

								if (tabucontains == false){ // als de move mag volgens de tabulijst
									if (diff < changetwo.diff && possible == true){ // slaag beste element NB op (wnr verbetering)
										changetwo.diff = diff;
										changetwo.finger1 = a;
										changetwo.finger2 = b;
										changetwo.id1 = id1;
										changetwo.id2 = id2;
										//first descending => break wanneer actief
										if (firstdescend == true){
											stop = true;
											break;
										}
									}
								}

							}
							//first descending => break wanneer actief
							if (firstdescend == true && stop == true){break;}
						}

					}
					//first descending => break wanneer actief
					if (firstdescend == true && stop == true){break;}

				}

			}
			//first descending => break wanneer actief
			if (firstdescend == true && stop == true){break;}
		}
		//first descending => break wanneer actief
		if (firstdescend == true && stop == true){break;}
	}

	// execute changes
	if (tabutenure != 0){
	//if (changetwo.diff < 0) {
		allnotes.at(changetwo.id1-1).fingering = changetwo.finger1;
		allnotes.at(changetwo.id2-1).fingering = changetwo.finger2;
		//update
		tabulist.at(changetwo.id1-1).at(changetwo.finger1-1) = itcounttabu + tabutenure;
		tabulist.at(changetwo.id2-1).at(changetwo.finger2-1) = itcounttabu + tabutenure;
		//increase
		itcounttabu++;
	//}
	}
	else{
		if (changetwo.diff < 0) {
			allnotes.at(changetwo.id1-1).fingering = changetwo.finger1;
			allnotes.at(changetwo.id2-1).fingering = changetwo.finger2;
			//update
			tabulist.at(changetwo.id1-1).at(changetwo.finger1-1) = itcounttabu + tabutenure;
			tabulist.at(changetwo.id2-1).at(changetwo.finger2-1) = itcounttabu + tabutenure;
			//increase
			itcounttabu++;
		}
	}

	return leftScore(info, allnotes);

}
