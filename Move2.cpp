/*
 * Move2.cpp
 *
 *  Created on: 28-okt.-2013
 *      Author: Matteo
 */

#include "Move2.h"
#include "Move.h"
#include <vector>
#include "score.h"
#include <cstdlib>
#include "Move.h"

using namespace std;

Move2::Move2(int a ,int b,double c){
	firstfinger = a;
	secondfinger = b;
	diff = c;
}

// algorithm

// Move type: SWAP 2 FINGERS: swap throughout 2 fingers = always feasible
// steapest descend: take the best.
// once right, once left

//right
double rightSwapf (MusicInfo &info, vector<note> &allnotes){
		//construct neighbourhood
		/*fout*/ // Move2 swapf; // elements of neighbourhood swapf
		vector<note> adaptnotes;

		/*fout*/ //swapf.diff = rightScore(info, allnotes);
		Move2 swapf(1,2,rightScore(info, allnotes));

		for (int fingera = 1; fingera <=4; fingera++){ // alle vingerparen afgaan (1-2, 1-3 .. 4-5)
			for (int fingerb = fingera+1; fingerb <=5; fingerb++){
				adaptnotes=allnotes; // copy note info to change fingering here


				for (unsigned int id = 1; id <= adaptnotes.size(); id++){ // go through all notes
					if (adaptnotes[(id-1)].hand == 'R'){ // right
						if (adaptnotes.at(id-1).fingering == fingera){
							adaptnotes.at(id-1).fingering = fingerb;
						}
						else if (adaptnotes.at(id-1).fingering == fingerb){
							adaptnotes.at(id-1).fingering = fingera;
						}
					}
				}
				double moeilijk = rightScore(info, adaptnotes);
				if (moeilijk < swapf.diff){
					swapf.diff = moeilijk;
					swapf.firstfinger = fingera;
					swapf.secondfinger = fingerb;
				}
			}
		}


		// select first value (is lowest difficulty) and execute change, if easier than previous
		if (rightScore(info, allnotes) > swapf.diff) {
			int fingera = swapf.firstfinger;
			int fingerb = swapf.secondfinger;

			for (unsigned int id = 1; id <= allnotes.size(); id++){ // go through all notes
				if (allnotes.at(id-1).hand == 'R'){ // right
					if (allnotes.at(id-1).fingering == fingera){
						allnotes.at(id-1).fingering = fingerb;
					}
					else if (allnotes.at(id-1).fingering == fingerb){
						allnotes.at(id-1).fingering = fingera;
					}
				}
			}
		}
	return rightScore(info, allnotes);
}


// left
double leftSwapf (MusicInfo &info, vector<note> &allnotes){
		//construct neighbourhood
		/*fout*/ // Move2 swapf; // elements of neighbourhood swapf
		vector<note> adaptnotes;

		/*fout*/ //swapf.diff = rightScore(info, allnotes);
		Move2 swapf(1,2,leftScore(info, allnotes));

		for (int fingera = 1; fingera <=4; fingera++){ // alle vingerparen afgaan (1-2, 1-3 .. 4-5)
			for (int fingerb = fingera+1; fingerb <=5; fingerb++){
				adaptnotes=allnotes; // copy note info to change fingering here


				for (unsigned int id = 1; id <= adaptnotes.size(); id++){ // go through all notes
					if (adaptnotes.at(id-1).hand == 'L'){ // left
						if (adaptnotes.at(id-1).fingering == fingera){
							adaptnotes.at(id-1).fingering = fingerb;
						}
						else if (adaptnotes.at(id-1).fingering == fingerb){
							adaptnotes.at(id-1).fingering = fingera;
						}
					}
				}
				double moeilijk = leftScore(info, adaptnotes);
				if (moeilijk < swapf.diff){
					swapf.diff = moeilijk;
					swapf.firstfinger = fingera;
					swapf.secondfinger = fingerb;
				}
			}
		}


		// select first value (is lowest difficulty) and execute change, if easier than previous
		if (leftScore(info, allnotes) > swapf.diff) {
			int fingera = swapf.firstfinger;
			int fingerb = swapf.secondfinger;

			for (unsigned int id = 1; id <= allnotes.size(); id++){ // go through all notes
				if (allnotes.at(id-1).hand == 'L'){ // left
					if (allnotes.at(id-1).fingering == fingera){
						allnotes.at(id-1).fingering = fingerb;
					}
					else if (allnotes.at(id-1).fingering == fingerb){
						allnotes.at(id-1).fingering = fingera;
					}
				}
			}
		}
	return leftScore(info, allnotes);
}
