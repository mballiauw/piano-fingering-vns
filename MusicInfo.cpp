/*
 * MusicInfo.cpp
 *
 *  Created on: 22 Oct 2013
 *      Author: dorien
 */

#include "MusicInfo.h"
#include <vector>
#include <string>
#include <iostream>
#include <string.h>
#include <fstream>
#include <stdlib.h>

using namespace std;


MusicInfo fingerSpecs(){

	MusicInfo info2;
// input finger distances and read .txt file
	//matrix [rij][kolom]

	int MinPrac [5][5];
	int MaxPrac [5][5];
	int MinComf [5][5];
	int MaxComf [5][5];
	int MinRel [5][5];
	int MaxRel [5][5];

	{
		//arrays (matrices) aanmaken.



		ifstream myfile;
		myfile.open("afstanden.txt");

		string buffer;

		//skip 1st and 2nd line
		getline(myfile,buffer);
		getline(myfile,buffer);

		//skip 3rd, read 4th; etc.
		// invullen matrices MinPrac en MinRel
		for (int i = 1; i < 5; i++){

		getline(myfile,buffer);

		myfile >> buffer;
		MinPrac [0][i] = atoi(buffer.c_str());
		myfile >> buffer;
		MinRel [0][i] = atoi(buffer.c_str());
		myfile >> buffer;
		MinRel [i][0] = (atoi(buffer.c_str())*(-1));
		myfile >> buffer;
		MinPrac [i][0] = (atoi(buffer.c_str())*(-1));

		getline(myfile,buffer);}

		for (int i = 2; i < 5; i++){

		getline(myfile,buffer);

		myfile >> buffer;
		MinPrac [1][i] = atoi(buffer.c_str());
		myfile >> buffer;
		MinRel [1][i] = atoi(buffer.c_str());
		myfile >> buffer;
		MinRel [i][1] = (atoi(buffer.c_str())*(-1));
		myfile >> buffer;
		MinPrac [i][1] = (atoi(buffer.c_str())*(-1));

		getline(myfile,buffer);}

		for (int i = 3; i < 5; i++){

		getline(myfile,buffer);

		myfile >> buffer;
		MinPrac [2][i] = atoi(buffer.c_str());
		myfile >> buffer;
		MinRel [2][i] = atoi(buffer.c_str());
		myfile >> buffer;
		MinRel [i][2] = (atoi(buffer.c_str())*(-1));
		myfile >> buffer;
		MinPrac [i][2] = (atoi(buffer.c_str())*(-1));

		getline(myfile,buffer);}

		getline(myfile,buffer);

		myfile >> buffer;
		MinPrac [3][4] = atoi(buffer.c_str());
		myfile >> buffer;
		MinRel [3][4] = atoi(buffer.c_str());
		myfile >> buffer;
		MinRel [4][3] = (atoi(buffer.c_str())*(-1));
		myfile >> buffer;
		MinPrac [4][3] = (atoi(buffer.c_str())*(-1));

		getline(myfile,buffer);

		myfile.close();

		//zeros generation
		for(int i = 0; i < 5; i++){
			MinPrac [i][i] = 0;
			MinRel[i][i] = 0;
			MinComf[i][i] = 0;
		}

		// MinComf = MinPrac + 2 (enkel wanneer duim erin: dus al de rest in volgende stap ongedaan maken)
		for (int i = 0; i < 5; i++){
			for (int j =0; j < 5; j++){
				if(i!=j){
					MinComf [i][j]= (MinPrac [i][j]+2);
				}
			}
		}

		//correction for pairs not involving thumb
		for (int i = 1; i < 4; i++){
			for (int j = 2; j < 5; j++){
				if(i<j){
					MinComf [i][j]= (MinComf [i][j]-2);
				}
			}
		}

		//Transpose to get max matrix
		for (int i = 0; i < 5; i++){
			for (int j =0; j < 5; j++){
				MaxPrac [i][j] = ((-1)* MinPrac[j][i]);
				MaxRel [i][j] = ((-1)* MinRel[j][i]);
				MaxComf [i][j] = ((-1)* MinComf[j][i]);
			}
		}
	// output getest: correcte matrices!
		/*for (int i = 0; i<5; i++){
			for (int j = 0; j< 5; j++){
				cout << MinComf[i][j] << ",	";
			}
			cout << "\n";
		}*/

	}

	// overzetten naar klasse info
	for (int s = 0; s < 5 ; s++){
		for (int t = 0; t < 5; t++){
			info2.MaxComf[s][t] = MaxComf[s][t];
			info2.MaxPrac[s][t] = MaxPrac[s][t];
			info2.MaxRel[s][t] = MaxRel[s][t];
			info2.MinComf[s][t] = MinComf[s][t];
			info2.MinPrac[s][t] = MinPrac[s][t];
			info2.MinRel[s][t] = MinRel[s][t];

		}
	}

	return info2;
	}
