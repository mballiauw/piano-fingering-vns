/*
 * Move.h
 *
 *  Created on: 23-okt.-2013
 *      Author: Matteo
 */

#ifndef MOVE_H_
#define MOVE_H_

#include "score.h"
#include <time.h>
#include <algorithm>

using namespace std;

class Move {

public:
	int id;
	int finger;
	double diff;
	Move (int,int,double);
};

int maximum(int x, int y, int z, int xx, int xy);

int compareQS (const void * a, const void * b);

// algorithm

	// Move type: CHANGE ONE: for every note: try other possible fingerings
	// steapest descend: take the feasible fingering that decreases score the most.
	// once right, once left.

// worden niet gebruikt
	// right hand
	double rightChangeone (MusicInfo &info, vector<note> &allnotes);
	// left hand
	double leftChangeone (MusicInfo &info, vector<note> &allnotes);


	// propagated move; steepest descend
	double rightChangeoneP (MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure);
	double leftChangeoneP (MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure);

	/*
	//first improving
	double rightChangeoneFirst (MusicInfo &info, vector<note> &allnotes, vector<int> &tabulist);
	double leftChangeoneFirst (MusicInfo &info, vector<note> &allnotes, vector<int> &tabulist);
*/


#endif /* MOVE_H_ */
