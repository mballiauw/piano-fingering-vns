/*
 * Move.cpp
 *
 *  Created on: 23-okt.-2013
 *      Author: Matteo
 */

#include "Move.h"
#include <vector>
#include "score.h"
#include <cstdlib>
#include <time.h>
#include <limits>

using namespace std;

Move::Move (int a, int b, double c) {
	  id = a;
	  finger = b;
	  diff = c;
	}

/* Function maximum definition */
/* x, y and z are parameters */
int maximum(int x, int y, int z, int xx, int xy) {
	int max = x; /* assume x is the largest */

	if (y > max) { /* if y is larger than max, assign y to max */
		max = y;
	} /* end if */

	if (z > max) { /* if z is larger than max, assign z to max */
		max = z;
	} /* end if */

	if (xx > max) { /* if z is larger than max, assign z to max */
		max = xx;
	} /* end if */

	if (xy > max) { /* if z is larger than max, assign z to max */
		max = xy;
	} /* end if */

	return max; /* max is the largest value */
} /* end function maximum */




//sorting function quicksort
int compareQS (const void * a, const void * b) {
	if ( (*(Move*)a).diff <  (*(Move*)b).diff) {return -1;}
	else if ( (*(Move*)a).diff == (*(Move*)b).diff  ) {return 0;}
	//else if ( (*(Move*)a).diff >  (*(Move*)b).diff ) {return 1;}
	else {return 1;}
}

// algorithm

	// Move type: CHANGE ONE: for every note: try other possible fingerings
	// steapest descend: take the feasible fingering that decreases score the most.
	// once right, once left.

	// right hand
double rightChangeone (MusicInfo &info, vector<note> &allnotes){
		//construct neighbourhood
		/*fout*/ // Move changeone; // elements of neighbourhood changeone
		vector<note> adaptnotes;

		/*fout*/ // changeone.diff = rightScore(info, allnotes);
		Move changeone (1,allnotes[0].fingering,rightScore(info, allnotes));

		for (unsigned int id = 1; id <= allnotes.size(); id++){ // go through all notes
			if (allnotes[(id-1)].hand == 'R'){ // right
				adaptnotes=allnotes; // copy note info to change fingering here

				int finger = getFingering((id), allnotes); // actual finger
				for (int a = 1; a <= 5; a++){ // go through all fingers
					if (a!= finger) { // for the four other fingers:

						adaptnotes[(id-1)].fingering = a; // change fingering

						//cout adaptnotes OKE (past telkens fingering aan
						/*cout << "Index of notes: " << "\n" "ID, Pitch, Start, Duration, Hand, Voice, Fingering:" << "\n";
						for(int z = 0; z < numberofnotes; z++){
							cout << adaptnotes[z].id <<",	" << adaptnotes[z].pitch <<",	" << adaptnotes[z].start <<",	" << adaptnotes[z].duration <<",	" << adaptnotes[z].hand <<",	" << adaptnotes[z].voice <<",	" << adaptnotes[z].fingering << "\n";
						}*/

						// check possibility
						bool possible = rightFeasibility(info, adaptnotes);
						/*cout << "mogelijke vingerzetting zo?" << possible <<"\n";*/

						if (possible==1 && rightScore(info, adaptnotes) < changeone.diff) {  // if change is feasible?
							changeone.diff = rightScore(info, adaptnotes); // store difficulty
							changeone.finger = a; // store finger
							changeone.id = id; // store id
						}
					}
				}
			}
		}


		// output neighbourhood
		/*cout << "id, finger, diff \n";
		for (unsigned int x= 0; x < NBchangeone.size(); x++){
			cout << NBchangeone[x].id << ",	" << NBchangeone[x].finger << ",	" << NBchangeone[x].diff << ",	";
			cout << "\n";
		}*/

		// select first value (is lowest difficulty) and execute change, if easier than previous
		if (rightScore(info, allnotes) > changeone.diff) {
			allnotes[((changeone.id)-1)].fingering = changeone.finger;
		}
	return rightScore(info, allnotes);
}


	// left hand
double leftChangeone (MusicInfo &info, vector<note> &allnotes){
		//construct neighbourhood
		/*fout*/ // Move changeone; // elements of neighbourhood changeone
		vector<note> adaptnotes;

		/*fout*/ // changeone.diff = leftScore(info, allnotes);
		Move changeone (1,allnotes[0].fingering,leftScore(info, allnotes));

		for (unsigned int id = 1; id <= allnotes.size(); id++){ // go through all notes
			if (allnotes[(id-1)].hand == 'L'){ // left
				adaptnotes=allnotes; // copy note info to change fingering here

				int finger = getFingering((id), allnotes); // actual finger
				for (int a = 1; a <= 5; a++){ // go through all fingers
					if (a!= finger) { // for the four other fingers:

						adaptnotes[(id-1)].fingering = a; // change fingering

						//cout adaptnotes OKE (past telkens fingering aan
						/*cout << "Index of notes: " << "\n" "ID, Pitch, Start, Duration, Hand, Voice, Fingering:" << "\n";
						for(int z = 0; z < numberofnotes; z++){
							cout << adaptnotes[z].id <<",	" << adaptnotes[z].pitch <<",	" << adaptnotes[z].start <<",	" << adaptnotes[z].duration <<",	" << adaptnotes[z].hand <<",	" << adaptnotes[z].voice <<",	" << adaptnotes[z].fingering << "\n";
						}*/

						// check possibility
						bool possible = leftFeasibility(info, adaptnotes);
						/*cout << "mogelijke vingerzetting zo?" << possible <<"\n";*/

						if (possible==1 && leftScore(info, adaptnotes) < changeone.diff) {  // if change is feasible?
							changeone.diff = leftScore(info, adaptnotes); // store difficulty
							changeone.finger = a; // store finger
							changeone.id = id; // store id
						}
					}
				}
			}
		}


		// output neighbourhood
		/*cout << "id, finger, diff \n";
		for (unsigned int x= 0; x < NBchangeone.size(); x++){
			cout << NBchangeone[x].id << ",	" << NBchangeone[x].finger << ",	" << NBchangeone[x].diff << ",	";
			cout << "\n";
		}*/

		// select first value (is lowest difficulty) and execute change, if easier than previous
		if (leftScore(info, allnotes) > changeone.diff) {
			allnotes[((changeone.id)-1)].fingering = changeone.finger;
		}
	return leftScore(info, allnotes);
}

double rightChangeoneP (MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure){
	//construct neighbourhood
	/*fout*/ // Move changeone; // elements of neighbourhood changeone
	vector<note> adaptnotes;

	/*//test of tabulijst aanwezig is
	bool tabuactive = false;
	if (tabutenure>0){tabuactive = true;}*/

	bool stop = false; // used for firstdescend

	/*fout*/ // changeone.diff = 0;
	Move changeone (1,allnotes.at(0).fingering,0);

	changeone.diff = std::numeric_limits<double>::infinity();
	if (firstdescend == true){
		changeone.diff = 0;
	}

	for (unsigned int slicenr = 0; slicenr < info.right.size(); slicenr ++){


		for (int el = 0; el<5;el++){
			adaptnotes=allnotes; // copy note info to change fingering here
			int id = info.right.at(slicenr).at(el);
			if (id!=0){

				int finger = getFingering((id), allnotes); // actual finger
				for (int a = 1; a <= 5; a++){ // go through all fingers
					if (a!= finger) { // for the four other fingers:

						adaptnotes.at(id-1).fingering = a; // change fingering

						// check possibility
						bool possible = rightFeasibility(info, adaptnotes);

						//bepaal begin en eindslice voor gedeeltelijke score
						int prevlength = 0;
						int crlength = allnotes.at(id-1).duration;
						int nextlength = 0;

						for (int backskipper = allnotes.at(id-1).start-1; backskipper >= 0; backskipper--){
							if (info.right.at(slicenr).at(0) != info.right.at(backskipper).at(0) || info.right.at(slicenr).at(1) != info.right.at(backskipper).at(1) ||
								info.right.at(slicenr).at(2) != info.right.at(backskipper).at(2) || info.right.at(slicenr).at(3) != info.right.at(backskipper).at(3) || info.right.at(slicenr).at(4) != info.right.at(backskipper).at(4)){

								int dur0 = 0;
								int dur1 = 0;
								int dur2 = 0;
								int dur3 = 0;
								int dur4 = 0;

								if (info.right.at(backskipper).at(0) >0) {
									dur0 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
								}
								if (info.right.at(backskipper).at(1) >0) {
									dur1 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
								}
								if (info.right.at(backskipper).at(2) >0) {
									dur2 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
								}
								if (info.right.at(backskipper).at(3) >0) {
									dur3 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
								}
								if (info.right.at(backskipper).at(4) >0) {
									dur4 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
								}

								prevlength = maximum(dur0, dur1, dur2, dur3, dur4);

								break;
							}
							else {continue;}
						}

						for (unsigned int skipper = allnotes.at(id-1).start+crlength; skipper < info.right.size(); skipper++){
							if (info.right.at(slicenr).at(0) != info.right.at(skipper).at(0) || info.right.at(slicenr).at(1) != info.right.at(skipper).at(1) ||
								info.right.at(slicenr).at(2) != info.right.at(skipper).at(2) || info.right.at(slicenr).at(3) != info.right.at(skipper).at(3) || info.right.at(slicenr).at(4) != info.right.at(skipper).at(4)){

								int dur0 = 0;
								int dur1 = 0;
								int dur2 = 0;
								int dur3 = 0;
								int dur4 = 0;

								if (info.right.at(skipper).at(0) >0) {
									dur0 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
								}
								if (info.right.at(skipper).at(1) >0) {
									dur1 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
								}
								if (info.right.at(skipper).at(2) >0) {
									dur2 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
								}
								if (info.right.at(skipper).at(3) >0) {
									dur3 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
								}
								if (info.right.at(skipper).at(4) >0) {
									dur4 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
								}

								nextlength = maximum(dur0, dur1, dur2, dur3, dur4);

								break;
							}
							else {continue;}
						}

						int start_vwd = slicenr - prevlength - crlength-1; // start die negatief kan worden
						int start = max (0, start_vwd); // beperk tot 0
						int end_vwd = slicenr + crlength + nextlength+1; // einde die voorbij einde kan gaan
						int sizeofright = info.right.size(); int end = min(sizeofright , end_vwd);  // beperkt tot einde

						//als tabulijst actief is; test of id erin zit
						bool tabucontains = false;
						if (itcounttabu < tabulist.at(id-1).at(a-1)){tabucontains = true; }

						if (tabucontains == false){ // als de move mag volgens de tabulijst
							double moeilijk = (rightScoreP(info, adaptnotes, start, end) - rightScoreP(info, allnotes, start, end));
							if (possible==1 && changeone.diff> moeilijk){ // if change is feasible? and easier
								changeone.diff = moeilijk; // store difficulty
								//cout << "score verschllen"<< rightScoreP(info, adaptnotes, start, end) <<", "<< rightScoreP(info, allnotes, start, end) <<", "<< changeone.diff<< ", f: " << changeone.finger << ", id: " << changeone.id<<"\n";
								changeone.finger = a; // store new finger
								changeone.id = id; // store id
								//first descending => break wanneer actief
								if (firstdescend == true){
									stop = true;
									break;
								}
							}
						}

					}
				}
			}
			//first descending => break wanneer actief
			if (firstdescend == true && stop == true){break;}
		}
		//first descending => break wanneer actief
		if (firstdescend == true && stop == true){break;}
	}

	//cout << NBchangeone[0].id << ", finger: " <<NBchangeone[0].finger << ", diff: " <<NBchangeone[0].diff<<"||";

	if (tabutenure != 0){
	// select first value (is lowest difficulty) and execute change,
	//if (changeone.diff < 0){
		allnotes.at((changeone.id)-1).fingering = changeone.finger; // doorvoeren
		// update tabu list
		tabulist.at(changeone.id-1).at(changeone.finger-1) = itcounttabu + tabutenure;
		itcounttabu++;
		//cout << "nieuwe: " << NBchangeone[0].finger<< "id" << NBchangeone[0].id;
	//}
	}
	else {
		if (changeone.diff < 0){
			allnotes.at((changeone.id)-1).fingering = changeone.finger; // doorvoeren
			// update tabu list
			tabulist.at(changeone.id-1).at(changeone.finger-1) = itcounttabu + tabutenure;
			itcounttabu++;
			//cout << "nieuwe: " << NBchangeone[0].finger<< "id" << NBchangeone[0].id;
		}
	}

	return rightScore (info, allnotes);
}

double leftChangeoneP (MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure){

	//construct neighbourhood
	/*fout*/ // Move changeone; // elements of neighbourhood changeone
	vector<note> adaptnotes;

	/*fout*/ // changeone.diff = 0;
	Move changeone (1,allnotes.at(0).fingering,0);

	/*//test of tabulijst aanwezig is
	bool tabuactive = false;
	if (tabutenure>0){tabuactive = true;}*/

	changeone.diff = std::numeric_limits<double>::infinity();
	if (firstdescend == true){
		changeone.diff = 0;
	}

	bool stop = false; // used for firstdescend

	for (unsigned int slicenr = 0; slicenr < info.left.size(); slicenr ++){


		for (int el = 0; el<5;el++){
			adaptnotes=allnotes; // copy note info to change fingering here
			int id = info.left.at(slicenr).at(el);
			if (id!=0){

				int finger = getFingering((id), allnotes); // actual finger
				for (int a = 1; a <= 5; a++){ // go through all fingers
					if (a!= finger) { // for the four other fingers:

						adaptnotes.at(id-1).fingering = a; // change fingering

						// check possibility
						bool possible = leftFeasibility(info, adaptnotes);

						//bepaal begin en eindslice voor gedeeltelijke score
						int prevlength = 0;
						int crlength = allnotes.at(id-1).duration;
						int nextlength = 0;

						for (int backskipper = allnotes.at(id-1).start-1; backskipper >= 0; backskipper--){
							if (info.left.at(slicenr).at(0) != info.left.at(backskipper).at(0) || info.left.at(slicenr).at(1) != info.left.at(backskipper).at(1) ||
								info.left.at(slicenr).at(2) != info.left.at(backskipper).at(2) || info.left.at(slicenr).at(3) != info.left.at(backskipper).at(3) || info.left.at(slicenr).at(4) != info.left.at(backskipper).at(4)){

								int dur0 = 0;
								int dur1 = 0;
								int dur2 = 0;
								int dur3 = 0;
								int dur4 = 0;

								if (info.left.at(backskipper).at(0) >0) {
									dur0 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
								}
								if (info.left.at(backskipper).at(1) >0) {
									dur1 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
								}
								if (info.left.at(backskipper).at(2) >0) {
									dur2 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
								}
								if (info.left.at(backskipper).at(3) >0) {
									dur3 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
								}
								if (info.left.at(backskipper).at(4) >0) {
									dur4 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
								}

								prevlength = maximum(dur0, dur1, dur2, dur3, dur4);

								break;
							}
							else {continue;}
						}

						for (unsigned int skipper = allnotes.at(id-1).start+crlength; skipper < info.left.size(); skipper++){
							if (info.left.at(slicenr).at(0) != info.left.at(skipper).at(0) || info.left.at(slicenr).at(1) != info.left.at(skipper).at(1) ||
								info.left.at(slicenr).at(2) != info.left.at(skipper).at(2) || info.left.at(slicenr).at(3) != info.left.at(skipper).at(3) || info.left.at(slicenr).at(4) != info.left.at(skipper).at(4)){

								int dur0 = 0;
								int dur1 = 0;
								int dur2 = 0;
								int dur3 = 0;
								int dur4 = 0;

								if (info.left.at(skipper).at(0) >0) {
									dur0 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
								}
								if (info.left.at(skipper).at(1) >0) {
									dur1 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
								}
								if (info.left.at(skipper).at(2) >0) {
									dur2 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
								}
								if (info.left.at(skipper).at(3) >0) {
									dur3 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
								}
								if (info.left.at(skipper).at(4) >0) {
									dur4 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
								}

								nextlength = maximum(dur0, dur1, dur2, dur3, dur4);

								break;
							}
							else {continue;}
						}

						int start_vwd = slicenr - prevlength - crlength-1; // start die negatief kan worden
						int start = max (0, start_vwd); // beperk tot 0
						int end_vwd = slicenr + crlength + nextlength+1; // einde die voorbij einde kan gaan
						int sizeofleft = info.left.size(); int end = min(sizeofleft , end_vwd);  // beperkt tot einde

						//als tabulijst actief is; test of id erin zit
						bool tabucontains = false;
						if (itcounttabu < tabulist.at(id-1).at(a-1)){tabucontains = true; }

						if (tabucontains == false){ // als de move mag volgens de tabulijst
							double moeilijk = (leftScoreP(info, adaptnotes, start, end) - leftScoreP(info, allnotes, start, end));
							if (possible==1 && changeone.diff> moeilijk){ // if change is feasible? and easier?
								changeone.diff = moeilijk; // store difficulty
								//cout << "score verschllen"<< leftScoreP(info, adaptnotes, start, end) <<", "<< leftScoreP(info, allnotes, start, end) <<", "<< changeone.diff<< ", f: " << changeone.finger << ", id: " << changeone.id<<"\n";
								changeone.finger = a; // store difficulty
								changeone.id = id; // store id
								//first descending => break wanneer actief
								if (firstdescend == true){
									stop = true;
									break;
								}
							}
						}

					}
				}
			}
			//first descending => break wanneer actief
			if (firstdescend == true && stop == true){break;}
		}
		//first descending => break wanneer actief
		if (firstdescend == true && stop == true){break;}
	}

	//cout << NBchangeone[0].id << ", finger: " <<NBchangeone[0].finger << ", diff: " <<NBchangeone[0].diff<<"||";

	if (tabutenure != 0){
	// select first value (is lowest difficulty) and execute change,
	//if (changeone.diff < 0){
		allnotes.at((changeone.id)-1).fingering = changeone.finger; // doorvoeren
		// update tabu list
		tabulist.at(changeone.id-1).at(changeone.finger-1) = itcounttabu + tabutenure;
		itcounttabu++;
		//cout << "nieuwe: " << NBchangeone[0].finger<< "id" << NBchangeone[0].id;
	//}
	}
	else {
		if (changeone.diff < 0){
			allnotes.at((changeone.id)-1).fingering = changeone.finger; // doorvoeren
			// update tabu list
			tabulist.at(changeone.id-1).at(changeone.finger-1) = itcounttabu + tabutenure;
			itcounttabu++;
			//cout << "nieuwe: " << NBchangeone[0].finger<< "id" << NBchangeone[0].id;
		}
	}

	return leftScore (info, allnotes);
}




/*
double rightChangeoneP (MusicInfo &info, vector<note> &allnotes, int frequency, clock_t init){
	//construct neighbourhood
	Move changeone; // elements of neighbourhood changeone
	vector <Move> NBchangeone; // contains neighbourhood changeone
	vector<note> adaptnotes;
	int divspm = info.divspm;

	for (unsigned int startsl = 0; startsl < (info.right.size()-(5*divspm)); startsl += (divspm*4)){

		for (int count = 0; count < frequency; count ++){
			NBchangeone.clear();
			for (unsigned int slice = startsl; slice < (startsl+(5*divspm)); slice++){
				for (int el = 0; el<5;el++){
					adaptnotes=allnotes; // copy note info to change fingering here
					int id = info.right[slice][el];
					if (id!=0){

						int finger = getFingering((id), allnotes); // actual finger
						for (int a = 1; a <= 5; a++){ // go through all fingers
							if (a!= finger) { // for the four other fingers:

								adaptnotes[(id-1)].fingering = a; // change fingering

								// check possibility
								bool possible = rightFeasibility(info, adaptnotes);

								if (possible==1){ // if change is feasible?
									changeone.diff = (rightScoreP(info, adaptnotes, startsl) - rightScoreP(info, allnotes, startsl)); // store difficulty
									changeone.finger = a; // store difficulty
									changeone.id = id; // store id
									NBchangeone.push_back(changeone); // capture neigbourhood
								}

							}
						}
					}
				}
			}
			// sorteer neighbourhood: laagste diff eerst.
			qsort(&NBchangeone[0], NBchangeone.size(), sizeof(Move), compareQS);

			//cout << NBchangeone[0].id << ", finger: " <<NBchangeone[0].finger << ", diff: " <<NBchangeone[0].diff<<"||";

			adaptnotes = allnotes;
			adaptnotes[((NBchangeone[0].id)-1)].fingering = NBchangeone[0].finger;

			// select first value (is lowest difficulty) and execute change, if easier than previous
			if (rightScoreP(info, allnotes, startsl) > rightScoreP(info, adaptnotes, startsl)) {
				allnotes[((NBchangeone[0].id)-1)].fingering = NBchangeone[0].finger;

				// output moveinfo
				clock_t final=clock()-init;
				cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << rightScore(info, allnotes) << ",	1_R (measure:" << (startsl/info.divspm) << ")" << "\n";

			}

			// stop als geen verbetering
			else {
				break;
			}

		}
	}

	int startsl = (info.right.size()-(5*divspm));

	for (int count = 0; count < frequency; count ++){
		NBchangeone.clear();
		for (int slice = startsl; slice < (startsl+(5*divspm)); slice++){
			for (int el = 0; el<5;el++){
				adaptnotes=allnotes; // copy note info to change fingering here
				int id = info.right[slice][el];
				if (id!=0){

					int finger = getFingering((id), allnotes); // actual finger
					for (int a = 1; a <= 5; a++){ // go through all fingers
						if (a!= finger) { // for the four other fingers:

							adaptnotes[(id-1)].fingering = a; // change fingering

							// check possibility
							bool possible = rightFeasibility(info, adaptnotes);

							if (possible==1){ // if change is feasible?
								changeone.diff = (rightScoreP(info, adaptnotes, startsl) - rightScoreP(info, allnotes, startsl)); // store difficulty
								changeone.finger = a; // store difficulty
								changeone.id = id; // store id
								NBchangeone.push_back(changeone); // capture neigbourhood
							}

						}
					}
				}
			}
		}
		// sorteer neighbourhood: laagste diff eerst.
		qsort(&NBchangeone[0], NBchangeone.size(), sizeof(Move), compareQS);

		adaptnotes = allnotes;
		adaptnotes[((NBchangeone[0].id)-1)].fingering = NBchangeone[0].finger;

		// select first value (is lowest difficulty) and execute change, if easier than previous
		if (rightScoreP(info, allnotes, startsl) > rightScoreP(info, adaptnotes, startsl)) {
			allnotes[((NBchangeone[0].id)-1)].fingering = NBchangeone[0].finger;

			// output moveinfo
			clock_t final=clock()-init;
			cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << rightScore(info, allnotes) << ",	1_R" << " (measure:" << (startsl/info.divspm) << ")" << "\n";

		}

		// stop als geen verbetering
		else {
			break;
		}

	}

	return rightScore (info, allnotes);
}

double leftChangeoneP (MusicInfo &info, vector<note> &allnotes, int frequency, clock_t init){
	//construct neighbourhood
	Move changeone; // elements of neighbourhood changeone
	vector <Move> NBchangeone; // contains neighbourhood changeone
	vector<note> adaptnotes;
	int divspm = info.divspm;

	for (unsigned int startsl = 0; startsl < (info.left.size()-(5*divspm)); startsl += (divspm*4)){

		for (int count = 0; count < frequency; count ++){
			NBchangeone.clear();
			for (unsigned int slice = startsl; slice < (startsl+(5*divspm)); slice++){
				for (int el = 0; el<5;el++){
					adaptnotes=allnotes; // copy note info to change fingering here
					int id = info.left[slice][el];
					if (id!=0){

						int finger = getFingering((id), allnotes); // actual finger
						for (int a = 1; a <= 5; a++){ // go through all fingers
							if (a!= finger) { // for the four other fingers:

								adaptnotes[(id-1)].fingering = a; // change fingering

								// check possibility
								bool possible = leftFeasibility(info, adaptnotes);

								if (possible==1){ // if change is feasible?
									changeone.diff = (leftScoreP(info, adaptnotes, startsl) - leftScoreP(info, allnotes, startsl)); // store difficulty
									changeone.finger = a; // store difficulty
									changeone.id = id; // store id
									NBchangeone.push_back(changeone); // capture neigbourhood
								}

							}
						}
					}
				}
			}
			// sorteer neighbourhood: laagste diff eerst.
			qsort(&NBchangeone[0], NBchangeone.size(), sizeof(Move), compareQS);

			//cout << NBchangeone[0].id << ", finger: " <<NBchangeone[0].finger << ", diff: " <<NBchangeone[0].diff<<"||";

			adaptnotes = allnotes;
			adaptnotes[((NBchangeone[0].id)-1)].fingering = NBchangeone[0].finger;

			// select first value (is lowest difficulty) and execute change, if easier than previous
			if (leftScoreP(info, allnotes, startsl) > leftScoreP(info, adaptnotes, startsl)) {
				allnotes[((NBchangeone[0].id)-1)].fingering = NBchangeone[0].finger;

				// output moveinfo
				clock_t final=clock()-init;
				cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << leftScore(info, allnotes) << ",	1_L (measure:" << (startsl/info.divspm) << ")" << "\n";

			}

			// stop als geen verbetering
			else {
				break;
			}

		}
	}

	int startsl = (info.left.size()-(5*divspm));

	for (int count = 0; count < frequency; count ++){
		NBchangeone.clear();
		for (int slice = startsl; slice < (startsl+(5*divspm)); slice++){
			for (int el = 0; el<5;el++){
				adaptnotes=allnotes; // copy note info to change fingering here
				int id = info.left[slice][el];
				if (id!=0){

					int finger = getFingering((id), allnotes); // actual finger
					for (int a = 1; a <= 5; a++){ // go through all fingers
						if (a!= finger) { // for the four other fingers:

							adaptnotes[(id-1)].fingering = a; // change fingering

							// check possibility
							bool possible = leftFeasibility(info, adaptnotes);

							if (possible==1){ // if change is feasible?
								changeone.diff = (leftScoreP(info, adaptnotes, startsl) - leftScoreP(info, allnotes, startsl)); // store difficulty
								changeone.finger = a; // store difficulty
								changeone.id = id; // store id
								NBchangeone.push_back(changeone); // capture neigbourhood
							}

						}
					}
				}
			}
		}
		// sorteer neighbourhood: laagste diff eerst.
		qsort(&NBchangeone[0], NBchangeone.size(), sizeof(Move), compareQS);

		adaptnotes = allnotes;
		adaptnotes[((NBchangeone[0].id)-1)].fingering = NBchangeone[0].finger;

		// select first value (is lowest difficulty) and execute change, if easier than previous
		if (leftScoreP(info, allnotes, startsl) > leftScoreP(info, adaptnotes, startsl)) {
			allnotes[((NBchangeone[0].id)-1)].fingering = NBchangeone[0].finger;

			// output moveinfo
			clock_t final=clock()-init;
			cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << leftScore(info, allnotes) << ",	1_L" << " (measure:" << (startsl/info.divspm) << ")" << "\n";

		}

		// stop als geen verbetering
		else {
			break;
		}

	}

	return leftScore (info, allnotes);
}
*/

/*
// first improving version
double rightChangeoneFirst (MusicInfo &info, vector<note> &allnotes, vector<int> &tabulist){
	//construct neighbourhood
	Move changeone; // elements of neighbourhood changeone
	vector<note> adaptnotes;

	for (unsigned int slicenr = 0; slicenr < info.right.size(); slicenr ++){


		for (int el = 0; el<5;el++){
			adaptnotes=allnotes; // copy note info to change fingering here
			int id = info.right[slicenr][el];
			if (id!=0){

				int finger = getFingering((id), allnotes); // actual finger
				for (int a = 1; a <= 5; a++){ // go through all fingers
					if (a!= finger) { // for the four other fingers:

						adaptnotes[(id-1)].fingering = a; // change fingering

						// check possibility
						bool possible = rightFeasibility(info, adaptnotes);

						//bepaal begin en eindslice voor gedeeltelijke score
						int prevlength = 0;
						int crlength = allnotes[(id-1)].duration;
						int nextlength = 0;

						for (int backskipper = allnotes[(id-1)].start-1; backskipper > 0; backskipper--){
							if (info.right[slicenr][0] != info.right[backskipper][0] || info.right[slicenr][1] != info.right[backskipper][1] ||
								info.right[slicenr][2] != info.right[backskipper][2] || info.right[slicenr][3] != info.right[backskipper][3] || info.right[slicenr][4] != info.right[backskipper][4]){
								prevlength = maximum(allnotes[(info.right[backskipper][0]-1)].duration, allnotes[(info.right[backskipper][1]-1)].duration, allnotes[(info.right[backskipper][2]-1)].duration, allnotes[(info.right[backskipper][3]-1)].duration, allnotes[(info.right[backskipper][4]-1)].duration);
								break;
							}
							else {continue;}
						}

						for (unsigned int skipper = allnotes[(id-1)].start+crlength; skipper < info.right.size(); skipper++){
							if (info.right[slicenr][0] != info.right[skipper][0] || info.right[slicenr][1] != info.right[skipper][1] ||
								info.right[slicenr][2] != info.right[skipper][2] || info.right[slicenr][3] != info.right[skipper][3] || info.right[slicenr][4] != info.right[skipper][4]){
								nextlength = maximum(allnotes[(info.right[skipper][0]-1)].duration, allnotes[(info.right[skipper][1]-1)].duration, allnotes[(info.right[skipper][2]-1)].duration, allnotes[(info.right[skipper][3]-1)].duration, allnotes[(info.right[skipper][4]-1)].duration);
								break;
							}
							else {continue;}
						}

						int start_vwd = slicenr - prevlength - crlength-1; // start die negatief kan worden
						int start = max (0, start_vwd); // beperk tot 0
						int end_vwd = slicenr + crlength + nextlength+1; // einde die voorbij einde kan gaan
						int sizeofright = info.right.size(); int end = min(sizeofright , end_vwd);  // beperkt tot einde

						if (possible==1){ // if change is feasible?
							changeone.diff = (rightScoreP(info, adaptnotes, start, end) - rightScoreP(info, allnotes, start, end)); // store difficulty
							//cout << "score verschllen"<< rightScoreP(info, adaptnotes, start, end) <<", "<< rightScoreP(info, allnotes, start, end) <<", "<< changeone.diff<< ", f: " << changeone.finger << ", id: " << changeone.id<<"\n";
							changeone.finger = a; // store difficulty
							changeone.id = id; // store id

							// select first improving value (diff<0) and execute change
							if (changeone.diff < 0){
								allnotes[((changeone.id)-1)].fingering = changeone.finger;
								//cout << "nieuwe: " << NBchangeone[0].finger<< "id" << NBchangeone[0].id;
								break;
							}
						}

					}
				}
			}
		}

	}

	return rightScore (info, allnotes);
}

double leftChangeoneFirst (MusicInfo &info, vector<note> &allnotes, vector<int> &tabulist){
	//construct neighbourhood
	Move changeone; // elements of neighbourhood changeone
	vector<note> adaptnotes;

	for (unsigned int slicenr = 0; slicenr < info.left.size(); slicenr ++){


		for (int el = 0; el<5;el++){
			adaptnotes=allnotes; // copy note info to change fingering here
			int id = info.left[slicenr][el];
			if (id!=0){

				int finger = getFingering((id), allnotes); // actual finger
				for (int a = 1; a <= 5; a++){ // go through all fingers
					if (a!= finger) { // for the four other fingers:

						adaptnotes[(id-1)].fingering = a; // change fingering

						// check possibility
						bool possible = leftFeasibility(info, adaptnotes);

						//bepaal begin en eindslice voor gedeeltelijke score
						int prevlength = 0;
						int crlength = allnotes[(id-1)].duration;
						int nextlength = 0;

						for (int backskipper = allnotes[(id-1)].start-1; backskipper > 0; backskipper--){
							if (info.left[slicenr][0] != info.left[backskipper][0] || info.left[slicenr][1] != info.left[backskipper][1] ||
								info.left[slicenr][2] != info.left[backskipper][2] || info.left[slicenr][3] != info.left[backskipper][3] || info.left[slicenr][4] != info.left[backskipper][4]){
								prevlength = maximum(allnotes[(info.left[backskipper][0]-1)].duration, allnotes[(info.left[backskipper][1]-1)].duration, allnotes[(info.left[backskipper][2]-1)].duration, allnotes[(info.left[backskipper][3]-1)].duration, allnotes[(info.left[backskipper][4]-1)].duration);
								break;
							}
							else {continue;}
						}

						for (unsigned int skipper = allnotes[(id-1)].start+crlength; skipper < info.left.size(); skipper++){
							if (info.left[slicenr][0] != info.left[skipper][0] || info.left[slicenr][1] != info.left[skipper][1] ||
								info.left[slicenr][2] != info.left[skipper][2] || info.left[slicenr][3] != info.left[skipper][3] || info.left[slicenr][4] != info.left[skipper][4]){
								nextlength = maximum(allnotes[(info.left[skipper][0]-1)].duration, allnotes[(info.left[skipper][1]-1)].duration, allnotes[(info.left[skipper][2]-1)].duration, allnotes[(info.left[skipper][3]-1)].duration, allnotes[(info.left[skipper][4]-1)].duration);
								break;
							}
							else {continue;}
						}

						int start_vwd = slicenr - prevlength - crlength-1; // start die negatief kan worden
						int start = max (0, start_vwd); // beperk tot 0
						int end_vwd = slicenr + crlength + nextlength+1; // einde die voorbij einde kan gaan
						int sizeofleft = info.left.size(); int end = min(sizeofleft , end_vwd);  // beperkt tot einde

						if (possible==1){ // if change is feasible?
							changeone.diff = (leftScoreP(info, adaptnotes, start, end) - leftScoreP(info, allnotes, start, end)); // store difficulty
							//cout << "score verschllen"<< leftScoreP(info, adaptnotes, start, end) <<", "<< leftScoreP(info, allnotes, start, end) <<", "<< changeone.diff<< ", f: " << changeone.finger << ", id: " << changeone.id<<"\n";
							changeone.finger = a; // store difficulty
							changeone.id = id; // store id

							// select first improving value (diff<0) and execute change
							if (changeone.diff < 0){
								allnotes[((changeone.id)-1)].fingering = changeone.finger;
								//cout << "nieuwe: " << NBchangeone[0].finger<< "id" << NBchangeone[0].id;
								break;
							}
						}

					}
				}
			}
		}

	}

	return leftScore (info, allnotes);
}
*/
