/*
 * main.cpp
 *
 *  Created on: 1-okt.-2013
 *      Author: Matteo
 */
#include <iostream>
#include <string.h>
#include <vector>
#include <algorithm>
#include <sstream>
#include "note.h"
#include <fstream>
#include "importMusicXML.h"
#include "exportMusicXML.h"
#include "MusicInfo.h"
#include "score.h"
#include "Move.h"
#include "Move2.h"
#include <time.h>
#include <sys/time.h>
#include "xmlParser.h"
#include "Move3.h"
#include "Move4.h"
#include "Move5.h"


using namespace std;

int main(int argc, char *argv[]) {

	//set here parameters for the experiment
	int typeofpert = 1; // 1 to perturbate best solution; 0 to perturbate current solution; 2 to make a random decision
	int nrofperturbations = 0; // number of perturbations after first loop w/o improvement.
	bool nbh_change = 1; // neighbourhood change on (1) or off (0)
	bool nbh_swap = 1; // neighbourhood swap on (1) or off (0)
	bool nbh_spart = 1; // neighbourhood spart on (1) or off (0)
	bool nbh_change2 = 1; // neigbourhood change2 on (1) or off (0)
	int nrofpartspert = 4; // hoeveel delen waarin perturbatie
	int percentagepert = 20; // Set % of slices that can be perturbated
	int tabupercentage = 1; // Set % of notes as size for tabu list
	bool tabupermove = 1; // use new tabu list for new move type (when set to 1)
	int firstdescent = 0; // First descend on (1) or off (0), using steepest descend then
	string bestand = "piece2.xml"; // contains file name
	bool experimentmode = false; // if true: then no cout print
	int allowedworsen = 200; // number of iterations as % of tabu size //number of iterations in tabu with worse result (MINIMAL = 1! ANDERS STEEDS JUMPEN!!)

	int tabupercent = tabupercentage * 5;

	//inlezen argementen
	int numarg=0;
	string argument;
	//cout << argc;
	while (numarg< argc) {
		argument = argv[numarg];
		if (argv[numarg][0] == '-') {
				string nextargument = argv[numarg + 1];

				if (argument == "-typeofpert") {
					typeofpert = atoi(nextargument.c_str());
				}
				else if (argument == "-nrofperturbations") {
					nrofperturbations = atoi(nextargument.c_str());
				}
				else if (argument == "-nbh_change") {
					nbh_change = atoi(nextargument.c_str());
				}
				else if (argument == "-nbh_swap") {
					nbh_swap = atoi(nextargument.c_str());
				}
				else if (argument == "-nbh_spart") {
					nbh_spart = atoi(nextargument.c_str());
				}
				else if (argument == "-nbh_change2") {
					nbh_change2 = atoi(nextargument.c_str());
				}
				else if (argument == "-nrofpartspert") {
					nrofpartspert = atoi(nextargument.c_str());
				}
				else if (argument == "-percentagepert") {
					percentagepert = atoi(nextargument.c_str());
				}
				else if (argument == "-tabupercentage") {
					tabupercentage = atoi(nextargument.c_str());
				}
				else if (argument == "-tabupermove") {
					tabupermove = atoi(nextargument.c_str());
				}
				else if (argument == "-firstdescent") {
					firstdescent = atoi(nextargument.c_str());
				}
				else if (argument == "-bestand") {
					bestand = nextargument.c_str();
				}
				else if (argument == "-experimentmode") {
					experimentmode = atoi(nextargument.c_str());
				}
				else if (argument == "-allowedworsener") {
					allowedworsen = atoi(nextargument.c_str());
				}
		}
		numarg++;
	}



//	cout << "typeofpert"<<  typeofpert << "nrofp" << nrofperturbations << "change"<< nbh_change << "swap"<< nbh_swap << "spart"<< nbh_spart	<< "nrofpartspert" << nrofpartspert << "%pert"<< percentagepert << "tabu%" << tabupercent <<"tabupm" << tabupermove << "firstd"<< firstdescent;
// looptijd links en rechts
	double runtimeR = 0;
	double runtimeL = 0;

	/*Random seed initialization - DO NOT DELETE.*/
	timeval t1;
	gettimeofday(&t1, NULL);
	srand(t1.tv_usec * t1.tv_sec);


	// probleem: todo divspermaat maatverandering.

	// inlezen bestand

//	cout << "enter filename.extension (e.g. bach.xml): ";
	//automatisch input mogelijk maken

	//bestand = "bach974part.xml";
//	getline (cin, bestand);
	//bestand = "Test.xml";

	/*
		// als stuk uit twee maattypes bestaat => vraag om op te splitsen.
		int morethanonemeasure = testOnemeasuretype (bestand);
		if (morethanonemeasure >1){cout << "This piece consists of different measure types." << "\n" << "Please split it up in different parts, one per measure type.";
			return 0;}
	 */

	vector<note> allnotes;
	//hier zitten alle noten in volgorde van xml in
	XMLNode NodeMain = importNode(bestand);
	allnotes = importMusicXML(NodeMain);
	int numberofnotes = allnotes.size();

	//count number of notes right and left
	//right
	int numberofrightnotes = 0;
	for (int loopallnotes = 0; loopallnotes < numberofnotes; loopallnotes++){
		if (allnotes[loopallnotes].hand == 'R'){
			numberofrightnotes++;
		}
	}

	// left
	int numberofleftnotes = 0;
	for (int loopallnotes = 0; loopallnotes < numberofnotes; loopallnotes++){
		if (allnotes[loopallnotes].hand == 'L'){
			numberofleftnotes++;
		}
	}



	// create tweede structuur (per hand: actieve noot-ID's in een bep. slice
	vector<vector<int> > right;
	vector<vector<int> > left;

	// 1 lege slice met 5 nullen.
	vector<int> slice (5,0);

	// multiply met aantal beschikbare slices (divs*measures) in een stuk.
	int measures ;
	measures = aantalmaten (bestand);

	int divspm;
	divspm = divpermaat (bestand);

	int slices = measures * divspm;

	//creatie vector right
	for (int z = 0; z < slices; z++){
		right.push_back(slice);}

	// identieke left
	left = right;

	//invullen van left en right, rij per rij
	for (int tel = 0; tel < numberofnotes; tel++){

		int id3 = allnotes[tel].id;
		int start3 = allnotes[tel].start;
		int duration3 = allnotes[tel].duration;

		if (allnotes[tel].hand == 'R'){
			for (int scountR = start3; scountR < (start3 + duration3); scountR++){
				for (int lineR = 0; lineR < 5; lineR++){
					if (right[scountR][lineR]!= 0){continue;}
					else {
						right[scountR][lineR] = id3;
						break;
					}
				}
			}
		}

		else if (allnotes[tel].hand == 'L'){
			for (int scountL = start3; scountL < (start3 + duration3); scountL++){
				for (int lineL = 0; lineL < 5; lineL++){
					if (left[scountL][lineL]!= 0){continue;}
					else {
						left[scountL][lineL] = id3;
						break;
					}
				}
			}
		}


	}


	// info apart in klasse steken
	MusicInfo info;
	info = fingerSpecs ();
	info.left = left;
	info.right = right;
	info.divspm = divspm;





	// random fingering toewijzen

	// nb: vectoren always sorted by ID's, and zero's follow for empty positions


	for (unsigned int slicenr=0;slicenr < right.size(); slicenr++){
		vector <int> used; // onthoudt welke vingerzetting reeds gebruikt is in een slice
		used.clear();
		for (int d=0; d<5; d++){
			int id4 = right[slicenr][d];
			if (id4 !=0 && allnotes[id4-1].fingering != 0){ // een bestaande noot die al een vingerzetting heeft: sla op in used
				int f = allnotes[id4-1].fingering;
				used.push_back(f);
			}
		}

		// nog geen vingerzetting (dus nog 0) => geef laagste, nog niet toegewezen vingerzetting in die slice
		int vinger = 1;
		for (int e=0; e<5; e++){
			int id5 = right[slicenr][e];
			if (id5 !=0 && allnotes[id5-1].fingering == 0){
				increaserR:
				int x = count(used.begin(), used.end(), vinger);
				if (x>0){
					vinger++;
					goto increaserR;
				}
				else {
					allnotes[id5-1].fingering = vinger;
					vinger++;
				}
			}
			else {continue;}
		}
	}

	// idem links
	for (unsigned int slicenr=0;slicenr < left.size(); slicenr++){
		vector <int> used;
		used.clear();
		for (int d=0; d<5; d++){
			int id4 = left[slicenr][d];
			if (id4 !=0 && allnotes[id4-1].fingering != 0){
				int f = allnotes[id4-1].fingering;
				used.push_back(f);
			}
		}

		int vinger = 1;
		for (int e=0; e<5; e++){
			int id5 = left[slicenr][e];
			if (id5 !=0 && allnotes[id5-1].fingering == 0){
				increaserL:
				int x = count(used.begin(), used.end(), vinger);
				if (x>0){
					vinger++;
					goto increaserL;
				}
				else {
					allnotes[id5-1].fingering = vinger;
					vinger++;
				}
			}
			else {continue;}
		}
	}


	/*// juiste ingeven bij "test.xml"
	allnotes[0].fingering = 3;
	allnotes[1].fingering = 2;
	allnotes[2].fingering = 1;
	allnotes[4].fingering = 4;
	allnotes[5].fingering = 2;
	allnotes[6].fingering = 3;
	allnotes[7].fingering = 4;
	allnotes[8].fingering = 2;
	allnotes[9].fingering = 1;
	allnotes[10].fingering = 5;*/


	//berekening rechter score en feasibility van initiele status:
	double rightscores = rightScore(info, allnotes);
	bool rightfeas = rightFeasibility(info, allnotes);

	//berekening linker score en feasibility:
	double leftscores = leftScore(info, allnotes);
	bool leftfeas = leftFeasibility(info, allnotes);

	//tabu list
	vector <vector<int> > tabulist;
	tabulist.clear(); // van nul vullen
	int tabutenure; // lengte wordt later per hand bepaald.

	vector<int> tabuforonenote;
	for (int fingernrs = 0 ; fingernrs < 5; fingernrs++){
		tabuforonenote.push_back(0);
	}

	for (int eachnote = 0 ; eachnote < numberofnotes; eachnote++){
		tabulist.push_back(tabuforonenote);
	}

	// generate random first solution!
	int lengthofchangedpart = slices; // zo: per part bepaald percentage wisselen
	int beginpert = 0;
	rightPerturbPart (beginpert, lengthofchangedpart-1, allnotes, info);
	leftPerturbPart (beginpert, lengthofchangedpart-1, allnotes, info);



	// algorithm

	//RIGHT

	// right tabu tenure
	tabutenure = tabupercent*numberofrightnotes/100;

	// iterations
	int allowedworsener = allowedworsen*tabutenure/100;
	// geen tabu? allowed worsener => 1
	if (tabupercentage == 0){allowedworsener =1;}

	// best solution ever;
	vector<note> bestnotes;

	// best solution in tabu
	vector<note> besttabunotes = allnotes;

	//count iteration for tabu
	int itcounttabu = 0;
	int worsener = 0;

	if (experimentmode == false){
	cout << "\n" << "Right algorithm: steps:" <<"\n" << "Time,	Score,	Algorithm,	Best:" << "\n";}

	clock_t init, final;
	init=clock();

	double bestscore= rightScore(info, allnotes); // track best score
	double newscore = rightScore(info, allnotes);
	double loopstartscore = rightScore(info, allnotes);
	double previousscore = rightScore(info, allnotes);
	double besttabuscore = rightScore(info, allnotes);

	if (nbh_swap ==1) {

		//move type 2: SWAP 2 FINGERS

		do{
			previousscore = rightScore(info, allnotes);
			newscore = rightSwapf (info, allnotes);
			bestscore = min(newscore,bestscore);

			// output moveinfo
			final=clock()-init;
			if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Swap_R,		" << bestscore << "\n";}

		} while (previousscore > newscore); // verbetering? anders stoppen
	}
	bestnotes = besttabunotes = allnotes;

	do{

		loopstartscore = rightScore(info, allnotes);
		newscore = rightScore(info, allnotes);

		//srand(time(NULL));
		bool change_first = rand() %2;

		if (nbh_spart ==1 && change_first == false) {
			// move type 4: SWAP 2 FINGERS IN PART

			/*//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				tabulist.clear();
				for (int i = 0; i < tabutenure; i++){
					tabulist.push_back(0);
				}
			}*/

			//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				itcounttabu = itcounttabu + tabutenure;
			}

			do{
				previousscore = rightScore(info, allnotes);
				newscore = rightSwapNote (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


				besttabuscore = rightScore(info, besttabunotes);
				if (besttabuscore > newscore) {
					besttabunotes = allnotes;
				}

				if (bestscore > newscore) {
					worsener = 0;
					bestscore = min(newscore,bestscore);
				}

				else {
					worsener++;
				}

				// output moveinfo
				final=clock()-init;
				if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	SPart_R,	" << bestscore << "\n";}


			} while (worsener < allowedworsener); // verbetering? anders stoppen
			worsener = 0;
		}
		allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

		if (nbh_change ==1) {
			//move type 1: CHANGE ONE

			/*//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				tabulist.clear();
				for (int i = 0; i < tabutenure; i++){
					tabulist.push_back(0);
				}
			}*/

			//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				itcounttabu = itcounttabu + tabutenure;
			}

			do{
				previousscore = rightScore(info, allnotes);
				newscore = rightChangeoneP (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


				besttabuscore = rightScore(info, besttabunotes);
				if (besttabuscore > newscore) {
					besttabunotes = allnotes;
				}

				if (bestscore > newscore) {
					worsener = 0;
					bestscore = min(newscore,bestscore);
				}

				else {
					worsener++;
				}

				// output moveinfo
				final=clock()-init;
				if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Change_R,	" << bestscore << "\n";}


			} while (worsener < allowedworsener); // verbetering? anders stoppen
			worsener = 0;
		}
		allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

		if (nbh_spart ==1 && change_first == true) {
			// move type 4: SWAP 2 FINGERS IN PART

			/*//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				tabulist.clear();
				for (int i = 0; i < tabutenure; i++){
					tabulist.push_back(0);
				}
			}*/

			//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				itcounttabu = itcounttabu + tabutenure;
			}

			do{
				previousscore = rightScore(info, allnotes);
				newscore = rightSwapNote (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);

				besttabuscore = rightScore(info, besttabunotes);
				if (besttabuscore > newscore) {
					besttabunotes = allnotes;
				}

				if (bestscore > newscore) {
					worsener = 0;
					bestscore = min(newscore,bestscore);
				}

				else {
					worsener++;
				}

				// output moveinfo
				final=clock()-init;
				if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	SPart_R,	" << bestscore << "\n";}


			} while (worsener < allowedworsener); // verbetering? anders stoppen
			worsener = 0;
		}
		allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

		if (nbh_change2 == 1){
			//move type 5: Change Two notes

			/*//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				tabulist.clear();
				for (int i = 0; i < tabutenure; i++){
					tabulist.push_back(0);
				}
			}*/

			//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				itcounttabu = itcounttabu + tabutenure;
			}

			do{
				previousscore = rightScore(info, allnotes);
				newscore = rightChangetwo (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


				besttabuscore = rightScore(info, besttabunotes);
				if (besttabuscore > newscore) {
					besttabunotes = allnotes;
				}

				if (bestscore > newscore) {
					worsener = 0;
					bestscore = min(newscore,bestscore);
				}

				else {
					worsener++;
				}

				// output moveinfo
				final=clock()-init;
				if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Change2_R,	" << bestscore << "\n";}


			} while (worsener < allowedworsener); // verbetering? anders stoppen
			worsener = 0;
		}
		allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

		newscore = rightScore(info,allnotes);
	} while (loopstartscore > newscore); // verbetering? anders stoppen

	// copy solution
	bestnotes = besttabunotes;


	for (int z = 0; z < nrofperturbations; z++){ // hoeveel iteraties ZONDER VERBETERING
		//perturbation: change a random part of the solution
		{

		int beginspert [nrofpartspert]; // bezit alle beginslices voor pert
		int lengthofchangedpart = slices/nrofpartspert*percentagepert/100; // zo: per part bepaald percentage wisselen
		int choicesetbeginpert = slices/nrofpartspert - lengthofchangedpart; // aantal mogelijke beginslices per deel

		//srand(time(NULL));
		for (int iterator = 0; iterator < nrofpartspert; iterator++){
			int randomstart = rand() % choicesetbeginpert;
			beginspert[iterator] = randomstart + iterator*slices/nrofpartspert;
		}

		for (int iterator = 0; iterator < nrofpartspert; iterator++){
			int beginpert = beginspert[iterator];
			newscore = rightPerturbPart (beginpert, beginpert + lengthofchangedpart, allnotes, info);
			bestscore = min(bestscore,newscore);
		}

		final=clock()-init;
		if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Pert_R,		" << bestscore << "\n";}
		}  // end perturbation

		if (nbh_swap ==1) { // first improvement
			//move type: SWAP 2 FINGERS

			do{
				previousscore = rightScore(info, allnotes);
				newscore = rightSwapf (info, allnotes);
				bestscore = min(newscore,bestscore);

				// output moveinfo
				final=clock()-init;
				if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Swap_R,		" << bestscore << "\n";}

			} while (previousscore > newscore); // verbetering? anders stoppen
		}
		besttabunotes = allnotes;

		//algoritme opnieuw

		do{

			loopstartscore = rightScore(info, allnotes);

			//right

			//srand(time(NULL));
			bool change_first = rand() %2;

			if (nbh_spart ==1 && change_first == false) {
				//move type 4: SWAP 2 FINGERS IN PART

				/*//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					tabulist.clear();
					for (int i = 0; i < tabutenure; i++){
						tabulist.push_back(0);
					}
				}*/

				//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					itcounttabu = itcounttabu + tabutenure;
				}

				do{
					previousscore = rightScore(info, allnotes);
					newscore = rightSwapNote (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


					besttabuscore = rightScore(info, besttabunotes);
					if (besttabuscore > newscore) {
						besttabunotes = allnotes;
					}

					if (bestscore > newscore) {
						worsener = 0;
						bestscore = min(newscore,bestscore);
					}

					else {
						worsener++;
					}

					// output moveinfo
					final=clock()-init;
					if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	SPart_R,	" << bestscore << "\n";}


				} while (worsener < allowedworsener); // verbetering? anders stoppen
				worsener = 0;
			}
			allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

			if (nbh_change ==1) {
				//move type 1: CHANGE ONE

				/*//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					tabulist.clear();
					for (int i = 0; i < tabutenure; i++){
						tabulist.push_back(0);
					}
				}*/

				//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					itcounttabu = itcounttabu + tabutenure;
				}

				do{
					previousscore = rightScore(info, allnotes);
					newscore = rightChangeoneP (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);

					besttabuscore = rightScore(info, besttabunotes);
					if (besttabuscore > newscore) {
						besttabunotes = allnotes;
					}

					if (bestscore > newscore) {
						worsener = 0;
						bestscore = min(newscore,bestscore);
					}

					else {
						worsener++;
					}

					// output moveinfo
					final=clock()-init;
					if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Change_R,	" << bestscore << "\n";}

				} while (worsener < allowedworsener); // verbetering? anders stoppen
				worsener = 0;
			}
			allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

			if (nbh_spart ==1 && change_first == true) {
				//move type 4: SWAP 2 FINGERS IN PART

				/*//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					tabulist.clear();
					for (int i = 0; i < tabutenure; i++){
						tabulist.push_back(0);
					}
				}*/

				//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					itcounttabu = itcounttabu + tabutenure;
				}

				do{
					previousscore = rightScore(info, allnotes);
					newscore = rightSwapNote (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


					besttabuscore = rightScore(info, besttabunotes);
					if (besttabuscore > newscore) {
						besttabunotes = allnotes;
					}

					if (bestscore > newscore) {
						worsener = 0;
						bestscore = min(newscore,bestscore);
					}

					else {
						worsener++;
					}

					// output moveinfo
					final=clock()-init;
					if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	SPart_R,	" << bestscore << "\n";}


				} while (worsener < allowedworsener); // verbetering? anders stoppen
				worsener = 0;
			}
			allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

			if (nbh_change2 == 1){
				//move type 5: Change Two notes

				/*//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					tabulist.clear();
					for (int i = 0; i < tabutenure; i++){
						tabulist.push_back(0);
					}
				}*/

				//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					itcounttabu = itcounttabu + tabutenure;
				}

				do{
					previousscore = rightScore(info, allnotes);
					newscore = rightChangetwo (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


					besttabuscore = rightScore(info, besttabunotes);
					if (besttabuscore > newscore) {
						besttabunotes = allnotes;
					}

					if (bestscore > newscore) {
						worsener = 0;
						bestscore = min(newscore,bestscore);
					}


					else {
						worsener++;
					}


					// output moveinfo
					final=clock()-init;
					if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Change2_R,	" << bestscore << "\n";}


				} while (worsener < allowedworsener); // verbetering? anders stoppen
				worsener = 0;
			}
			allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

			newscore = rightScore(info,allnotes);
		} while (loopstartscore > newscore); // verbetering? anders stoppen

		if(rightScore(info,besttabunotes)<rightScore(info,bestnotes)){ // als deze loop een verbetering geeft: bijhouden
			bestnotes = besttabunotes;
			z=-1; // bij verbetering: restart number of iterations
		}

		// perturbatie op beste igv "1", anders current
		//srand(time(NULL));
		int randnr = rand() %2;

		if (typeofpert == 1 or (typeofpert == 2 and randnr == 1)){
		allnotes = bestnotes;}

	}

	final=clock()-init;
	runtimeR = ((double)final / ((double)CLOCKS_PER_SEC));

	// op einde allnotes bewaren en andere vrijmaken
	allnotes = bestnotes;
	bestnotes.clear();


	//LEFT

	// left tabu tenure
	tabutenure = tabupercent*numberofleftnotes/100;

	// iterations
	allowedworsener = allowedworsen*tabutenure/100;
	// geen tabu? allowed worsener => 1
	if (tabupercentage == 0){allowedworsener =1;}

	//reset tabu  iteration counter
	itcounttabu = 0;

	if (experimentmode == false){cout << "\n" << "Left algorithm: steps:" <<"\n" << "Time,	Score,	Algorithm,	Best:" << "\n";}

	init=clock();

	// re�nit tabulist for left hand
	tabulist.clear(); // van nul vullen

	for (int eachnote = 0 ; eachnote < numberofnotes; eachnote++){
		tabulist.push_back(tabuforonenote);
	}


	bestscore= leftScore(info, allnotes); // track best score
	newscore = leftScore(info, allnotes);
	loopstartscore = leftScore(info, allnotes);
	previousscore = leftScore(info, allnotes);
	besttabuscore = leftScore(info, allnotes);

	if (nbh_swap ==1) {

		// move type 2: SWAP 2 FINGERS
		do{
			previousscore = leftScore(info, allnotes);
			newscore = leftSwapf (info, allnotes);
			bestscore = min(newscore,bestscore);

			// output moveinfo
			final=clock()-init;
			if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Swap_L,		" << bestscore << "\n";}

		} while (previousscore > newscore); // verbetering? anders stoppen
	}
	bestnotes = besttabunotes = allnotes;

	do{

		loopstartscore = leftScore(info, allnotes);


		//srand(time(NULL));
		bool change_first = rand() %2;

		if (nbh_spart ==1 && change_first == false) {
			//move type 4: SWAP 2 FINGERS IN PART

			/*//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				tabulist.clear();
				for (int i = 0; i < tabutenure; i++){
					tabulist.push_back(0);
				}
			}*/

			//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				itcounttabu = itcounttabu + tabutenure;
			}

			do{
				previousscore = leftScore(info, allnotes);
				newscore = leftSwapNote (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


				besttabuscore = leftScore(info, besttabunotes);
				if (besttabuscore > newscore) {
					besttabunotes = allnotes;
				}

				if (bestscore > newscore) {
					worsener = 0;
					bestscore = min(newscore,bestscore);
				}

				else {
					worsener++;
				}

				// output moveinfo
				final=clock()-init;
				if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	SPart_L,	" << bestscore << "\n";}


			} while (worsener < allowedworsener); // verbetering? anders stoppen
			worsener = 0;
		}
		allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

		if (nbh_change ==1) {
			//move type 1: CHANGE ONE

			/*//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				tabulist.clear();
				for (int i = 0; i < tabutenure; i++){
					tabulist.push_back(0);
				}
			}*/

			//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				itcounttabu = itcounttabu + tabutenure;
			}

			do{
				previousscore = leftScore(info, allnotes);
				newscore = leftChangeoneP (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);



				besttabuscore = leftScore(info, besttabunotes);
				if (besttabuscore > newscore) {
					besttabunotes = allnotes;
				}

				if (bestscore > newscore) {
					worsener = 0;
					bestscore = min(newscore,bestscore);
				}

				else {
					worsener++;
				}

				// output moveinfo
				final=clock()-init;
				if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Change_L,	" << bestscore << "\n";}


			} while (worsener < allowedworsener); // verbetering? anders stoppen
			worsener = 0;
		}
		allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

		if (nbh_spart ==1 && change_first == true) {
			//move type 4: SWAP 2 FINGERS IN PART

			/*//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				tabulist.clear();
				for (int i = 0; i < tabutenure; i++){
					tabulist.push_back(0);
				}
			}*/

			//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				itcounttabu = itcounttabu + tabutenure;
			}

			do{
				previousscore = leftScore(info, allnotes);
				newscore = leftSwapNote (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


				besttabuscore = leftScore(info, besttabunotes);
				if (besttabuscore > newscore) {
					besttabunotes = allnotes;
				}

				if (bestscore > newscore) {
					worsener = 0;
					bestscore = min(newscore,bestscore);
				}

				else {
					worsener++;
				}

				// output moveinfo
				final=clock()-init;
				if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	SPart_L,	" << bestscore << "\n";}


			} while (worsener < allowedworsener); // verbetering? anders stoppen
			worsener = 0;
		}
		allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

		if (nbh_change2 == 1){
			//move type 5: Change Two notes

			/*//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				tabulist.clear();
				for (int i = 0; i < tabutenure; i++){
					tabulist.push_back(0);
				}
			}*/

			//reset tabu list, als nieuwe per move nodig is
			if (tabupermove == 1 && tabutenure != 0){
				itcounttabu = itcounttabu + tabutenure;
			}

			do{
				previousscore = leftScore(info, allnotes);
				newscore = leftChangetwo (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


				besttabuscore = leftScore(info, besttabunotes);
				if (besttabuscore > newscore) {
					besttabunotes = allnotes;
				}

				if (bestscore > newscore) {
					worsener = 0;
					bestscore = min(newscore,bestscore);
				}

				else {
					worsener++;
				}

				// output moveinfo
				final=clock()-init;
				if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Change2_L,	" << bestscore << "\n";}


			} while (worsener < allowedworsener); // verbetering? anders stoppen
			worsener = 0;
		}
		allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

		newscore = leftScore(info,allnotes);
	} while (loopstartscore > newscore);

	// copy solution
	bestnotes = besttabunotes;


	for (int z = 0; z < nrofperturbations; z++){ // hoeveel iteraties ZONDER VERBETERING
		//perturbation: change a random part of the solution
		{

		int beginspert [nrofpartspert]; // bezit alle beginslices voor pert
		int lengthofchangedpart = slices/nrofpartspert*percentagepert/100; // zo: per part bepaald percentage wisselen
		int choicesetbeginpert = slices/nrofpartspert - lengthofchangedpart; // aantal mogelijke beginslices per deel

		//srand(time(NULL));
		for (int iterator = 0; iterator < nrofpartspert; iterator++){
			int randomstart = rand() % choicesetbeginpert;
			beginspert[iterator] = randomstart + iterator*slices/nrofpartspert;
		}



		for (int iterator = 0; iterator < nrofpartspert; iterator++){
			int beginpert = beginspert[iterator];
			newscore = leftPerturbPart (beginpert, beginpert + lengthofchangedpart, allnotes, info);
			bestscore = min(bestscore,newscore);
		}

		final=clock()-init;
		if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Pert_L,		" << bestscore << "\n";}
		}  // end perturbation

		if (nbh_swap ==1) {
			//move type 2: SWAP 2 FINGERS

			do{
				previousscore = leftScore(info, allnotes);
				newscore = leftSwapf (info, allnotes);
				bestscore = min(newscore,bestscore);

				// output moveinfo
				final=clock()-init;
				if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Swap_L,		" << bestscore << "\n";}

			} while (previousscore > newscore); // verbetering? anders stoppen
		}
		besttabunotes = allnotes;

		//algoritme opnieuw

		do{


			loopstartscore = leftScore(info, allnotes);

			//left

			//srand(time(NULL));
			bool change_first = rand() %2;

			if (nbh_spart ==1 && change_first == false) {
				//move type 4: SWAP 2 FINGERS IN PART

				/*//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					tabulist.clear();
					for (int i = 0; i < tabutenure; i++){
						tabulist.push_back(0);
					}
				}*/

				//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					itcounttabu = itcounttabu + tabutenure;
				}

				do{
					previousscore = leftScore(info, allnotes);
					newscore = leftSwapNote (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);

					besttabuscore = leftScore(info, besttabunotes);
					if (besttabuscore > newscore) {
						besttabunotes = allnotes;
					}

					if (bestscore > newscore) {
						worsener = 0;
						bestscore = min(newscore,bestscore);
					}

					else {
						worsener++;
					}

					// output moveinfo
					final=clock()-init;
					if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	SPart_L,	" << bestscore << "\n";}


				} while (worsener < allowedworsener); // verbetering? anders stoppen
				worsener = 0;
			}
			allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

			if (nbh_change ==1) {
				//move type 1: CHANGE ONE

				/*//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					tabulist.clear();
					for (int i = 0; i < tabutenure; i++){
						tabulist.push_back(0);
					}
				}*/

				//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					itcounttabu = itcounttabu + tabutenure;
				}

				do{
					previousscore = leftScore(info, allnotes);
					newscore = leftChangeoneP (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


					besttabuscore = leftScore(info, besttabunotes);
					if (besttabuscore > newscore) {
						besttabunotes = allnotes;
					}

					if (bestscore > newscore) {
						worsener = 0;
						bestscore = min(newscore,bestscore);
					}

					else {
						worsener++;
					}

					// output moveinfo
					final=clock()-init;
					if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Change_L,	" << bestscore << "\n";}


				} while (worsener < allowedworsener); // verbetering? anders stoppen
				worsener = 0;
			}
			allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

			if (nbh_spart ==1 && change_first == true) {
				//move type 4: SWAP 2 FINGERS IN PART

				/*//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					tabulist.clear();
					for (int i = 0; i < tabutenure; i++){
						tabulist.push_back(0);
					}
				}*/

				//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					itcounttabu = itcounttabu + tabutenure;
				}

				do{
					previousscore = leftScore(info, allnotes);
					newscore = leftSwapNote (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


					besttabuscore = leftScore(info, besttabunotes);
					if (besttabuscore > newscore) {
						besttabunotes = allnotes;
					}

					if (bestscore > newscore) {
						worsener = 0;
						bestscore = min(newscore,bestscore);
					}

					else {
						worsener++;
					}

					// output moveinfo
					final=clock()-init;
					if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	SPart_L,	" << bestscore << "\n";}


				} while (worsener < allowedworsener); // verbetering? anders stoppen
				worsener = 0;
			}
			allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

			if (nbh_change2 == 1){
				//move type 5: Change Two notes

				/*//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					tabulist.clear();
					for (int i = 0; i < tabutenure; i++){
						tabulist.push_back(0);
					}
				}*/

				//reset tabu list, als nieuwe per move nodig is
				if (tabupermove == 1 && tabutenure != 0){
					itcounttabu = itcounttabu + tabutenure;
				}

				do{
					previousscore = leftScore(info, allnotes);
					newscore = leftChangetwo (info, allnotes, tabulist, firstdescent, itcounttabu, tabutenure);


					besttabuscore = leftScore(info, besttabunotes);
					if (besttabuscore > newscore) {
						besttabunotes = allnotes;
					}

					if (bestscore > newscore) {
						worsener = 0;
						bestscore = min(newscore,bestscore);
					}

					else {
						worsener++;
					}

					// output moveinfo
					final=clock()-init;
					if (experimentmode == false){cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	Change2_L,	" << bestscore << "\n";}


				} while (worsener < allowedworsener); // verbetering? anders stoppen
				worsener = 0;
			}
			allnotes = besttabunotes; // onthoudt na nbh de beste die hij daarin in totaal gevonden heeft

			newscore = leftScore(info,allnotes);
		} while (loopstartscore > newscore);

		if(leftScore(info,besttabunotes)<leftScore(info,bestnotes)){ // als deze loop een verbetering geeft: bijhouden
			bestnotes = besttabunotes;
			z=-1; // bij verbetering: restart number of iterations
		}

		// perturbatie op beste igv "1", anders current
		//srand(time(NULL));
		int randnr = rand() %2;

		if (typeofpert == 1 or (typeofpert == 2 and randnr == 1)){
		allnotes = bestnotes;}

	}

	final=clock()-init;
	runtimeL = ((double)final / ((double)CLOCKS_PER_SEC));

	// op einde allnotes bewaren en andere vrijmaken
	allnotes = bestnotes;
	bestnotes.clear();

	/* fout oorspronkelijk
	//3e move: test all feasible combinations in 1 measure
	for (int maat = 0; maat < measures; maat++){
		int newscore = leftSwapMeasure (maat*divspm, (maat*divspm+divspm), allnotes, info);

		final=clock()-init;
		cout << ((double)final / ((double)CLOCKS_PER_SEC)) << ",	" << newscore << ",	3_L" << "\n";
	}*/

	//handmatig fingering ingeven
	/*
	allnotes[x].fingering = x;
	 */

	//berekening rechter score en feasibility:
	rightscores = rightScore(info, allnotes);
	rightfeas = rightFeasibility(info, allnotes);

	//berekening linker score en feasibility:
	leftscores = leftScore(info, allnotes);
	leftfeas = leftFeasibility(info, allnotes);

	// output

	// display number of notes and all attributes

	if (experimentmode == false){cout << "\n" << "Number of notes: "<< numberofnotes<< "\n" ;}

	if (experimentmode == false){
		cout << "Index of notes: " << "\n" "ID, Pitch, Start, Duration, Hand, Voice, Fingering:" << "\n";
		for(int z = 0; z < numberofnotes; z++){
			cout << allnotes[z].id <<",	" << allnotes[z].pitch <<",	" << allnotes[z].start <<",	" << allnotes[z].duration <<",	" << allnotes[z].hand <<",	" << allnotes[z].voice <<",	" << allnotes[z].fingering << "\n";
		}


		// display slices right active notes (b = positie in slice, a = aantal slices)
		cout << "\n" << "Active note-ID's in right hand:" << "\n";
		for (int b = 0; b<5;b++){
			for (unsigned int a = 0; a<info.right.size(); a++){
				cout << info.right[a][b] << ",	";
			}
			cout << "\n";
		}


		/*
		// display right getransponeerd
		for (unsigned int a = 0; a<info.right.size(); a++){
			for (int b = 0; b<5;b++){
				cout << info.right[a][b] << ",	";
			}
			cout << "\n";
		}
		 */

		// display left active notes
		cout << "\n" << "Active note-ID's in left hand:" << "\n";
		for (int b = 0; b<5;b++){
			for (unsigned int a = 0; a<info.left.size(); a++){
				cout << info.left[a][b] << ",	";
			}
			cout << "\n";
		}

		// display right and left difficulty of fingering

		cout << "\n";
		cout << "Difficulty of right: "<< rightscores << ". Feasibility (1 = yes): " << rightfeas << "\n";

		cout << "Difficulty of left: " << leftscores << ". Feasibility (1 = yes): " << leftfeas << "\n";
	}

	if (experimentmode == false){
		// open the XML file again (and get info into XMLNode: NodeMain, because importMusicXML empties NodeMain)
		NodeMain = importNode(bestand);
		//write to output file
		exportMusicXML (allnotes, NodeMain, bestand);
	}

	if (experimentmode == true){

		//open the output_experiment.txt file to store info on the experiment
		ofstream outfile;
		outfile.open("output_experiment.txt", ios::app);

		//outfile << "typeofpert;	nrofperturbations;	nbh_change;	nbh_swap;	nbh_spart;	nrofpartspert;	percentagepert;	tabupercent;	tabupermove;	firstdescent;	runtimeR;	runtimeL;	scoreR;		scoreL";
		double runtimeT = runtimeR + runtimeL;
		double totscores = rightscores + leftscores;
		outfile << "\n" << typeofpert << "	" <<	nrofperturbations << "	" <<	nbh_change << "	" <<	nbh_swap << "	" <<	nbh_spart << "	" <<	nbh_change2 << "	" <<	nrofpartspert << "	" <<	percentagepert << "	" <<	tabupercentage << "	" <<	tabupermove << "	" <<	allowedworsen	<< "	" <<	firstdescent << "	" <<	runtimeR << "	" <<	runtimeL << "	" <<	rightscores << "	" <<	leftscores << "	" <<	runtimeT << "	" <<	totscores  << "	" <<	bestand;

		outfile.close();
	}

	return 0;
}

