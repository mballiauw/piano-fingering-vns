/*
 * importMusicXML.h
 *
 *  Created on: 9-okt.-2013
 *      Author: Matteo
 */
#include "note.h"
#include <vector>
#include <string.h>
#include "xmlParser.h"

#ifndef EXPORTMUSICXML_H_
#define EXPORTMUSICXML_H_

using namespace std;

void exportMusicXML(vector<note> &allnotes, XMLNode &NodeMain, string &bestand);

#endif /* EXPORTMUSICXML_H_ */
