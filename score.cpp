/*
 * score.cpp
 *
 *  Created on: 22 Oct 2013
 *      Author: dorien
 */

#include "score.h"
#include <vector>
#include <string>
#include <iostream>
#include <string.h>
#include <fstream>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <algorithm>
#include <sstream>
#include "note.h"
#include <fstream>
#include "importMusicXML.h"
#include "MusicInfo.h"
#include "assert.h"


using namespace std;

int getPitch (unsigned int id, vector<note> &name) {
	assert (id >=1 && id <= name.size());
	int p;
	p = name[id-1].pitch;
	return (p);
}

int getFingering (unsigned int id, vector<note> &name) {
	assert (id >=1 && id <= name.size());
	int f;
	f = name[id-1].fingering;
	return (f);
}

int comparePitch (const void * a, const void * b) {
	if ( (*(Notepart*)a).pitch <  (*(Notepart*)b).pitch) {return -1;}
	else if ( (*(Notepart*)a).pitch == (*(Notepart*)b).pitch  ) {return 0;}
	//else if ( (*(Move*)a).diff >  (*(Move*)b).diff ) {return 1;}
	else {return 1;}
}

/*int countdecidedfingering (int slice, vector<vector<int> > hand, vector<note> database) { // number of already fingered notes in a slice
	int v = 0;
	for (int a =0; a<5; a++){
		if (hand[slice][a]!=0){
			int f = getfingering (hand[slice][a], database);
			if (f!=0){v++;}
			else if (f==0) {continue;}
		}
	}
	return v;
}*/

double rightScore(MusicInfo &info, vector<note> &allnotes){

	// berekening doelfunctie rechts (voor hele stuk)

	double righttargetfunction = 0;
	double righthorizontal = 0;
	double rightvertical = 0;
	double rightuseoffour = 0;
	double rightthreefour = 0;
	double rightthumbswitch = 0;
	double rightblackthumb = 0;
	double rightblackfifth = 0;
	double righthandmove = 0;
	double righthoronethree = 0;
	double rightquick = 0;
	double rightdouble = 0;

	double value = 1; // hoe erg is de vierde vinger?

	// regels 1, 2, 14
	for (unsigned int a = 0; a < (info.right.size()-1); a++){ // vanaf eerste slice t.e.m. voorlaatste:
		for (int n = 0; n < 5; n++){
			// voor een niet-nul ID, die niet in volgende slice zit, pitch en fingering ophalen
			if (info.right[a][n]!=0 && count(info.right[a+1].begin(), info.right[a+1].end(), info.right[a][n])==0){
				int P1 = getPitch(info.right[a][n], allnotes);
				int F1 = (getFingering(info.right[a][n], allnotes)-1); // meteen juiste positie in array[][]
				for (int m = 0; m < 5; m++){
					// in volgende slice kijken: niet-nul ID en niet in vorige slice aanwezig
					if (info.right[a+1][m]!=0 && count(info.right[a].begin(), info.right[a].end(), info.right[a+1][m])==0){
						int P2 = getPitch(info.right[a+1][m], allnotes);
						int F2 = (getFingering(info.right[a+1][m], allnotes)-1); // meteen juiste positie in array[][]
						int pdist = P2-P1; // bereken afstand tussen pitches = afstand tssn toetsen (per constructie)

						// afstand kleiner dan MinRel => check ook Comf en Prac
						if (pdist < info.MinRel[F1][F2]) {
							righthorizontal = righthorizontal + (info.MinRel[F1][F2]-pdist);
							if (pdist < info.MinComf[F1][F2]) {
								righthorizontal = righthorizontal + 2*(info.MinComf[F1][F2]-pdist);
								if (pdist < info.MinPrac[F1][F2]) {
									righthorizontal = righthorizontal + 10*(info.MinPrac[F1][F2]-pdist);
								}
							}
						}
						// afstand groter dan MaxRel => check ook Comf en Prac
						else if (pdist > info.MaxRel[F1][F2]) {
							righthorizontal = righthorizontal + (pdist - info.MaxRel[F1][F2]);
							if (pdist > info.MaxComf[F1][F2]) {
								righthorizontal = righthorizontal + 2*(pdist - info.MaxComf[F1][F2]);
								if (pdist > info.MaxPrac[F1][F2]) {
									righthorizontal = righthorizontal + 10*(pdist - info.MaxPrac[F1][F2]);
								}
							}
						}
						else {continue;}

					}
				}
			}
		}
	}

	// regel 15 (binnen een slice: afstanden)
	{	// slice 0
		unsigned int a = 0;
		// eerste 4 noten checken: niet nul
			for (int n = 0; n < 4; n++){
				if (info.right[a][n]!=0){
					// pitch en fingering ophalen
				int P1 = getPitch(info.right[a][n], allnotes);
				int F1 = (getFingering(info.right[a][n], allnotes)-1);
				// vergelijk met volgende noten binnen slice
				for (int m = n+1; m < 5; m++){
					if (info.right[a][m]!=0){
					int P2 = getPitch(info.right[a][m], allnotes);
					int F2 = (getFingering(info.right[a][m], allnotes)-1);
					int pdist = P2-P1;

					if (pdist < info.MinRel[F1][F2]) {
						// zelfde afstanden, maar MinRel dubbel gewicht geven
						rightvertical = rightvertical +2*(info.MinRel[F1][F2]-pdist);
						if (pdist < info.MinComf[F1][F2]) {
							rightvertical = rightvertical + 4*(info.MinComf[F1][F2]-pdist);
							if (pdist < info.MinPrac[F1][F2]) {
								rightvertical = rightvertical + 10*(info.MinPrac[F1][F2]-pdist);
							}
						}
					}
					else if (pdist > info.MaxRel[F1][F2]) {
						rightvertical = rightvertical + 2*(pdist - info.MaxRel[F1][F2]);
						if (pdist > info.MaxComf[F1][F2]) {
							rightvertical = rightvertical + 4*(pdist - info.MaxComf[F1][F2]);
							if (pdist > info.MaxPrac[F1][F2]) {
								rightvertical = rightvertical + 10*(pdist - info.MaxPrac[F1][F2]);
							}
						}
					}
					else {continue;}
				}}
			}
		}

			// vanaf slice 2: als een slice niet gelijk is aan vorige (dus 1 positie verschilt): bereken opnieuw binnen slice
		for ( a = 1; a < info.right.size(); a++){
			if (info.right[a][0]!=info.right[a-1][0] || info.right[a][1]!=info.right[a-1][1] || info.right[a][2]!=info.right[a-1][2] || info.right[a][3]!=info.right[a-1][3] || info.right[a][4]!=info.right[a-1][4]){
				for (int n = 0; n < 4; n++){
					if (info.right[a][n]!=0){
					int P1 = getPitch(info.right[a][n], allnotes);
					int F1 = (getFingering(info.right[a][n], allnotes)-1);
					for (int m = n+1; m < 5; m++){
						if (info.right[a][m]!=0){
						int P2 = getPitch(info.right[a][m], allnotes);
						int F2 = (getFingering(info.right[a][m], allnotes)-1);
						int pdist = P2-P1;

						if (pdist < info.MinRel[F1][F2]) {
							rightvertical = rightvertical + 2*(info.MinRel[F1][F2]-pdist);
							if (pdist < info.MinComf[F1][F2]) {
								rightvertical = rightvertical + 4*(info.MinComf[F1][F2]-pdist);
								if (pdist < info.MinPrac[F1][F2]) {
									rightvertical = rightvertical + 10*(info.MinPrac[F1][F2]-pdist);
								}
							}
						}
						else if (pdist > info.MaxRel[F1][F2]) {
							rightvertical = rightvertical + 2*(pdist - info.MaxRel[F1][F2]);
							if (pdist > info.MaxComf[F1][F2]) {
								rightvertical = rightvertical + 4*(pdist - info.MaxComf[F1][F2]);
								if (pdist > info.MaxPrac[F1][F2]) {
									rightvertical = rightvertical + 10*(pdist - info.MaxPrac[F1][F2]);
								}
							}
						}
						else {continue;}
					}}
				}}
			}
		}
	}

	// regel 5 (use of 4 (monotone): bestraf met 1 pt)
	for (unsigned int n = 0; n<(info.right.size()); n++){
		if (count(info.right[n].begin(), info.right[n].end(), 0)==4){
			if (getFingering(info.right[n][0],allnotes)==4 && allnotes[(info.right[n][0])-1].hand == 'R'){
				rightuseoffour ++;
			}
		}
	}

	// regel 7 en 8 (als 3 en 4 naast elkaar komen: +1) vervolgens: witte 3, zwarte 4:  + 1 extra
	for (unsigned int n = 0; n<(info.right.size()-1); n++){
		// voorwaarde: monofoon: 4 nullen in een slice
		if (count(info.right[n].begin(), info.right[n].end(), 0)==4 && count(info.right[n+1].begin(), info.right[n+1].end(), 0)==4 && info.right[n][0]!=info.right[n+1][0]){
			if (getFingering(info.right[n][0],allnotes) ==3 && getFingering(info.right[n+1][0],allnotes)==4){
				rightthreefour++;
				if ((getPitch(info.right[n][0],allnotes) % 2) == 1 && (getPitch(info.right[n+1][0],allnotes) % 2) == 0){
					rightthreefour++;
				}
			}
			else if (getFingering(info.right[n][0],allnotes) ==4 && getFingering(info.right[n+1][0],allnotes)==3){
				rightthreefour++;
				if ((getPitch(info.right[n][0],allnotes) % 2) == 0 && (getPitch(info.right[n+1][0],allnotes) % 2) == 1){
					rightthreefour++;
				}
			}
		}
	}

	// regel 11 en 12 (duimwissel)
	for (unsigned int n = 1; n<(info.right.size()); n++){
		if (count(info.right[n].begin(), info.right[n].end(), 0)==4){
			int currentf = getFingering(info.right[n][0],allnotes);
			if(count(info.right[n-1].begin(), info.right[n-1].end(), 0)==4 && currentf ==1){ // monofone duim
				int previousf = getFingering(info.right[n-1][0],allnotes);
				int previousp = getPitch(info.right[n-1][0],allnotes); // info vorige slice
				int currentp = getPitch(info.right[n][0],allnotes);
				int nextf = 0, nextp = 0;

				if ((n+1)<info.right.size() && count(info.right[n+1].begin(), info.right[n+1].end(), 0)==4){ // als de volgende slice bestaat
					nextf = getFingering(info.right[n+1][0],allnotes);
					nextp = getPitch(info.right[n+1][0],allnotes);
				}

				// als vorige of volgende noot niet met 1 gespeeld wordt en een lagere pitch heeft dan de huidige:
				if(count(info.right[n-1].begin(), info.right[n-1].end(), 0)==4 && currentp>previousp && previousf!=1){
					if ((currentp % 2) == (previousp % 2)){ // zelfde niveau
						rightthumbswitch++;
					}
					else if ((currentp % 2)==0 && (previousp % 2)==1 ){ // duim op zwart, andere op wit
						rightthumbswitch = rightthumbswitch + 2;
					}
				}

				if((n+1)<info.right.size()){
					if(count(info.right[n+1].begin(), info.right[n+1].end(), 0)==4 && currentp>nextp && nextf!=1){
						if ((currentp % 2) == (nextp % 2)){
							rightthumbswitch++;
						}
						else if ((currentp % 2)==0 && (nextp % 2)==1 ){
							rightthumbswitch = rightthumbswitch + 2;
						}
					}
				}
			}
		}
	}

	// monotone regels over 3 noten
	for (unsigned int n = 0; n<(info.right.size()-2); n++){ // ga hele stuk af (tot en met 2 voor einde)
		if (info.right[n][0]!=0 && count(info.right[n].begin(), info.right[n].end(), 0)==4
				&& info.right[n+1][0]!=0 && count(info.right[n+1].begin(), info.right[n+1].end(), 0)==4 // monotoon
				&& info.right[n][0]!=info.right[n+1][0]) { // verschillende slices
			int pitch_a = getPitch(info.right[n][0], allnotes); //1st note
			int finger_a = (getFingering(info.right[n][0], allnotes)); // fingering correct position in info_matrices
			int pitch_b = getPitch(info.right[n+1][0], allnotes); //2nd note
			int finger_b = (getFingering(info.right[n+1][0], allnotes)); // fingering correct position in info_matrices
			int pitch_c =0;
			int finger_c =0; //3rd note
			int d = 2;
			bool three = false;
			// zoek ook noot 3
			for(unsigned int a =(n+2); a<info.right.size();a++){
				if (count(info.right[a].begin(), info.right[a].end(), 0)==4 && d < 3) {
					if (info.right[a][0]!=info.right[n+1][0] && info.right[n][0]!=0){ // niet nul en verschillend van 2e
						pitch_c = getPitch(info.right[a][0], allnotes); //3rd note
						finger_c = (getFingering(info.right[a][0], allnotes)); // fingering correct position in info_matrices
						d++;
						three = true;
					}
				}
				else {break;}
			}

			//regel 9 (1 op zwart)
			if (three == true && finger_b == 1 && (pitch_b % 2) == 0) { //+1
				rightblackthumb ++;
				if (finger_a != 1 && (pitch_a % 2) == 1){ // vorige noot wit? +2
					rightblackthumb ++; rightblackthumb ++;
				}
				if (finger_c != 1 && (pitch_c % 2) == 1){ // volgende noot wit +2
					rightblackthumb ++; rightblackthumb ++;
				}
			}

			//regel 10 (5 op zwart)
			if (three == true && finger_b == 5 && (pitch_b % 2) == 0) {
				if (finger_a != 5 && (pitch_a % 2) == 1){ // vorige noot wit? +2
					rightblackfifth ++; rightblackfifth ++;
				}
				if (finger_c != 5 && (pitch_c % 2) == 1){ // volgende noot wit +2
					rightblackfifth ++; rightblackfifth ++;
				}
			}

			int pdistca = pitch_c - pitch_a; //distance between 1st and 3rd  note
			//regel 3
			if (three == true){
				if (pdistca > info.MaxComf[(finger_a-1)][(finger_c-1)] || pdistca < info.MinComf[(finger_a-1)][(finger_c-1)]){
					righthandmove ++; // afstand 1e en 3e te groot => half hand move => +1
					if (finger_b == 1 && ( ( (pitch_a-pitch_b)>0 && (pitch_b-pitch_c)>0 ) || ( (pitch_a-pitch_b)<0 && (pitch_b-pitch_c)<0 ) ) ){ // if 2 add. conditions: 2nd finger = 1 and pitch b between a and c : one additional point (together with previous => +2)
						righthandmove ++;
					}
				}
				// complete half finger change rule: for 1st or 3rd note played with thumb: if pitch is same and finger is different: +1
				if (finger_a == 1 || finger_c==1 ){
					if (pitch_a == pitch_c && finger_a != finger_c){
						righthandmove ++;
					}
				}
			}

			// eigen regel 13: als eerste en derde noot verschillen, maar zelfde vinger en tweede noot ertussen ligt: +1.
			if (three == true && ( ( (pitch_a-pitch_b)>0 && (pitch_b-pitch_c)>0 ) || ( (pitch_a-pitch_b)<0 && (pitch_b-pitch_c)<0 ) ) && finger_a == finger_c){
				rightquick ++; // afstand 1e en 3e te groot => half hand move => +1
			}


			//regel 4: dist (1-3) > MaxComf or dist (1-3) < MinComf => + 1 per eenheid
			if (three == true){
				if (pdistca < info.MinComf[(finger_a-1)][(finger_c-1)]) {
					righthoronethree = righthoronethree + (info.MinComf[(finger_a-1)][(finger_c-1)]-pdistca);
				}
				else if (pdistca > info.MaxComf[(finger_a-1)][(finger_c-1)]) {
					righthoronethree = righthoronethree + (pdistca - info.MaxComf[(finger_a-1)][(finger_c-1)]);
				}
			}

		}
	}

	// eigen regel (weglaten): -1 als duim gebruikt wordt op eerste tel
	int rightfirstthumb = 0;
	for (unsigned int n = 0; n<(info.right.size()); n++){ // ga hele stuk af
		if ((n % info.divspm)==0){
			for (int a = 0; a < 5; a++){
				if(info.right[n][a]!= 0){
					if(getFingering(info.right[n][a], allnotes)==1){
						rightfirstthumb--;
					}
				}
			}
		}
		else {continue;}
	}

	// regel 16
	for (unsigned int a = 0; a < (info.right.size()-1); a++){ // vanaf eerste slice t.e.m. voorlaatste:
		// zoek verschillende slices (dus met andere noten (ID) ), maar de pitches moeten wel 1op1 gelijk zijn (dus ook gelijk aantal nullen nodig)
		if ((info.right[a][0]!=info.right[a+1][0] || info.right[a][1]!=info.right[a+1][1] || info.right[a][2]!=info.right[a+1][2] || info.right[a][3]!=info.right[a+1][3] || info.right[a][4]!=info.right[a+1][4]) && count(info.right[a].begin(),info.right[a].end(),0) ==  count(info.right[a+1].begin(),info.right[a+1].end(),0) ){
			// lijst de pitches op in huidige slice, en in volgende => 1 op 1 gelijk ==> test fingering ook 1 op 1 gelijk
			Notepart notepart;
			vector<Notepart> slicecur, slicenext;
			slicecur.clear();
			slicenext.clear();

			// get pitches and fingerings for slice a
			for (int b = 0; b<5; b++){
				if (info.right[a][b]!=0){
					notepart.pitch = getPitch(info.right[a][b], allnotes);
					notepart.finger = getFingering(info.right[a][b], allnotes);
					slicecur.push_back(notepart);
				}
			}

			// get pitches and fingerings for slice a+1
			for (int b = 0; b<5; b++){
				if (info.right[a+1][b]!=0){
					notepart.pitch = getPitch(info.right[a+1][b], allnotes);
					notepart.finger = getFingering(info.right[a+1][b], allnotes);
					slicenext.push_back(notepart);
				}
			}

			//qsort slicecur en slicenext op pitch
			qsort(&slicecur[0], slicecur.size(), sizeof(Notepart), comparePitch);
			qsort(&slicenext[0], slicenext.size(), sizeof(Notepart), comparePitch);

			//vergelijk pitches, eis 1 op 1 gelijk
			vector <int> thesame;
			thesame.clear();
			for (unsigned int t = 0; t < slicecur.size(); t++){
				if (slicecur[t].pitch == slicenext[t].pitch){
					thesame.push_back(1);
				}
				else {thesame.push_back(0);}
			}
			if (count (thesame.begin(),thesame.end(),0)==0) {
				for (unsigned int u = 0; u < slicecur.size(); u++){
					if (slicecur[u].finger != slicenext[u].finger){
						rightdouble++;
					}
				}
			}

		}
	}

	// moeten elementen van score nog in vector komen? vector<int> scores
	// 6, 7, ... (score per regel, dan optellen) => gebeurt hier reeds via ints.

	//alle elementen van score optellen

righttargetfunction = righthorizontal + rightvertical + 0*value*rightuseoffour + value*rightthreefour + rightthumbswitch + rightblackthumb/2 + rightblackfifth/2 + righthandmove + rightquick + righthoronethree + 0*rightfirstthumb/2 + rightdouble;
return righttargetfunction;

}

double leftScore (MusicInfo &info, vector<note> &allnotes){

// berekening doelfunctie links (voor hele stuk)
	// berekeningen: rechts MaxPrac[1][2] = links MaxPrac [2][1]

	// comments: zie rightScore

	double lefttargetfunction = 0;
	double lefthorizontal = 0;
	double leftvertical = 0;
	double leftuseoffour = 0;
	double leftthreefour = 0;
	double leftthumbswitch = 0;
	double leftblackthumb = 0;
	double leftblackfifth = 0;
	double lefthandmove = 0;
	double lefthoronethree = 0;
	double leftquick = 0;
	double leftdouble = 0;

	double value = 1; // hoe erg is 4e vinger?

	// regels 1, 2, 14
	for (unsigned int a = 0; a < (info.left.size()-1); a++){
		for (int n = 0; n < 5; n++){
			if (info.left.at(a).at(n)!=0 && count(info.left.at(a+1).begin(), info.left.at(a+1).end(), info.left.at(a).at(n))==0){
				int P1 = getPitch(info.left.at(a).at(n), allnotes);
				int F3 = (getFingering(info.left.at(a).at(n), allnotes)-1); // meteen juiste positie in array[][]
				for (int m = 0; m < 5; m++){
					if (info.left.at(a+1).at(m)!=0 && count(info.left.at(a).begin(), info.left.at(a).end(), info.left.at(a+1).at(m))==0){
						int P2 = getPitch(info.left.at(a+1).at(m), allnotes);
						int F4 = (getFingering(info.left.at(a+1).at(m), allnotes)-1); // meteen juiste positie in array[][]
						int pdist = P2-P1;

						if (pdist < info.MinRel[F4][F3]) {
							lefthorizontal = lefthorizontal + (info.MinRel[F4][F3]-pdist);
							if (pdist < info.MinComf[F4][F3]) {
								lefthorizontal = lefthorizontal + 2*(info.MinComf[F4][F3]-pdist);
								if (pdist < info.MinPrac[F4][F3]) {
									lefthorizontal = lefthorizontal + 10*(info.MinPrac[F4][F3]-pdist);
								}
							}
						}
						else if (pdist > info.MaxRel[F4][F3]) {
							lefthorizontal = lefthorizontal + (pdist - info.MaxRel[F4][F3]);
							if (pdist > info.MaxComf[F4][F3]) {
								lefthorizontal = lefthorizontal + 2*(pdist - info.MaxComf[F4][F3]);
								if (pdist > info.MaxPrac[F4][F3]) {
									lefthorizontal = lefthorizontal + 10*(pdist - info.MaxPrac[F4][F3]);
								}
							}
						}
						else {continue;}

					}
				}
			}
		}
	}

	// regel 15 (in slice) => x2: important to have a playable chord
	{	unsigned int a = 0;
			for (int n = 0; n < 4; n++){
				if (info.left.at(a).at(n)!=0){
				int P1 = getPitch(info.left.at(a).at(n), allnotes);
				int F3 = (getFingering(info.left.at(a).at(n), allnotes)-1);
				for (int m = n+1; m < 5; m++){
					if (info.left.at(a).at(m)!=0){
					int P2 = getPitch(info.left.at(a).at(m), allnotes);
					int F4 = (getFingering(info.left.at(a).at(m), allnotes)-1);
					int pdist = P2-P1;

					if (pdist < info.MinRel[F4][F3]) {
						leftvertical = leftvertical + 2*(info.MinRel[F4][F3]-pdist);
						if (pdist < info.MinComf[F4][F3]) {
							leftvertical = leftvertical + 4*(info.MinComf[F4][F3]-pdist);
							if (pdist < info.MinPrac[F4][F3]) {
								leftvertical = leftvertical + 10*(info.MinPrac[F4][F3]-pdist);
							}
						}
					}
					else if (pdist > info.MaxRel[F4][F3]) {
						leftvertical = leftvertical + 2*(pdist - info.MaxRel[F4][F3]);
						if (pdist > info.MaxComf[F4][F3]) {
							leftvertical = leftvertical + 4*(pdist - info.MaxComf[F4][F3]);
							if (pdist > info.MaxPrac[F4][F3]) {
								leftvertical = leftvertical + 10*(pdist - info.MaxPrac[F4][F3]);
							}
						}
					}
					else {continue;}
				}
			}}
		}

		for ( a = 1; a < info.left.size(); a++){
			if (info.left.at(a).at(0)!=info.left.at(a-1).at(0) || info.left.at(a).at(1)!=info.left.at(a-1).at(1) || info.left.at(a).at(2)!=info.left.at(a-1).at(2) || info.left.at(a).at(3)!=info.left.at(a-1).at(3) || info.left.at(a).at(4)!=info.left.at(a-1).at(4)){
				for (int n = 0; n < 4; n++){
					if (info.left.at(a).at(n)!=0){
					int P1 = getPitch(info.left.at(a).at(n), allnotes);
					int F3 = (getFingering(info.left.at(a).at(n), allnotes)-1);
					for (int m = n+1; m < 5; m++){
						if (info.left.at(a).at(m)!=0){
						int P2 = getPitch(info.left.at(a).at(m), allnotes);
						int F4 = (getFingering(info.left.at(a).at(m), allnotes)-1);
						int pdist = P2-P1;

						if (pdist < info.MinRel[F4][F3]) {
							leftvertical = leftvertical + 2*(info.MinRel[F4][F3]-pdist);
							if (pdist < info.MinComf[F4][F3]) {
								leftvertical = leftvertical + 4*(info.MinComf[F4][F3]-pdist);
								if (pdist < info.MinPrac[F4][F3]) {
									leftvertical = leftvertical + 10*(info.MinPrac[F4][F3]-pdist);
								}
							}
						}
						else if (pdist > info.MaxRel[F4][F3]) {
							leftvertical = leftvertical + 2*(pdist - info.MaxRel[F4][F3]);
							if (pdist > info.MaxComf[F4][F3]) {
								leftvertical = leftvertical + 4*(pdist - info.MaxComf[F4][F3]);
								if (pdist > info.MaxPrac[F4][F3]) {
									leftvertical = leftvertical + 10*(pdist - info.MaxPrac[F4][F3]);
								}
							}
						}
						else {continue;}
					}}
				}}
			}
		}
	}

	// regel 5 (use of 4) monofoon
	for (unsigned int n = 0; n<(info.left.size()); n++){
		if (count(info.left.at(n).begin(), info.left.at(n).end(), 0)==4){
			if( getFingering(info.left.at(n).at(0),allnotes)==4 && allnotes.at(info.left.at(n).at(0)-1).hand == 'L'){
				leftuseoffour ++;
			}
		}
	}

	// regel 7 en 8 (3-4; zwart-wit)
	for (unsigned int n = 0; n<(info.left.size()-1); n++){
		if (count(info.left.at(n).begin(), info.left.at(n).end(), 0)==4 && count(info.left.at(n+1).begin(), info.left.at(n+1).end(), 0)==4 && info.left.at(n).at(0)!=info.left.at(n+1).at(0)){
			if (getFingering(info.left.at(n).at(0),allnotes) ==3 && getFingering(info.left.at(n+1).at(0),allnotes)==4){
				leftthreefour++;
				if ((getPitch(info.left.at(n).at(0),allnotes) % 2) == 1 && (getPitch(info.left.at(n+1).at(0),allnotes) % 2) == 0){
					leftthreefour++;
				}
			}

			else if (getFingering(info.left.at(n).at(0),allnotes) ==4 && getFingering(info.left.at(n+1).at(0),allnotes)==3){
				leftthreefour++;
				if ((getPitch(info.left.at(n).at(0),allnotes) % 2) == 0 && (getPitch(info.left.at(n+1).at(0),allnotes) % 2) == 1){
					leftthreefour++;
				}
			}
		}
	}

	// regel 11 en 12 (duimwissel)
	for (unsigned int n = 1; n<(info.left.size()); n++){
		if(count(info.left.at(n).begin(), info.left.at(n).end(), 0)==4){
			int currentf = getFingering(info.left.at(n).at(0),allnotes);
			if(count(info.left.at(n-1).begin(), info.left.at(n-1).end(), 0)==4 && currentf ==1){
				int previousf = getFingering(info.left.at(n-1).at(0),allnotes);
				int previousp = getPitch(info.left.at(n-1).at(0),allnotes);
				int currentp = getPitch(info.left.at(n).at(0),allnotes);
				int nextf = 0, nextp = 0;

				if ((n+1)<info.left.size() && count(info.left.at(n+1).begin(), info.left.at(n+1).end(), 0)==4){
					nextf = getFingering(info.left.at(n+1).at(0),allnotes);
					nextp = getPitch(info.left.at(n+1).at(0),allnotes);
				}

				// hier als vorige of volgende hoger is dan huidige (die met 1 gespeeld w), en niet met 1 gespeeld wordt.
				if(count(info.left.at(n-1).begin(), info.left.at(n-1).end(), 0)==4 && currentp<previousp && previousf!=1){
					if ((currentp % 2) == (previousp % 2)){
						leftthumbswitch++;
					}
					else if ((currentp % 2)==0 && (previousp % 2)==1 ){
						leftthumbswitch = leftthumbswitch + 2;
					}
				}

				if ((n+1)<info.left.size()){
					if(count(info.left.at(n+1).begin(), info.left.at(n+1).end(), 0)==4 && currentp<nextp && nextf!=1){
						if ((currentp % 2) == (nextp % 2)){
							leftthumbswitch++;
						}
						else if ((currentp % 2)==0 && (nextp % 2)==1 ){
							leftthumbswitch = leftthumbswitch + 2;
						}
					}
				}
			}
		}
	}

	// monotone regels over 3 noten
		for (unsigned int n = 0; n<(info.left.size()-2); n++){ // ga hele stuk af (tot en met 2 voor einde)
			if (info.left.at(n).at(0)!=0 && count(info.left.at(n).begin(), info.left.at(n).end(), 0)==4
					&& info.left.at(n+1).at(0)!=0 && count(info.left.at(n+1).begin(), info.left.at(n+1).end(), 0)==4 // monotoon
					&& info.left.at(n).at(0)!=info.left.at(n+1).at(0)) { // verschillende slices
				int pitch_a = getPitch(info.left.at(n).at(0), allnotes); //1st note
				int finger_a = (getFingering(info.left.at(n).at(0), allnotes)); // fingering correct position in info_matrices
				int pitch_b = getPitch(info.left.at(n+1).at(0), allnotes); //2nd note
				int finger_b = (getFingering(info.left.at(n+1).at(0), allnotes)); // fingering correct position in info_matrices
				int pitch_c =0;
				int finger_c =0; //3rd note
				int d = 2;
				bool three = false;
				// zoek ook noot 3
				for(unsigned int a =(n+2); a<info.left.size();a++){
					if (count(info.left.at(a).begin(), info.left.at(a).end(), 0)==4 && d < 3) {
						if (info.left.at(a).at(0)!=info.left.at(n+1).at(0) && info.left.at(n).at(0)!=0){ // niet nul en verschillend van 2e
							pitch_c = getPitch(info.left.at(a).at(0), allnotes); //3rd note
							finger_c = (getFingering(info.left.at(a).at(0), allnotes)); // fingering correct position in info_matrices
							d++;
							three = true;
						}
					}
					else {break;}
				}

				//regel 9 (1 op zwart)
				if (three == true && finger_b == 1 && (pitch_b % 2) == 0) { //+1
					leftblackthumb ++;
					if (finger_a != 1 && (pitch_a % 2) == 1){ // vorige noot wit? +2
						leftblackthumb ++; leftblackthumb ++;
					}
					if (finger_c != 1 && (pitch_c % 2) == 1){ // volgende noot wit +2
						leftblackthumb ++; leftblackthumb ++;
					}
				}

				//regel 10 (5 op zwart)
				if (three == true && finger_b == 5 && (pitch_b % 2) == 0) {
					if (finger_a != 5 && (pitch_a % 2) == 1){ // vorige noot wit? +2
						leftblackfifth ++; leftblackfifth ++;
					}
					if (finger_c != 5 && (pitch_c % 2) == 1){ // volgende noot wit +2
						leftblackfifth ++; leftblackfifth ++;
					}
				}

				int pdistca = pitch_c - pitch_a; //distance between 1st and 3rd  note
				//regel 3
				if (three == true){
					if (pdistca > info.MaxComf[(finger_c-1)][(finger_a-1)] || pdistca < info.MinComf[(finger_c-1)][(finger_a-1)]){
						lefthandmove ++; // afstand 1e en 3e te groot => half hand move => +1
						if (finger_b == 1 && ( ( (pitch_a-pitch_b)>0 && (pitch_b-pitch_c)>0 ) || ( (pitch_a-pitch_b)<0 && (pitch_b-pitch_c)<0 ) ) ){ // if 2 add. conditions: 2nd finger = 1 and pitch b between a and c : one additional point (together with previous => +2)
							lefthandmove ++;
						}
					}
					// complete half finger change rule: for 1st or 3rd note played with thumb: if pitch is same and finger is different: +1
					if (finger_a == 1 || finger_c==1 ){
						if (pitch_a == pitch_c && finger_a != finger_c){
							lefthandmove ++;
						}
					}
				}

				// eigen regel 13: als eerste en derde noot verschillen, maar zelfde vinger en tweede noot ertussen ligt: +1.
				if (three == true && ( ( (pitch_a-pitch_b)>0 && (pitch_b-pitch_c)>0 ) || ( (pitch_a-pitch_b)<0 && (pitch_b-pitch_c)<0 ) ) && finger_a == finger_c){
					leftquick ++; // afstand 1e en 3e te groot => half hand move => +1
				}

				//regel 4: dist (1-3) > MaxComf or dist (1-3) < MinComf => + 1 per eenheid
				if (three == true){
					if (pdistca < info.MinComf[(finger_c-1)][(finger_a-1)]) {
						lefthoronethree = lefthoronethree + (info.MinComf[(finger_c-1)][(finger_a-1)]-pdistca);
					}
					else if (pdistca > info.MaxComf[(finger_c-1)][(finger_a-1)]) {
						lefthoronethree = lefthoronethree + (pdistca - info.MaxComf[(finger_c-1)][(finger_a-1)]);
					}
				}
			}
		}

		// eigen regel (weglaten): -1 als duim gebruikt wordt op eerste tel
		int leftfirstthumb = 0;
		for (unsigned int n = 0; n<(info.left.size()); n++){ // ga hele stuk af
			if ((n % info.divspm)==0){
				for (int a = 0; a < 5; a++){
					if(info.left.at(n).at(a)!= 0){
						if( getFingering(info.left.at(n).at(a), allnotes)==1){
							leftfirstthumb--;
						}
					}
				}
			}
			else {continue;}
		}


		// regel 16
		for (unsigned int a = 0; a < (info.left.size()-1); a++){ // vanaf eerste slice t.e.m. voorlaatste:
			// zoek verschillende slices (dus met andere noten (ID) ), maar de pitches moeten wel 1op1 gelijk zijn (dus ook gelijk aantal nullen nodig)
			if ((info.left.at(a).at(0)!=info.left.at(a+1).at(0) || info.left.at(a).at(1)!=info.left.at(a+1).at(1) || info.left.at(a).at(2)!=info.left.at(a+1).at(2) || info.left.at(a).at(3)!=info.left.at(a+1).at(3) || info.left.at(a).at(4)!=info.left.at(a+1).at(4)) && count(info.left.at(a).begin(),info.left.at(a).end(),0) ==  count(info.left.at(a+1).begin(),info.left.at(a+1).end(),0) ){
				// lijst de pitches op in huidige slice, en in volgende => 1 op 1 gelijk ==> test fingering ook 1 op 1 gelijk
				Notepart notepart;
				vector<Notepart> slicecur, slicenext;
				slicecur.clear();
				slicenext.clear();

				// get pitches and fingerings for slice a
				for (int b = 0; b<5; b++){
					if (info.left.at(a).at(b)!=0){
						notepart.pitch = getPitch(info.left.at(a).at(b), allnotes);
						notepart.finger = getFingering(info.left.at(a).at(b), allnotes);
						slicecur.push_back(notepart);
					}
				}

				// get pitches and fingerings for slice a+1
				for (int b = 0; b<5; b++){
					if (info.left.at(a+1).at(b)!=0){
						notepart.pitch = getPitch(info.left.at(a+1).at(b), allnotes);
						notepart.finger = getFingering(info.left.at(a+1).at(b), allnotes);
						slicenext.push_back(notepart);
					}
				}

				//qsort slicecur en slicenext op pitch
				qsort(&slicecur.at(0), slicecur.size(), sizeof(Notepart), comparePitch);
				qsort(&slicenext.at(0), slicenext.size(), sizeof(Notepart), comparePitch);

				//vergelijk pitches, eis 1 op 1 gelijk
				vector <int> thesame;
				thesame.clear();
				for (unsigned int t = 0; t < slicecur.size(); t++){
					if (slicecur.at(t).pitch == slicenext.at(t).pitch){
						thesame.push_back(1);
					}
					else {thesame.push_back(0);}
				}
				if (count (thesame.begin(),thesame.end(),0)==0) {
					for (unsigned int u = 0; u < slicecur.size(); u++){
						if (slicecur.at(u).finger != slicenext.at(u).finger){
							leftdouble++;
						}
					}
				}

			}
		}

		// moeten elementen van score nog in vector komen? vector<int> scores
	// 6, 7, ... (score per regel, dan optellen) => gebeurt hier reeds via ints.

	//alle elementen van score optellen

	lefttargetfunction = lefthorizontal + leftvertical + 0*value*leftuseoffour + value*leftthreefour + leftthumbswitch + leftquick + leftblackthumb/2 + leftblackfifth/2 + lefthandmove + lefthoronethree + 0*leftfirstthumb/2 + leftdouble;
	return lefttargetfunction;

	}

bool rightFeasibility(MusicInfo &info, vector<note> &allnotes){
	bool feas;
	vector <bool> slicefeas; // per slice 1 of 0 voor feasibility opslaan in vector
	bool slicefea; // value for feasibility per slice
	vector <short int> slicefingers; // fingering per slice
	slicefeas.clear();
	for (unsigned int a = 0; a < info.right.size(); a++){ // per slice evaluation
		slicefingers.clear();
		for (int n = 0; n < 5; n++){
			if (info.right[a][n]!=0){ // if note id !=0 => store fingering in vector
				short int finger = getFingering(info.right[a][n], allnotes);
				slicefingers.push_back(finger);
			}
		}
		// if every finger is used once (or less) = that slice is feasible and a value of 1 is pushed in vector slicefeas
		if (count(slicefingers.begin(), slicefingers.end(), 1)<=1 && count(slicefingers.begin(), slicefingers.end(), 2)<=1 && count(slicefingers.begin(), slicefingers.end(), 2)<=1 && count(slicefingers.begin(), slicefingers.end(), 3)<=1 && count(slicefingers.begin(), slicefingers.end(), 4)<=1 && count(slicefingers.begin(), slicefingers.end(), 5)<=1 ){
			slicefea = 1;
			slicefeas.push_back(slicefea);
		}
		// else: one finger is used more than once: zero is pushed in vector
		else {
			slicefea = 0;
			slicefeas.push_back(slicefea);
		}
	}

	// if no zero's (unfeas) in any slice: fingering is feasible => return 1; else return 0
	if (count (slicefeas.begin(), slicefeas.end(), 0)==0){
		feas = 1;
	}
	else {
		feas = 0;
	}
	return feas; // 1 als feasible
}


bool leftFeasibility(MusicInfo &info, vector<note> &allnotes){
	bool feas;
	vector <bool> slicefeas; // per slice 1 of 0 voor feasibility opslaan in vector
	bool slicefea; // value for feasibility per slice
	vector <short int> slicefingers; // fingering per slice
	slicefeas.clear();
	for (unsigned int aa = 0; aa < info.left.size(); aa++){ // per slice evaluation
		slicefingers.clear();
		for (int n = 0; n < 5; n++){
			if (info.left[aa][n]!=0){ // if note id !=0 => store fingering in vector
				short int finger = getFingering(info.left[aa][n], allnotes);
				slicefingers.push_back(finger);
			}
		}
		// if every finger is used once (or less) = that slice is feasible and a value of 1 is pushed in vector slicefeas
		if (count(slicefingers.begin(), slicefingers.end(), 1)<=1 && count(slicefingers.begin(), slicefingers.end(), 2)<=1 && count(slicefingers.begin(), slicefingers.end(), 2)<=1 && count(slicefingers.begin(), slicefingers.end(), 3)<=1 && count(slicefingers.begin(), slicefingers.end(), 4)<=1 && count(slicefingers.begin(), slicefingers.end(), 5)<=1 ){
			slicefea = 1;
			slicefeas.push_back(slicefea);
		}
		// else: one finger is used more than once: zero is pushed in vector
		else {
			slicefea = 0;
			slicefeas.push_back(slicefea);
		}
	}

	// if no zero's (unfeas) in any slice: fingering is feasible => return 1; else return 0
	if (count (slicefeas.begin(), slicefeas.end(), 0)==0){
		feas = 1;
	}
	else {
		feas = 0;
	}
	return feas; // 1 als feasible
}



/****
 * Zelfde score berekening, maar nu slechts voor een gedeelte.
 */

double rightScoreP (MusicInfo &info, vector<note> &allnotes, int start, int end){

	// berekening doelfunctie rechts (voor hele stuk)

	double righttargetfunction = 0;
	double righthorizontal = 0;
	double rightvertical = 0;
	double rightuseoffour = 0;
	double rightthreefour = 0;
	double rightthumbswitch = 0;
	double rightblackthumb = 0;
	double rightblackfifth = 0;
	double righthandmove = 0;
	double righthoronethree = 0;
	double rightquick = 0;
	double rightdouble = 0;

	double value = 1; // hoe erg is de vierde vinger?

	// regels 1, 2, 14
	for (int a = start; a < (end-1); a++){ // vanaf eerste slice t.e.m. voorlaatste:
		for (int n = 0; n < 5; n++){
			// voor een niet-nul ID, die niet in volgende slice zit, pitch en fingering ophalen
			if (info.right[a][n]!=0 && count(info.right[a+1].begin(), info.right[a+1].end(), info.right[a][n])==0){
				int P1 = getPitch(info.right[a][n], allnotes);
				int F1 = (getFingering(info.right[a][n], allnotes)-1); // meteen juiste positie in array[][]
				for (int m = 0; m < 5; m++){
					// in volgende slice kijken: niet-nul ID en niet in vorige slice aanwezig
					if (info.right[a+1][m]!=0 && count(info.right[a].begin(), info.right[a].end(), info.right[a+1][m])==0){
						int P2 = getPitch(info.right[a+1][m], allnotes);
						int F2 = (getFingering(info.right[a+1][m], allnotes)-1); // meteen juiste positie in array[][]
						int pdist = P2-P1; // bereken afstand tussen pitches = afstand tssn toetsen (per constructie)

						// afstand kleiner dan MinRel => check ook Comf en Prac
						if (pdist < info.MinRel[F1][F2]) {
							righthorizontal = righthorizontal + (info.MinRel[F1][F2]-pdist);
							if (pdist < info.MinComf[F1][F2]) {
								righthorizontal = righthorizontal + 2*(info.MinComf[F1][F2]-pdist);
								if (pdist < info.MinPrac[F1][F2]) {
									righthorizontal = righthorizontal + 10*(info.MinPrac[F1][F2]-pdist);
								}
							}
						}
						// afstand groter dan MaxRel => check ook Comf en Prac
						else if (pdist > info.MaxRel[F1][F2]) {
							righthorizontal = righthorizontal + (pdist - info.MaxRel[F1][F2]);
							if (pdist > info.MaxComf[F1][F2]) {
								righthorizontal = righthorizontal + 2*(pdist - info.MaxComf[F1][F2]);
								if (pdist > info.MaxPrac[F1][F2]) {
									righthorizontal = righthorizontal + 10*(pdist - info.MaxPrac[F1][F2]);
								}
							}
						}
						else {continue;}

					}
				}
			}
		}
	}

	// regel 15 (binnen een slice: afstanden)
	{	// slice 0
		int a = start;
		// eerste 4 noten checken: niet nul
			for (int n = 0; n < 4; n++){
				if (info.right[a][n]!=0){
					// pitch en fingering ophalen
				int P1 = getPitch(info.right[a][n], allnotes);
				int F1 = (getFingering(info.right[a][n], allnotes)-1);
				// vergelijk met volgende noten binnen slice
				for (int m = n+1; m < 5; m++){
					if (info.right[a][m]!=0){
					int P2 = getPitch(info.right[a][m], allnotes);
					int F2 = (getFingering(info.right[a][m], allnotes)-1);
					int pdist = P2-P1;

					if (pdist < info.MinRel[F1][F2]) {
						// zelfde afstanden, maar MinRel dubbel gewicht geven
						rightvertical = rightvertical +2*(info.MinRel[F1][F2]-pdist);
						if (pdist < info.MinComf[F1][F2]) {
							rightvertical = rightvertical + 4*(info.MinComf[F1][F2]-pdist);
							if (pdist < info.MinPrac[F1][F2]) {
								rightvertical = rightvertical + 10*(info.MinPrac[F1][F2]-pdist);
							}
						}
					}
					else if (pdist > info.MaxRel[F1][F2]) {
						rightvertical = rightvertical + 2*(pdist - info.MaxRel[F1][F2]);
						if (pdist > info.MaxComf[F1][F2]) {
							rightvertical = rightvertical + 4*(pdist - info.MaxComf[F1][F2]);
							if (pdist > info.MaxPrac[F1][F2]) {
								rightvertical = rightvertical + 10*(pdist - info.MaxPrac[F1][F2]);
							}
						}
					}
					else {continue;}
				}}
			}
		}

			// vanaf slice 2: als een slice niet gelijk is aan vorige (dus 1 positie verschilt): bereken opnieuw binnen slice
		for ( a = start+1; a < end; a++){
			if (info.right[a][0]!=info.right[a-1][0] || info.right[a][1]!=info.right[a-1][1] || info.right[a][2]!=info.right[a-1][2] || info.right[a][3]!=info.right[a-1][3] || info.right[a][4]!=info.right[a-1][4]){
				for (int n = 0; n < 4; n++){
					if (info.right[a][n]!=0){
					int P1 = getPitch(info.right[a][n], allnotes);
					int F1 = (getFingering(info.right[a][n], allnotes)-1);
					for (int m = n+1; m < 5; m++){
						if (info.right[a][m]!=0){
						int P2 = getPitch(info.right[a][m], allnotes);
						int F2 = (getFingering(info.right[a][m], allnotes)-1);
						int pdist = P2-P1;

						if (pdist < info.MinRel[F1][F2]) {
							rightvertical = rightvertical + 2*(info.MinRel[F1][F2]-pdist);
							if (pdist < info.MinComf[F1][F2]) {
								rightvertical = rightvertical + 4*(info.MinComf[F1][F2]-pdist);
								if (pdist < info.MinPrac[F1][F2]) {
									rightvertical = rightvertical + 10*(info.MinPrac[F1][F2]-pdist);
								}
							}
						}
						else if (pdist > info.MaxRel[F1][F2]) {
							rightvertical = rightvertical + 2*(pdist - info.MaxRel[F1][F2]);
							if (pdist > info.MaxComf[F1][F2]) {
								rightvertical = rightvertical + 4*(pdist - info.MaxComf[F1][F2]);
								if (pdist > info.MaxPrac[F1][F2]) {
									rightvertical = rightvertical + 10*(pdist - info.MaxPrac[F1][F2]);
								}
							}
						}
						else {continue;}
					}}
				}}
			}
		}
	}

	// regel 5 (use of 4 (monotone): bestraf met 1 pt)
	for (int n = start; n<end; n++){
		if (count(info.right[n].begin(), info.right[n].end(), 0)==4 ){
			if( getFingering(info.right[n][0],allnotes)==4 && allnotes[(info.right[n][0]-1)].hand == 'R'){
				rightuseoffour ++;
			}
		}
	}

	// regel 7 en 8 (als 3 en 4 naast elkaar komen: +1) vervolgens: witte 3, zwarte 4:  + 1 extra
	for (int n = start; n< (end-1); n++){
		// voorwaarde: monofoon: 4 nullen in een slice
		if (count(info.right[n].begin(), info.right[n].end(), 0)==4 && count(info.right[n+1].begin(), info.right[n+1].end(), 0)==4 && info.right[n][0]!=info.right[n+1][0]){
			if (getFingering(info.right[n][0],allnotes) ==3 && getFingering(info.right[n+1][0],allnotes)==4){
				rightthreefour++;
				if ((getPitch(info.right[n][0],allnotes) % 2) == 1 && (getPitch(info.right[n+1][0],allnotes) % 2) == 0){
					rightthreefour++;
				}
			}
			else if (getFingering(info.right[n][0],allnotes) ==4 && getFingering(info.right[n+1][0],allnotes)==3){
				rightthreefour++;
				if ((getPitch(info.right[n][0],allnotes) % 2) == 0 && (getPitch(info.right[n+1][0],allnotes) % 2) == 1){
					rightthreefour++;
				}
			}
		}
	}

	// regel 11 en 12 (duimwissel) mono
	for (int n = (start + 1); n<(end); n++){
		if (count(info.right[n].begin(), info.right[n].end(), 0)==4){
			int currentf = getFingering(info.right[n][0],allnotes);
			if(count(info.right[n-1].begin(), info.right[n-1].end(), 0)==4 && currentf ==1){ // monofone duim
				int previousf = getFingering(info.right[n-1][0],allnotes);
				int previousp = getPitch(info.right[n-1][0],allnotes); // info vorige slice
				int currentp = getPitch(info.right[n][0],allnotes);
				int nextf = 0, nextp = 0;

				if ((n+1)<(end) && count(info.right[n+1].begin(), info.right[n+1].end(), 0)==4){ // als de volgende slice bestaat
					nextf = getFingering(info.right[n+1][0],allnotes);
					nextp = getPitch(info.right[n+1][0],allnotes);
				}

				// als vorige of volgende noot niet met 1 gespeeld wordt en een lagere pitch heeft dan de huidige:
				if(count(info.right[n-1].begin(), info.right[n-1].end(), 0)==4 && currentp>previousp && previousf!=1){
					if ((currentp % 2) == (previousp % 2)){ // zelfde niveau
						rightthumbswitch++;
					}
					else if ((currentp % 2)==0 && (previousp % 2)==1 ){ // duim op zwart, andere op wit
						rightthumbswitch = rightthumbswitch + 2;
					}
				}

				if((n+1)<end){
					if(count(info.right[n+1].begin(), info.right[n+1].end(), 0)==4 && currentp>nextp && nextf!=1){
						if ((currentp % 2) == (nextp % 2)){
							rightthumbswitch++;
						}
						else if ((currentp % 2)==0 && (nextp % 2)==1 ){
							rightthumbswitch = rightthumbswitch + 2;
						}
					}
				}
			}
		}
	}

	// monotone regels over 3 noten
	for (int n = start; n<(end-2); n++){ // ga hele stuk af (tot en met 2 voor einde)
		if (info.right[n][0]!=0 && count(info.right[n].begin(), info.right[n].end(), 0)==4
				&& info.right[n+1][0]!=0 && count(info.right[n+1].begin(), info.right[n+1].end(), 0)==4 // monotoon
				&& info.right[n][0]!=info.right[n+1][0]) { // verschillende slices
			int pitch_a = getPitch(info.right[n][0], allnotes); //1st note
			int finger_a = (getFingering(info.right[n][0], allnotes)); // fingering correct position in info_matrices
			int pitch_b = getPitch(info.right[n+1][0], allnotes); //2nd note
			int finger_b = (getFingering(info.right[n+1][0], allnotes)); // fingering correct position in info_matrices
			int pitch_c =0;
			int finger_c =0; //3rd note
			int d = 2;
			bool three = false;
			// zoek ook noot 3
			for(int a =(n+2); a<(end);a++){
				if (count(info.right[a].begin(), info.right[a].end(), 0)==4 && d < 3) {
					if (info.right[a][0]!=info.right[n+1][0] && info.right[n][0]!=0){ // niet nul en verschillend van 2e
						pitch_c = getPitch(info.right[a][0], allnotes); //3rd note
						finger_c = (getFingering(info.right[a][0], allnotes)); // fingering correct position in info_matrices
						d++;
						three = true;
					}
				}
				else {break;}
			}

			//regel 9 (1 op zwart)
			if (three == true && finger_b == 1 && (pitch_b % 2) == 0) { //+1
				rightblackthumb ++;
				if (finger_a != 1 && (pitch_a % 2) == 1){ // vorige noot wit? +2
					rightblackthumb ++; rightblackthumb ++;
				}
				if (finger_c != 1 && (pitch_c % 2) == 1){ // volgende noot wit +2
					rightblackthumb ++; rightblackthumb ++;
				}
			}

			//regel 10 (5 op zwart)
			if (three == true && finger_b == 5 && (pitch_b % 2) == 0) {
				if (finger_a != 5 && (pitch_a % 2) == 1){ // vorige noot wit? +2
					rightblackfifth ++; rightblackfifth ++;
				}
				if (finger_c != 5 && (pitch_c % 2) == 1){ // volgende noot wit +2
					rightblackfifth ++; rightblackfifth ++;
				}
			}

			int pdistca = pitch_c - pitch_a; //distance between 1st and 3rd  note
			//regel 3
			if (three == true){
				if (pdistca > info.MaxComf[(finger_a-1)][(finger_c-1)] || pdistca < info.MinComf[(finger_a-1)][(finger_c-1)]){
					righthandmove ++; // afstand 1e en 3e te groot => half hand move => +1
					if (finger_b == 1 && ( ( (pitch_a-pitch_b)>0 && (pitch_b-pitch_c)>0 ) || ( (pitch_a-pitch_b)<0 && (pitch_b-pitch_c)<0 ) ) ){ // if 2 add. conditions: 2nd finger = 1 and pitch b between a and c : one additional point (together with previous => +2)
						righthandmove ++;
					}
				}
				// complete half finger change rule: for 1st or 3rd note played with thumb: if pitch is same and finger is different: +1
				if (finger_a == 1 || finger_c==1 ){
					if (pitch_a == pitch_c && finger_a != finger_c){
						righthandmove ++;
					}
				}
			}

			// eigen regel 13: als eerste en derde noot verschillen, maar zelfde vinger en tweede noot ertussen ligt: +1.
			if (three == true && ( ( (pitch_a-pitch_b)>0 && (pitch_b-pitch_c)>0 ) || ( (pitch_a-pitch_b)<0 && (pitch_b-pitch_c)<0 ) ) && finger_a == finger_c){
				rightquick ++; // afstand 1e en 3e te groot => half hand move => +1
			}


			//regel 4: dist (1-3) > MaxComf or dist (1-3) < MinComf => + 1 per eenheid
			if (three == true){
				if (pdistca < info.MinComf[(finger_a-1)][(finger_c-1)]) {
					righthoronethree = righthoronethree + (info.MinComf[(finger_a-1)][(finger_c-1)]-pdistca);
				}
				else if (pdistca > info.MaxComf[(finger_a-1)][(finger_c-1)]) {
					righthoronethree = righthoronethree + (pdistca - info.MaxComf[(finger_a-1)][(finger_c-1)]);
				}
			}

		}
	}

	// eigen regel (weglaten): -1 als duim gebruikt wordt op eerste tel
	int rightfirstthumb = 0;
	for (int n = start; n<(end); n++){ // ga hele stuk af
		if ((n % info.divspm)==0){
			for (int a = 0; a < 5; a++){
				if(info.right[n][a]!= 0) {
					if(getFingering(info.right[n][a], allnotes)==1){
						rightfirstthumb--;
					}
				}
			}
		}
		else {continue;}
	}

	// regel 16
	for (int a = start; a < (end-1); a++){ // vanaf eerste slice t.e.m. voorlaatste:
		// zoek verschillende slices (dus met andere noten (ID) ), maar de pitches moeten wel 1op1 gelijk zijn (dus ook gelijk aantal nullen nodig)
		if ((info.right[a][0]!=info.right[a+1][0] || info.right[a][1]!=info.right[a+1][1] || info.right[a][2]!=info.right[a+1][2] || info.right[a][3]!=info.right[a+1][3] || info.right[a][4]!=info.right[a+1][4]) && count(info.right[a].begin(),info.right[a].end(),0) ==  count(info.right[a+1].begin(),info.right[a+1].end(),0) ){
			// lijst de pitches op in huidige slice, en in volgende => 1 op 1 gelijk ==> test fingering ook 1 op 1 gelijk
			Notepart notepart;
			vector<Notepart> slicecur, slicenext;
			slicecur.clear();
			slicenext.clear();

			// get pitches and fingerings for slice a
			for (int b = 0; b<5; b++){
				if (info.right[a][b]!=0){
					notepart.pitch = getPitch(info.right[a][b], allnotes);
					notepart.finger = getFingering(info.right[a][b], allnotes);
					slicecur.push_back(notepart);
				}
			}

			// get pitches and fingerings for slice a+1
			for (int b = 0; b<5; b++){
				if (info.right[a+1][b]!=0){
					notepart.pitch = getPitch(info.right[a+1][b], allnotes);
					notepart.finger = getFingering(info.right[a+1][b], allnotes);
					slicenext.push_back(notepart);
				}
			}

			//qsort slicecur en slicenext op pitch
			qsort(&slicecur[0], slicecur.size(), sizeof(Notepart), comparePitch);
			qsort(&slicenext[0], slicenext.size(), sizeof(Notepart), comparePitch);

			//vergelijk pitches, eis 1 op 1 gelijk
			vector <int> thesame;
			thesame.clear();
			for (unsigned int t = 0; t < slicecur.size(); t++){
				if (slicecur[t].pitch == slicenext[t].pitch){
					thesame.push_back(1);
				}
				else {thesame.push_back(0);}
			}
			if (count (thesame.begin(),thesame.end(),0)==0) {
				for (unsigned int u = 0; u < slicecur.size(); u++){
					if (slicecur[u].finger != slicenext[u].finger){
						rightdouble++;
					}
				}
			}

		}
	}

	// moeten elementen van score nog in vector komen? vector<int> scores
	// 6, 7, ... (score per regel, dan optellen) => gebeurt hier reeds via ints.

	//alle elementen van score optellen

	righttargetfunction = righthorizontal + rightvertical + 0*value*rightuseoffour + value*rightthreefour + rightthumbswitch + rightblackthumb/2 + rightblackfifth/2 + righthandmove + rightquick + righthoronethree + 0*rightfirstthumb/2 + rightdouble;
	return righttargetfunction;

}

double leftScoreP (MusicInfo &info, vector<note> &allnotes, int start, int end){

	// berekening doelfunctie rechts (voor hele stuk)

	double lefttargetfunction = 0;
	double lefthorizontal = 0;
	double leftvertical = 0;
	double leftuseoffour = 0;
	double leftthreefour = 0;
	double leftthumbswitch = 0;
	double leftblackthumb = 0;
	double leftblackfifth = 0;
	double lefthandmove = 0;
	double lefthoronethree = 0;
	double leftquick = 0;
	double leftdouble = 0;

	double value = 1; // hoe erg is de vierde vinger?

	// regels 1, 2, 14
	for (int a = start; a < (end-1); a++){ // vanaf eerste slice t.e.m. voorlaatste:
		for (int n = 0; n < 5; n++){
			if (info.left[a][n]!=0 && count(info.left[a+1].begin(), info.left[a+1].end(), info.left[a][n])==0){
				int P1 = getPitch(info.left[a][n], allnotes);
				int F3 = (getFingering(info.left[a][n], allnotes)-1); // meteen juiste positie in array[][]
				for (int m = 0; m < 5; m++){
					if (info.left[a+1][m]!=0 && count(info.left[a].begin(), info.left[a].end(), info.left[a+1][m])==0){
						int P2 = getPitch(info.left[a+1][m], allnotes);
						int F4 = (getFingering(info.left[a+1][m], allnotes)-1); // meteen juiste positie in array[][]
						int pdist = P2-P1;

						if (pdist < info.MinRel[F4][F3]) {
							lefthorizontal = lefthorizontal + (info.MinRel[F4][F3]-pdist);
							if (pdist < info.MinComf[F4][F3]) {
								lefthorizontal = lefthorizontal + 2*(info.MinComf[F4][F3]-pdist);
								if (pdist < info.MinPrac[F4][F3]) {
									lefthorizontal = lefthorizontal + 10*(info.MinPrac[F4][F3]-pdist);
								}
							}
						}
						else if (pdist > info.MaxRel[F4][F3]) {
							lefthorizontal = lefthorizontal + (pdist - info.MaxRel[F4][F3]);
							if (pdist > info.MaxComf[F4][F3]) {
								lefthorizontal = lefthorizontal + 2*(pdist - info.MaxComf[F4][F3]);
								if (pdist > info.MaxPrac[F4][F3]) {
									lefthorizontal = lefthorizontal + 10*(pdist - info.MaxPrac[F4][F3]);
								}
							}
						}
						else {continue;}

					}
				}
			}
		}
	}

	// regel 15 (in slice) => x2: important to have a playable chord
	{int a = start;
			for (int n = 0; n < 4; n++){
				if (info.left[a][n]!=0){
				int P1 = getPitch(info.left[a][n], allnotes);
				int F3 = (getFingering(info.left[a][n], allnotes)-1);
				for (int m = n+1; m < 5; m++){
					if (info.left[a][m]!=0){
					int P2 = getPitch(info.left[a][m], allnotes);
					int F4 = (getFingering(info.left[a][m], allnotes)-1);
					int pdist = P2-P1;

					if (pdist < info.MinRel[F4][F3]) {
						leftvertical = leftvertical + 2*(info.MinRel[F4][F3]-pdist);
						if (pdist < info.MinComf[F4][F3]) {
							leftvertical = leftvertical + 4*(info.MinComf[F4][F3]-pdist);
							if (pdist < info.MinPrac[F4][F3]) {
								leftvertical = leftvertical + 10*(info.MinPrac[F4][F3]-pdist);
							}
						}
					}
					else if (pdist > info.MaxRel[F4][F3]) {
						leftvertical = leftvertical + 2*(pdist - info.MaxRel[F4][F3]);
						if (pdist > info.MaxComf[F4][F3]) {
							leftvertical = leftvertical + 4*(pdist - info.MaxComf[F4][F3]);
							if (pdist > info.MaxPrac[F4][F3]) {
								leftvertical = leftvertical + 10*(pdist - info.MaxPrac[F4][F3]);
							}
						}
					}
					else {continue;}
				}
			}}
		}

		for ( a = start+1; a < (end); a++){
			if (info.left[a][0]!=info.left[a-1][0] || info.left[a][1]!=info.left[a-1][1] || info.left[a][2]!=info.left[a-1][2] || info.left[a][3]!=info.left[a-1][3] || info.left[a][4]!=info.left[a-1][4]){
				for (int n = 0; n < 4; n++){
					if (info.left[a][n]!=0){
					int P1 = getPitch(info.left[a][n], allnotes);
					int F3 = (getFingering(info.left[a][n], allnotes)-1);
					for (int m = n+1; m < 5; m++){
						if (info.left[a][m]!=0){
						int P2 = getPitch(info.left[a][m], allnotes);
						int F4 = (getFingering(info.left[a][m], allnotes)-1);
						int pdist = P2-P1;

						if (pdist < info.MinRel[F4][F3]) {
							leftvertical = leftvertical + 2*(info.MinRel[F4][F3]-pdist);
							if (pdist < info.MinComf[F4][F3]) {
								leftvertical = leftvertical + 4*(info.MinComf[F4][F3]-pdist);
								if (pdist < info.MinPrac[F4][F3]) {
									leftvertical = leftvertical + 10*(info.MinPrac[F4][F3]-pdist);
								}
							}
						}
						else if (pdist > info.MaxRel[F4][F3]) {
							leftvertical = leftvertical + 2*(pdist - info.MaxRel[F4][F3]);
							if (pdist > info.MaxComf[F4][F3]) {
								leftvertical = leftvertical + 4*(pdist - info.MaxComf[F4][F3]);
								if (pdist > info.MaxPrac[F4][F3]) {
									leftvertical = leftvertical + 10*(pdist - info.MaxPrac[F4][F3]);
								}
							}
						}
						else {continue;}
					}}
				}}
			}
		}
	}

	// regel 5 (use of 4 (monotone): bestraf met 1 pt)
	for (int n = start; n<end; n++){
		if (count(info.left[n].begin(), info.left[n].end(), 0)==4){
			if (getFingering(info.left[n][0],allnotes)==4 && allnotes[(info.left[n][0]-1)].hand == 'L'){
				leftuseoffour ++;
			}
		}
	}

	// regel 7 en 8 (als 3 en 4 naast elkaar komen: +1) vervolgens: witte 3, zwarte 4:  + 1 extra
	for (int n = start; n< (end-1); n++){
		// voorwaarde: monofoon: 4 nullen in een slice
		if (count(info.left[n].begin(), info.left[n].end(), 0)==4 && count(info.left[n+1].begin(), info.left[n+1].end(), 0)==4 && info.left[n][0]!=info.left[n+1][0]){
			if (getFingering(info.left[n][0],allnotes) ==3 && getFingering(info.left[n+1][0],allnotes)==4){
				leftthreefour++;
				if ((getPitch(info.left[n][0],allnotes) % 2) == 1 && (getPitch(info.left[n+1][0],allnotes) % 2) == 0){
					leftthreefour++;
				}
			}
			else if (getFingering(info.left[n][0],allnotes) ==4 && getFingering(info.left[n+1][0],allnotes)==3){
				leftthreefour++;
				if ((getPitch(info.left[n][0],allnotes) % 2) == 0 && (getPitch(info.left[n+1][0],allnotes) % 2) == 1){
					leftthreefour++;
				}
			}
		}
	}

	// regel 11 en 12 (duimwissel)
	for (int n = (start + 1); n<(end); n++){
		if(count(info.left[n].begin(), info.left[n].end(), 0)==4){
			int currentf = getFingering(info.left[n][0],allnotes);
			if(count(info.left[n-1].begin(), info.left[n-1].end(), 0)==4 && currentf ==1){ // monofone duim
				int previousf = getFingering(info.left[n-1][0],allnotes);
				int previousp = getPitch(info.left[n-1][0],allnotes); // info vorige slice
				int currentp = getPitch(info.left[n][0],allnotes);
				int nextf = 0, nextp = 0;

				if ((n+1)<(end) && count(info.left[n+1].begin(), info.left[n+1].end(), 0)==4){ // als de volgende slice bestaat
					nextf = getFingering(info.left[n+1][0],allnotes);
					nextp = getPitch(info.left[n+1][0],allnotes);
				}

				// hier als vorige of volgende hoger is dan huidige (die met 1 gespeeld w), en niet met 1 gespeeld wordt.
				if(count(info.left[n-1].begin(), info.left[n-1].end(), 0)==4 && currentp<previousp && previousf!=1){
					if ((currentp % 2) == (previousp % 2)){
						leftthumbswitch++;
					}
					else if ((currentp % 2)==0 && (previousp % 2)==1 ){
						leftthumbswitch = leftthumbswitch + 2;
					}
				}

				if ((n+1)<(end)){
					if(count(info.left[n+1].begin(), info.left[n+1].end(), 0)==4 && currentp<nextp && nextf!=1){
						if ((currentp % 2) == (nextp % 2)){
							leftthumbswitch++;
						}
						else if ((currentp % 2)==0 && (nextp % 2)==1 ){
							leftthumbswitch = leftthumbswitch + 2;
						}
					}
				}
			}
		}
	}

	// monotone regels over 3 noten
	for (int n = start; n<(end-2); n++){ // ga hele stuk af (tot en met 2 voor einde)
			if (info.left[n][0]!=0 && count(info.left[n].begin(), info.left[n].end(), 0)==4
					&& info.left[n+1][0]!=0 && count(info.left[n+1].begin(), info.left[n+1].end(), 0)==4 // monotoon
					&& info.left[n][0]!=info.left[n+1][0]) { // verschillende slices
				int pitch_a = getPitch(info.left[n][0], allnotes); //1st note
				int finger_a = (getFingering(info.left[n][0], allnotes)); // fingering correct position in info_matrices
				int pitch_b = getPitch(info.left[n+1][0], allnotes); //2nd note
				int finger_b = (getFingering(info.left[n+1][0], allnotes)); // fingering correct position in info_matrices
				int pitch_c =0;
				int finger_c =0; //3rd note
				int d = 2;
				bool three = false;
				// zoek ook noot 3
				for(int a =(n+2); a<(end);a++){
					if (count(info.left[a].begin(), info.left[a].end(), 0)==4 && d < 3) {
						if (info.left[a][0]!=info.left[n+1][0] && info.left[n][0]!=0){ // niet nul en verschillend van 2e
							pitch_c = getPitch(info.left[a][0], allnotes); //3rd note
							finger_c = (getFingering(info.left[a][0], allnotes)); // fingering correct position in info_matrices
							d++;
							three = true;
						}
					}
					else {break;}
				}

				//regel 9 (1 op zwart)
				if (three == true && finger_b == 1 && (pitch_b % 2) == 0) { //+1
					leftblackthumb ++;
					if (finger_a != 1 && (pitch_a % 2) == 1){ // vorige noot wit? +2
						leftblackthumb ++; leftblackthumb ++;
					}
					if (finger_c != 1 && (pitch_c % 2) == 1){ // volgende noot wit +2
						leftblackthumb ++; leftblackthumb ++;
					}
				}

				//regel 10 (5 op zwart)
				if (three == true && finger_b == 5 && (pitch_b % 2) == 0) {
					if (finger_a != 5 && (pitch_a % 2) == 1){ // vorige noot wit? +2
						leftblackfifth ++; leftblackfifth ++;
					}
					if (finger_c != 5 && (pitch_c % 2) == 1){ // volgende noot wit +2
						leftblackfifth ++; leftblackfifth ++;
					}
				}

				int pdistca = pitch_c - pitch_a; //distance between 1st and 3rd  note
				//regel 3
				if (three == true){
					if (pdistca > info.MaxComf[(finger_c-1)][(finger_a-1)] || pdistca < info.MinComf[(finger_c-1)][(finger_a-1)]){
						lefthandmove ++; // afstand 1e en 3e te groot => half hand move => +1
						if (finger_b == 1 && ( ( (pitch_a-pitch_b)>0 && (pitch_b-pitch_c)>0 ) || ( (pitch_a-pitch_b)<0 && (pitch_b-pitch_c)<0 ) ) ){ // if 2 add. conditions: 2nd finger = 1 and pitch b between a and c : one additional point (together with previous => +2)
							lefthandmove ++;
						}
					}
					// complete half finger change rule: for 1st or 3rd note played with thumb: if pitch is same and finger is different: +1
					if (finger_a == 1 || finger_c==1 ){
						if (pitch_a == pitch_c && finger_a != finger_c){
							lefthandmove ++;
						}
					}
				}

				// eigen regel 13: als eerste en derde noot verschillen, maar zelfde vinger en tweede noot ertussen ligt: +1.
				if (three == true && ( ( (pitch_a-pitch_b)>0 && (pitch_b-pitch_c)>0 ) || ( (pitch_a-pitch_b)<0 && (pitch_b-pitch_c)<0 ) ) && finger_a == finger_c){
					leftquick ++; // afstand 1e en 3e te groot => half hand move => +1
				}

				//regel 4: dist (1-3) > MaxComf or dist (1-3) < MinComf => + 1 per eenheid
				if (three == true){
					if (pdistca < info.MinComf[(finger_c-1)][(finger_a-1)]) {
						lefthoronethree = lefthoronethree + (info.MinComf[(finger_c-1)][(finger_a-1)]-pdistca);
					}
					else if (pdistca > info.MaxComf[(finger_c-1)][(finger_a-1)]) {
						lefthoronethree = lefthoronethree + (pdistca - info.MaxComf[(finger_c-1)][(finger_a-1)]);
					}
				}
			}
		}

	// eigen regel (weglaten): -1 als duim gebruikt wordt op eerste tel
	int leftfirstthumb = 0;
	for (int n = start; n<(end); n++){ // ga hele stuk af
		if ((n % info.divspm)==0){
			for (int a = 0; a < 5; a++){
				if(info.left[n][a]!= 0){
					if (getFingering(info.left[n][a], allnotes)==1){
						leftfirstthumb--;
					}
				}
			}
		}
		else {continue;}
	}

	// regel 16
		for (int a = start; a < (end-1); a++){ // vanaf eerste slice t.e.m. voorlaatste:
			// zoek verschillende slices (dus met andere noten (ID) ), maar de pitches moeten wel 1op1 gelijk zijn (dus ook gelijk aantal nullen nodig)
			if ((info.left[a][0]!=info.left[a+1][0] || info.left[a][1]!=info.left[a+1][1] || info.left[a][2]!=info.left[a+1][2] || info.left[a][3]!=info.left[a+1][3] || info.left[a][4]!=info.left[a+1][4]) && count(info.left[a].begin(),info.left[a].end(),0) ==  count(info.left[a+1].begin(),info.left[a+1].end(),0) ){
				// lijst de pitches op in huidige slice, en in volgende => 1 op 1 gelijk ==> test fingering ook 1 op 1 gelijk
				Notepart notepart;
				vector<Notepart> slicecur, slicenext;
				slicecur.clear();
				slicenext.clear();

				// get pitches and fingerings for slice a
				for (int b = 0; b<5; b++){
					if (info.left[a][b]!=0){
						notepart.pitch = getPitch(info.left[a][b], allnotes);
						notepart.finger = getFingering(info.left[a][b], allnotes);
						slicecur.push_back(notepart);
					}
				}

				// get pitches and fingerings for slice a+1
				for (int b = 0; b<5; b++){
					if (info.left[a+1][b]!=0){
						notepart.pitch = getPitch(info.left[a+1][b], allnotes);
						notepart.finger = getFingering(info.left[a+1][b], allnotes);
						slicenext.push_back(notepart);
					}
				}

				//qsort slicecur en slicenext op pitch
				qsort(&slicecur[0], slicecur.size(), sizeof(Notepart), comparePitch);
				qsort(&slicenext[0], slicenext.size(), sizeof(Notepart), comparePitch);

				//vergelijk pitches, eis 1 op 1 gelijk
				vector <int> thesame;
				thesame.clear();
				for (unsigned int t = 0; t < slicecur.size(); t++){
					if (slicecur[t].pitch == slicenext[t].pitch){
						thesame.push_back(1);
					}
					else {thesame.push_back(0);}
				}
				if (count (thesame.begin(),thesame.end(),0)==0) {
					for (unsigned int u = 0; u < slicecur.size(); u++){
						if (slicecur[u].finger != slicenext[u].finger){
							leftdouble++;
						}
					}
				}

			}
		}

	// moeten elementen van score nog in vector komen? vector<int> scores
	// 6, 7, ... (score per regel, dan optellen) => gebeurt hier reeds via ints.

	//alle elementen van score optellen

	lefttargetfunction = lefthorizontal + leftvertical + 0*value*leftuseoffour + value*leftthreefour + leftthumbswitch + leftquick + leftblackthumb/2 + leftblackfifth/2 + lefthandmove + lefthoronethree +0*leftfirstthumb/2 + leftdouble;
	return lefttargetfunction;

	}


/* double leftScoreP (MusicInfo &info, vector<note> &allnotes, int start){
// DIT WAS VORIGE PROPAGATION: NEGEER

// berekening doelfunctie links (voor hele stuk)
	// berekeningen: rechts MaxPrac[1][2] = links MaxPrac [2][1]

	double lefttargetfunction = 0;
	double lefthorizontal = 0;
	double leftvertical = 0;
	double leftuseoffour = 0;
	double leftthreefour = 0;
	double leftthumbswitch = 0;
	double leftblackthumb = 0;
	double leftblackfifth = 0;
	double lefthandmove = 0;
	double lefthoronethree = 0;
	double leftquick = 0;

	double value = 1; // hoe erg is 4e vinger?

	// regels 1, 2, 15
	for (int a = start; a < (start + (5*info.divspm)-1); a++){ // vanaf eerste slice t.e.m. voorlaatste:
		for (int n = 0; n < 5; n++){
			if (info.left[a][n]!=0 && count(info.left[a+1].begin(), info.left[a+1].end(), info.left[a][n])==0){
				int P1 = getPitch(info.left[a][n], allnotes);
				int F3 = (getFingering(info.left[a][n], allnotes)-1); // meteen juiste positie in array[][]
				for (int m = 0; m < 5; m++){
					if (info.left[a+1][m]!=0 && count(info.left[a].begin(), info.left[a].end(), info.left[a+1][m])==0){
						int P2 = getPitch(info.left[a+1][m], allnotes);
						int F4 = (getFingering(info.left[a+1][m], allnotes)-1); // meteen juiste positie in array[][]
						int pdist = P2-P1;

						if (pdist < info.MinRel[F4][F3]) {
							lefthorizontal = lefthorizontal + (info.MinRel[F4][F3]-pdist);
							if (pdist < info.MinComf[F4][F3]) {
								lefthorizontal = lefthorizontal + 2*(info.MinComf[F4][F3]-pdist);
								if (pdist < info.MinPrac[F4][F3]) {
									lefthorizontal = lefthorizontal + 10*(info.MinPrac[F4][F3]-pdist);
								}
							}
						}
						else if (pdist > info.MaxRel[F4][F3]) {
							lefthorizontal = lefthorizontal + (pdist - info.MaxRel[F4][F3]);
							if (pdist > info.MaxComf[F4][F3]) {
								lefthorizontal = lefthorizontal + 2*(pdist - info.MaxComf[F4][F3]);
								if (pdist > info.MaxPrac[F4][F3]) {
									lefthorizontal = lefthorizontal + 10*(pdist - info.MaxPrac[F4][F3]);
								}
							}
						}
						else {continue;}

					}
				}
			}
		}
	}

	// regel 16 (in slice) => x2: important to have a playable chord
	{int a = start;
			for (int n = 0; n < 4; n++){
				if (info.left[a][n]!=0){
				int P1 = getPitch(info.left[a][n], allnotes);
				int F3 = (getFingering(info.left[a][n], allnotes)-1);
				for (int m = n+1; m < 5; m++){
					if (info.left[a][m]!=0){
					int P2 = getPitch(info.left[a][m], allnotes);
					int F4 = (getFingering(info.left[a][m], allnotes)-1);
					int pdist = P2-P1;

					if (pdist < info.MinRel[F4][F3]) {
						leftvertical = leftvertical + 2*(info.MinRel[F4][F3]-pdist);
						if (pdist < info.MinComf[F4][F3]) {
							leftvertical = leftvertical + 4*(info.MinComf[F4][F3]-pdist);
							if (pdist < info.MinPrac[F4][F3]) {
								leftvertical = leftvertical + 10*(info.MinPrac[F4][F3]-pdist);
							}
						}
					}
					else if (pdist > info.MaxRel[F4][F3]) {
						leftvertical = leftvertical + 2*(pdist - info.MaxRel[F4][F3]);
						if (pdist > info.MaxComf[F4][F3]) {
							leftvertical = leftvertical + 4*(pdist - info.MaxComf[F4][F3]);
							if (pdist > info.MaxPrac[F4][F3]) {
								leftvertical = leftvertical + 10*(pdist - info.MaxPrac[F4][F3]);
							}
						}
					}
					else {continue;}
				}
			}}
		}

		for ( a = start+1; a < (start + (5*info.divspm)); a++){
			if (info.left[a][0]!=info.left[a-1][0] || info.left[a][1]!=info.left[a-1][1] || info.left[a][2]!=info.left[a-1][2] || info.left[a][3]!=info.left[a-1][3] || info.left[a][4]!=info.left[a-1][4]){
				for (int n = 0; n < 4; n++){
					if (info.left[a][n]!=0){
					int P1 = getPitch(info.left[a][n], allnotes);
					int F3 = (getFingering(info.left[a][n], allnotes)-1);
					for (int m = n+1; m < 5; m++){
						if (info.left[a][m]!=0){
						int P2 = getPitch(info.left[a][m], allnotes);
						int F4 = (getFingering(info.left[a][m], allnotes)-1);
						int pdist = P2-P1;

						if (pdist < info.MinRel[F4][F3]) {
							leftvertical = leftvertical + 2*(info.MinRel[F4][F3]-pdist);
							if (pdist < info.MinComf[F4][F3]) {
								leftvertical = leftvertical + 4*(info.MinComf[F4][F3]-pdist);
								if (pdist < info.MinPrac[F4][F3]) {
									leftvertical = leftvertical + 10*(info.MinPrac[F4][F3]-pdist);
								}
							}
						}
						else if (pdist > info.MaxRel[F4][F3]) {
							leftvertical = leftvertical + 2*(pdist - info.MaxRel[F4][F3]);
							if (pdist > info.MaxComf[F4][F3]) {
								leftvertical = leftvertical + 4*(pdist - info.MaxComf[F4][F3]);
								if (pdist > info.MaxPrac[F4][F3]) {
									leftvertical = leftvertical + 10*(pdist - info.MaxPrac[F4][F3]);
								}
							}
						}
						else {continue;}
					}}
				}}
			}
		}
	}

	// regel 5 (use of 4) monofoon
	for (int n = start; n<(start + (5*info.divspm)); n++){
		if (count(info.left[n].begin(), info.left[n].end(), 0)==4 && getFingering(info.left[n][0],allnotes)==4 && allnotes[n].hand == 'L'){
			leftuseoffour ++;
		}
	}

	// regel 7 en 8 (3-4; zwart-wit)
	for (int n = start; n<(start + (5*info.divspm)-1); n++){
		if (count(info.left[n].begin(), info.left[n].end(), 0)==4 && count(info.left[n+1].begin(), info.left[n+1].end(), 0)==4 && info.left[n][0]!=info.left[n+1][0]){
			if (getFingering(info.left[n][0],allnotes) ==3 && getFingering(info.left[n+1][0],allnotes)==4){
				leftthreefour++;
				if ((getPitch(info.left[n][0],allnotes) % 2) == 1 && (getPitch(info.left[n+1][0],allnotes) % 2) == 0){
					leftthreefour++;
				}
			}

			else if (getFingering(info.left[n][0],allnotes) ==4 && getFingering(info.left[n+1][0],allnotes)==3){
				leftthreefour++;
				if ((getPitch(info.left[n][0],allnotes) % 2) == 0 && (getPitch(info.left[n+1][0],allnotes) % 2) == 1){
					leftthreefour++;
				}
			}
		}
	}

	// regel 11 en 12 (duimwissel)
	for (int n = start + 1; n<(start + (5*info.divspm)); n++){
		int currentf = getFingering(info.left[n][0],allnotes);
		if(count(info.left[n].begin(), info.left[n].end(), 0)==4 && currentf ==1){
			int previousf = getFingering(info.left[n-1][0],allnotes);
			int previousp = getPitch(info.left[n-1][0],allnotes);
			int currentp = getPitch(info.left[n][0],allnotes);
			int nextf = 0, nextp = 0;

			if ((n+1)<(start + (5*info.divspm))){
				nextf = getFingering(info.left[n+1][0],allnotes);
				nextp = getPitch(info.left[n+1][0],allnotes);
			}

			// hier als vorige of volgende hoger is dan huidige (die met 1 gespeeld w), en niet met 1 gespeeld wordt.
			if(count(info.left[n-1].begin(), info.left[n-1].end(), 0)==4 && currentp<previousp && previousf!=1){
				if ((currentp % 2) == (previousp % 2)){
					leftthumbswitch++;
				}
				else if ((currentp % 2)==0 && (previousp % 2)==1 ){
					leftthumbswitch = leftthumbswitch + 2;
				}
			}

			if ((n+1)<(start + (5*info.divspm))){
				if(count(info.left[n+1].begin(), info.left[n+1].end(), 0)==4 && currentp<nextp && nextf!=1){
					if ((currentp % 2) == (nextp % 2)){
						leftthumbswitch++;
					}
					else if ((currentp % 2)==0 && (nextp % 2)==1 ){
						leftthumbswitch = leftthumbswitch + 2;
					}
				}
			}
		}
	}

	// monotone regels over 3 noten
	for (int n = start; n<(start + (5*info.divspm)-2); n++){ // ga hele stuk af (tot en met 2 voor einde)
			if (info.left[n][0]!=0 && count(info.left[n].begin(), info.left[n].end(), 0)==4
					&& info.left[n+1][0]!=0 && count(info.left[n+1].begin(), info.left[n+1].end(), 0)==4 // monotoon
					&& info.left[n][0]!=info.left[n+1][0]) { // verschillende slices
				int pitch_a = getPitch(info.left[n][0], allnotes); //1st note
				int finger_a = (getFingering(info.left[n][0], allnotes)); // fingering correct position in info_matrices
				int pitch_b = getPitch(info.left[n+1][0], allnotes); //2nd note
				int finger_b = (getFingering(info.left[n+1][0], allnotes)); // fingering correct position in info_matrices
				int pitch_c =0;
				int finger_c =0; //3rd note
				int d = 2;
				bool three = false;
				// zoek ook noot 3
				for(int a =(n+2); a<(start + (5*info.divspm));a++){
					if (count(info.left[a].begin(), info.left[a].end(), 0)==4 && d < 3) {
						if (info.left[a][0]!=info.left[n+1][0] && info.left[n][0]!=0){ // niet nul en verschillend van 2e
							pitch_c = getPitch(info.left[a][0], allnotes); //3rd note
							finger_c = (getFingering(info.left[a][0], allnotes)); // fingering correct position in info_matrices
							d++;
							three = true;
						}
					}
					else {break;}
				}

				//regel 9 (1 op zwart)
				if (three == true && finger_b == 1 && (pitch_b % 2) == 0) { //+1
					leftblackthumb ++;
					if (finger_a != 1 && (pitch_a % 2) == 1){ // vorige noot wit? +2
						leftblackthumb ++; leftblackthumb ++;
					}
					if (finger_c != 1 && (pitch_c % 2) == 1){ // volgende noot wit +2
						leftblackthumb ++; leftblackthumb ++;
					}
				}

				//regel 10 (5 op zwart)
				if (three == true && finger_b == 5 && (pitch_b % 2) == 0) {
					if (finger_a != 5 && (pitch_a % 2) == 1){ // vorige noot wit? +2
						leftblackfifth ++; leftblackfifth ++;
					}
					if (finger_c != 5 && (pitch_c % 2) == 1){ // volgende noot wit +2
						leftblackfifth ++; leftblackfifth ++;
					}
				}

				int pdistca = pitch_c - pitch_a; //distance between 1st and 3rd  note
				//regel 3
				if (three == true){
					if (pdistca > info.MaxComf[(finger_c-1)][(finger_a-1)] || pdistca < info.MinComf[(finger_c-1)][(finger_a-1)]){
						lefthandmove ++; // afstand 1e en 3e te groot => half hand move => +1
						if (finger_b == 1 && ( ( (pitch_a-pitch_b)>0 && (pitch_b-pitch_c)>0 ) || ( (pitch_a-pitch_b)<0 && (pitch_b-pitch_c)<0 ) ) ){ // if 2 add. conditions: 2nd finger = 1 and pitch b between a and c : one additional point (together with previous => +2)
							lefthandmove ++;
						}
					}
					// complete half finger change rule: for 1st or 3rd note played with thumb: if pitch is same and finger is different: +1
					if (finger_a == 1 || finger_c==1 ){
						if (pitch_a == pitch_c && finger_a != finger_c){
							lefthandmove ++;
						}
					}
				}

				// eigen regel 14: als eerste en derde noot verschillen, maar zelfde vinger en tweede noot ertussen ligt: +1.
				if (three == true && ( ( (pitch_a-pitch_b)>0 && (pitch_b-pitch_c)>0 ) || ( (pitch_a-pitch_b)<0 && (pitch_b-pitch_c)<0 ) ) && finger_a == finger_c){
					leftquick ++; // afstand 1e en 3e te groot => half hand move => +1
				}

				//regel 4: dist (1-3) > MaxComf or dist (1-3) < MinComf => + 1 per eenheid
				if (three == true){
					if (pdistca < info.MinComf[(finger_c-1)][(finger_a-1)]) {
						lefthoronethree = lefthoronethree + (info.MinComf[(finger_a-1)][(finger_c-1)]-pdistca);
					}
					else if (pdistca > info.MaxComf[(finger_c-1)][(finger_a-1)]) {
						lefthoronethree = lefthoronethree + (pdistca - info.MaxComf[(finger_a-1)][(finger_c-1)]);
					}
				}
			}
		}

		// eigen regel 13: -1 als duim gebruikt wordt op eerste tel
		int leftfirstthumb = 0;
		for (int n = start; n<(start + (5*info.divspm)); n++){ // ga hele stuk af
			if ((n % info.divspm)==0){
				for (int a = 0; a < 5; a++){
					if(info.left[n][a]!= 0 && getFingering(info.left[n][a], allnotes)==1){
						leftfirstthumb--;
					}
				}
			}
			else {continue;}
		}

	// moeten elementen van score nog in vector komen? vector<int> scores
	// 6, 7, ... (score per regel, dan optellen) => gebeurt hier reeds via ints.

	//alle elementen van score optellen

	lefttargetfunction = lefthorizontal + leftvertical + 0*value*leftuseoffour + value*leftthreefour + leftthumbswitch + leftquick + leftblackthumb/2 + leftblackfifth/2 + lefthandmove + lefthoronethree +leftfirstthumb/2;
	return lefttargetfunction;

	}
*/

/*
//output feas
bool rightFeasibilityTrace(MusicInfo &info, vector<note> &allnotes){

	cout << "\n" << "here come all part. feas.";

	bool feas;
	vector <bool> slicefeas; // per slice 1 of 0 voor feasibility opslaan in vector
	bool slicefea; // value for feasibility per slice
	vector <short int> slicefingers; // fingering per slice
	slicefeas.clear();
	for (unsigned int a = 0; a < info.right.size(); a++){ // per slice evaluation
		slicefingers.clear();
		for (int n = 0; n < 5; n++){
			if (info.right[a][n]!=0){ // if note id !=0 => store fingering in vector
				short int finger = getFingering(info.right[a][n], allnotes);
				slicefingers.push_back(finger);
			}
		}
		// if every finger is used once (or less) = that slice is feasible and a value of 1 is pushed in vector slicefeas
		if (count(slicefingers.begin(), slicefingers.end(), 1)<=1 && count(slicefingers.begin(), slicefingers.end(), 2)<=1 && count(slicefingers.begin(), slicefingers.end(), 2)<=1 && count(slicefingers.begin(), slicefingers.end(), 3)<=1 && count(slicefingers.begin(), slicefingers.end(), 4)<=1 && count(slicefingers.begin(), slicefingers.end(), 5)<=1 ){
			slicefea = 1;
			slicefeas.push_back(slicefea);
			cout << 1;
		}
		// else: one finger is used more than once: zero is pushed in vector
		else {
			slicefea = 0;
			slicefeas.push_back(slicefea);
			cout << 0;
		}
	}

	// if no zero's (unfeas) in any slice: fingering is feasible => return 1; else return 0
	if (count (slicefeas.begin(), slicefeas.end(), 0)==0){
		feas = 1;
	}
	else {
		feas = 0;
	}
	cout << "\n" << "endfeas: "<<feas << "\n";
	return feas; // 1 als feasible
}

// output feas.
bool leftFeasibilityTrace(MusicInfo &info, vector<note> &allnotes){

	cout << "\n" << "here come all part. feas." << "\n";

	bool feas;
	vector <bool> slicefeas; // per slice 1 of 0 voor feasibility opslaan in vector
	bool slicefea; // value for feasibility per slice
	vector <short int> slicefingers; // fingering per slice
	slicefeas.clear();
	for (unsigned int a = 0; a < info.left.size(); a++){ // per slice evaluation
		slicefingers.clear();
		for (int n = 0; n < 5; n++){
			if (info.left[a][n]!=0){ // if note id !=0 => store fingering in vector
				short int finger = getFingering(info.left[a][n], allnotes);
				slicefingers.push_back(finger);
			}
		}
		// if every finger is used once (or less) = that slice is feasible and a value of 1 is pushed in vector slicefeas
		if (count(slicefingers.begin(), slicefingers.end(), 1)<=1 && count(slicefingers.begin(), slicefingers.end(), 2)<=1 && count(slicefingers.begin(), slicefingers.end(), 2)<=1 && count(slicefingers.begin(), slicefingers.end(), 3)<=1 && count(slicefingers.begin(), slicefingers.end(), 4)<=1 && count(slicefingers.begin(), slicefingers.end(), 5)<=1 ){
			slicefea = 1;
			slicefeas.push_back(slicefea);
			cout << 1 << ",	";
		}
		// else: one finger is used more than once: zero is pushed in vector
		else {
			slicefea = 0;
			slicefeas.push_back(slicefea);
			cout << 0 << ",	";
		}
	}

	// if no zero's (unfeas) in any slice: fingering is feasible => return 1; else return 0
	if (count (slicefeas.begin(), slicefeas.end(), 0)==0){
		feas = 1;
	}
	else {
		feas = 0;
	}
	cout << "\n" << "endfeas: "<<feas << "\n";
	return feas; // 1 als feasible
}
*/
