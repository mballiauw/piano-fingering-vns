/*
 * note.h
 *
 *  Created on: 1-okt.-2013
 *      Author: Matteo
 */

#ifndef NOTE_H_
#define NOTE_H_

class note{


public:
	int pitch;
	int start;
	int duration;
	int id;
	char hand;
	int voice;
	int fingering;
	note(int pitch2, int start2, int duration2, int id2, char hand2, int voice2, int fingering2);
	//int reinit(int pitch2, int start2, int duration2, int id2, char hand2, int voice2; int fingering2);
};



#endif /* NOTE_H_ */
