/*
 * Move2.h
 *
 *  Created on: 28-okt.-2013
 *      Author: Matteo
 */

#ifndef MOVE2_H_
#define MOVE2_H_

#include "score.h"

using namespace std;

class Move2 {
public:
	int firstfinger;
	int secondfinger;
	double diff;
	Move2(int,int,double);
};

// algorithm

	// Move type: SWAP 2 FINGERS: swap throughout 2 fingers = always feasible
	// steapest descend: take the best.
	// once right, once left

	//right
	double rightSwapf (MusicInfo &info, vector<note> &allnotes);
	// left
	double leftSwapf (MusicInfo &info, vector<note> &allnotes);

#endif /* MOVE2_H_ */
