/*
 * importMusicXML.h
 *
 *  Created on: 9-okt.-2013
 *      Author: Matteo
 */
#include "note.h"
#include <vector>
#include <string.h>
#include "xmlParser.h"

#ifndef IMPORTMUSICXML_H_
#define IMPORTMUSICXML_H_

using namespace std;

XMLNode importNode(string filename);
vector<note> importMusicXML(XMLNode &NodeMain);
int aantalmaten(string filename);
int divpermaat(string filename);
int testOnemeasuretype (string filename);

#endif /* IMPORTMUSICXML_H_ */
