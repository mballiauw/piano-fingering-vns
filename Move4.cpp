/*
 * Move4.cpp
 *
 *  Created on: 10-dec.-2013
 *      Author: Matteo
 */

#include "Move4.h"
#include <vector>
#include "Move3.h"
#include "Move.h"
#include "score.h"
#include <limits>


using namespace std;

Move4::Move4 (int a, int b , vector<int> c , int d, double e){
	ID1 = a;
	f = b;
	ID2s = c;
	g = d;
	diff = e;
}

double rightSwapNote(MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure){

	/*//test of tabulijst aanwezig is
	bool tabuactive = false;
	if (tabutenure>0){tabuactive = true;}*/

	bool stop = false; // used for firstdescend

	char hand = 'R';

	vector <int> ID2s; // bevat alle noten b die met g gespeeld worden
	Move4 swaptwo (1,1,ID2s,2,0);

	swaptwo.diff = std::numeric_limits<double>::infinity();
	if (firstdescend == true){
		swaptwo.diff = 0;
	}

	for (unsigned int id = 1; id <= allnotes.size(); id++){ // go through all notes a
		if (allnotes.at(id-1).hand == hand){
			int f = getFingering(id,allnotes); // note a
			int begin = allnotes.at(id-1).start; // note a
			int end = (begin + allnotes.at(id-1).duration - 1); // note a
			int first = findFirst(hand, begin, end, allnotes); // returns first ID in allnotes that is exclusively in the time note a is active
			int last = findLast(hand, begin, end, allnotes); // returns last ID in allnotes that is exclusively in the time note a is active

			// go through all fingers g (1 to 5, excluding f): change all these notes (active in a) to fingering f
			for (int g = 1; g < 6; g++){ // 1 tot 5 afgaan
				if (g == f){continue;} // f en g niet gelijk aan elkaar!
				else {
					vector <note> adaptnotes = allnotes;
					ID2s.clear();
					for(int b = first; b <=last; b++){
						// zoek excl noten tgl met a, slaag id op en wijzig fingering
						if (exclusiveNote(b, begin, end, allnotes)==true && allnotes.at(b-1).hand == hand && allnotes.at(b-1).fingering == g){
							ID2s.push_back(b);
							adaptnotes.at(b-1).fingering = f;
						}
					}
					// wijzig fingering id1 naar g
					adaptnotes.at(id-1).fingering= g;

					//bepaal begin en eindslice voor gedeeltelijke score
					int prevlength = 0;
					int crlength = allnotes.at(id-1).duration;
					int nextlength = 0;

					for (int backskipper = allnotes.at(id-1).start-1; backskipper >= 0; backskipper--){
						if (info.right.at(begin).at(0) != info.right.at(backskipper).at(0) || info.right.at(begin).at(1) != info.right.at(backskipper).at(1) ||
							info.right.at(begin).at(2) != info.right.at(backskipper).at(2) || info.right.at(begin).at(3) != info.right.at(backskipper).at(3) || info.right.at(begin).at(4) != info.right.at(backskipper).at(4)){

							int dur0 = 0;
							int dur1 = 0;
							int dur2 = 0;
							int dur3 = 0;
							int dur4 = 0;

							if (info.right.at(backskipper).at(0) >0) {
								dur0 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
							}
							if (info.right.at(backskipper).at(1) >0) {
								dur1 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
							}
							if (info.right.at(backskipper).at(2) >0) {
								dur2 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
							}
							if (info.right.at(backskipper).at(3) >0) {
								dur3 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
							}
							if (info.right.at(backskipper).at(4) >0) {
								dur4 = allnotes.at((info.right.at(backskipper).at(0)-1)).duration;
							}

							prevlength = maximum(dur0, dur1, dur2, dur3, dur4);

							break;
						}
						else {continue;}
					}

					for (unsigned int skipper = allnotes.at(id-1).start+crlength; skipper < info.right.size(); skipper++){
						if (info.right.at(end).at(0) != info.right.at(skipper).at(0) || info.right.at(end).at(1) != info.right.at(skipper).at(1) ||
							info.right.at(end).at(2) != info.right.at(skipper).at(2) || info.right.at(end).at(3) != info.right.at(skipper).at(3) || info.right.at(end).at(4) != info.right.at(skipper).at(4)){

							int dur0 = 0;
							int dur1 = 0;
							int dur2 = 0;
							int dur3 = 0;
							int dur4 = 0;

							if (info.right.at(skipper).at(0) >0) {
								dur0 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
							}
							if (info.right.at(skipper).at(1) >0) {
								dur1 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
							}
							if (info.right.at(skipper).at(2) >0) {
								dur2 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
							}
							if (info.right.at(skipper).at(3) >0) {
								dur3 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
							}
							if (info.right.at(skipper).at(4) >0) {
								dur4 = allnotes.at((info.right.at(skipper).at(0)-1)).duration;
							}

							nextlength = maximum(dur0, dur1, dur2, dur3, dur4);

							break;
						}
						else {continue;}
					}

					int start_vwd = begin - prevlength - crlength-1; // start die negatief kan worden
					int fragstart = max (0, start_vwd); // beperk tot 0
					int end_vwd = end + crlength + nextlength+1; // einde die voorbij einde kan gaan
					int sizeofright = info.right.size(); int fragend = min(sizeofright , end_vwd);  // beperkt tot einde

					//bepaal verschil in score en speelbaarheid

					double diff = rightScoreP(info,adaptnotes,fragstart, fragend)-rightScoreP(info,allnotes,fragstart, fragend);
					bool possible = rightFeasibility(info, adaptnotes);

					//als tabulijst actief is; test of id erin zit
					bool tabucontains = false;
					//if (tabuactive == true){
						if (itcounttabu < tabulist.at(id-1).at(g-1)){tabucontains = true;}
						// bekijk enkel de lange noot
					//}

					if (tabucontains == false){ // als de move mag volgens de tabulijst
						if (diff < swaptwo.diff && possible == true){ // slaag beste element NB op (wnr verbetering)
							swaptwo.diff = diff;
							swaptwo.f = f;
							swaptwo.g = g;
							swaptwo.ID1 = id;
							swaptwo.ID2s = ID2s;
							//first descending => break wanneer actief
							if (firstdescend == true){
								stop = true;
								break;
							}
						}
					}
				}
			}
		}
		//first descending => break wanneer actief
		if (firstdescend == true && stop == true){break;}
	}

	// execute changes
	if (tabutenure != 0){
	//if (swaptwo.diff < 0) {
		allnotes.at(swaptwo.ID1-1).fingering = swaptwo.g;
		for (unsigned int teller = 0; teller < swaptwo.ID2s.size(); teller++){
			allnotes.at(swaptwo.ID2s.at(teller)-1).fingering = swaptwo.f;
		}
		// update tabu list
		tabulist.at(swaptwo.ID1-1).at(swaptwo.g-1) = itcounttabu + tabutenure;
		// increase counter
		itcounttabu++;
	//}
	}
	else {
		if (swaptwo.diff < 0) {
			allnotes.at(swaptwo.ID1-1).fingering = swaptwo.g;
			for (unsigned int teller = 0; teller < swaptwo.ID2s.size(); teller++){
				allnotes.at(swaptwo.ID2s.at(teller)-1).fingering = swaptwo.f;
			}
			// update tabu list
			tabulist.at(swaptwo.ID1-1).at(swaptwo.g-1) = itcounttabu + tabutenure;
			// increase counter
			itcounttabu++;
		}
	}

	return rightScore(info, allnotes);
}

double leftSwapNote(MusicInfo &info, vector<note> &allnotes, vector <vector <int> > &tabulist, bool firstdescend, int &itcounttabu, int tabutenure){

	/*//test of tabulijst aanwezig is
	bool tabuactive = false;
	if (tabutenure>0){tabuactive = true;}*/

	bool stop = false; // used for firstdescend

	char hand = 'L';

	vector <int> ID2s; // bevat alle noten b die met g gespeeld worden
	Move4 swaptwo (1,1,ID2s,2,0);

	swaptwo.diff = std::numeric_limits<double>::infinity();
	if (firstdescend == true){
		swaptwo.diff = 0;
	}

	for (unsigned int id = 1; id <= allnotes.size(); id++){ // go through all notes a
		if (allnotes.at(id-1).hand == hand){
			int f = getFingering(id,allnotes); // note a
			int begin = allnotes.at(id-1).start; // note a
			int end = (begin + allnotes.at(id-1).duration - 1); // note a
			int first = findFirst(hand, begin, end, allnotes); // returns first ID in allnotes that is exclusively in the time note a is active
			int last = findLast(hand, begin, end, allnotes); // returns last ID in allnotes that is exclusively in the time note a is active

			// go through all fingers g (1 to 5, excluding f): change all these notes (active in a) to fingering f
			for (int g = 1; g < 6; g++){ // 1 tot 5 afgaan
				if (g == f){continue;} // f en g niet gelijk aan elkaar!
				else {
					vector <note> adaptnotes = allnotes;
					ID2s.clear();
					for(int b = first; b <=last; b++){
						// zoek excl noten tgl met a, slaag id op en wijzig fingering
						if (exclusiveNote(b, begin, end, allnotes)==true && allnotes.at(b-1).hand == hand && allnotes.at(b-1).fingering == g){
							ID2s.push_back(b);
							adaptnotes.at(b-1).fingering = f;
						}
					}
					// wijzig fingering id1 naar g
					adaptnotes.at(id-1).fingering= g;

					//bepaal begin en eindslice voor gedeeltelijke score
					int prevlength = 0;
					int crlength = allnotes.at(id-1).duration;
					int nextlength = 0;

					for (int backskipper = allnotes.at(id-1).start-1; backskipper >= 0; backskipper--){
						if (info.left.at(begin).at(0) != info.left.at(backskipper).at(0) || info.left.at(begin).at(1) != info.left.at(backskipper).at(1) ||
							info.left.at(begin).at(2) != info.left.at(backskipper).at(2) || info.left.at(begin).at(3) != info.left.at(backskipper).at(3) || info.left.at(begin).at(4) != info.left.at(backskipper).at(4)){

							int dur0 = 0;
							int dur1 = 0;
							int dur2 = 0;
							int dur3 = 0;
							int dur4 = 0;

							if (info.left.at(backskipper).at(0) >0) {
								dur0 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
							}
							if (info.left.at(backskipper).at(1) >0) {
								dur1 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
							}
							if (info.left.at(backskipper).at(2) >0) {
								dur2 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
							}
							if (info.left.at(backskipper).at(3) >0) {
								dur3 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
							}
							if (info.left.at(backskipper).at(4) >0) {
								dur4 = allnotes.at((info.left.at(backskipper).at(0)-1)).duration;
							}

							prevlength = maximum(dur0, dur1, dur2, dur3, dur4);

							break;
						}
						else {continue;}
					}

					for (unsigned int skipper = allnotes.at(id-1).start+crlength; skipper < info.left.size(); skipper++){
						if (info.left.at(end).at(0) != info.left.at(skipper).at(0) || info.left.at(end).at(1) != info.left.at(skipper).at(1) ||
							info.left.at(end).at(2) != info.left.at(skipper).at(2) || info.left.at(end).at(3) != info.left.at(skipper).at(3) || info.left.at(end).at(4) != info.left.at(skipper).at(4)){

							int dur0 = 0;
							int dur1 = 0;
							int dur2 = 0;
							int dur3 = 0;
							int dur4 = 0;

							if (info.left.at(skipper).at(0) >0) {
								dur0 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
							}
							if (info.left.at(skipper).at(1) >0) {
								dur1 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
							}
							if (info.left.at(skipper).at(2) >0) {
								dur2 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
							}
							if (info.left.at(skipper).at(3) >0) {
								dur3 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
							}
							if (info.left.at(skipper).at(4) >0) {
								dur4 = allnotes.at((info.left.at(skipper).at(0)-1)).duration;
							}

							nextlength = maximum(dur0, dur1, dur2, dur3, dur4);

							break;
						}
						else {continue;}
					}

					int start_vwd = begin - prevlength - crlength-1; // start die negatief kan worden
					int fragstart = max (0, start_vwd); // beperk tot 0
					int end_vwd = end + crlength + nextlength+1; // einde die voorbij einde kan gaan
					int sizeofleft = info.left.size(); int fragend = min(sizeofleft , end_vwd);  // beperkt tot einde

					//bepaal verschil in score en speelbaarheid

					double diff = leftScoreP(info,adaptnotes,fragstart, fragend)-leftScoreP(info,allnotes,fragstart, fragend);
					bool possible = leftFeasibility(info, adaptnotes);

					//als tabulijst actief is; test of id erin zit
					bool tabucontains = false;
					//if (tabuactive == true){
						if (itcounttabu < tabulist.at(id-1).at(g-1)){tabucontains = true;}
					//}

					if (tabucontains == false){ // als de move mag volgens de tabulijst
						if (diff < swaptwo.diff && possible == true){ // slaag beste element NB op (wnr verbetering)
							swaptwo.diff = diff;
							swaptwo.f = f;
							swaptwo.g = g;
							swaptwo.ID1 = id;
							swaptwo.ID2s = ID2s;
							//first descending => break wanneer actief
							if (firstdescend == true){
								stop = true;
								break;
							}
						}
					}
				}
			}
		}
		//first descending => break wanneer actief
		if (firstdescend == true && stop == true){break;}
	}

	// execute changes
	if (tabutenure != 0){
	//if (swaptwo.diff < 0) {
		allnotes.at(swaptwo.ID1-1).fingering = swaptwo.g;
		for (unsigned int teller = 0; teller < swaptwo.ID2s.size(); teller++){
			allnotes.at(swaptwo.ID2s.at(teller)-1).fingering = swaptwo.f;
		}
		// update tabu list
		tabulist.at(swaptwo.ID1-1).at(swaptwo.g-1) = itcounttabu + tabutenure;
		// increase counter
		itcounttabu++;
	//}
	}
	else {
		if (swaptwo.diff < 0) {
			allnotes.at(swaptwo.ID1-1).fingering = swaptwo.g;
			for (unsigned int teller = 0; teller < swaptwo.ID2s.size(); teller++){
				allnotes.at(swaptwo.ID2s.at(teller)-1).fingering = swaptwo.f;
			}
			// update tabu list
			tabulist.at(swaptwo.ID1-1).at(swaptwo.g-1) = itcounttabu + tabutenure;
			// increase counter
			itcounttabu++;
		}
	}

	return leftScore(info, allnotes);
}
