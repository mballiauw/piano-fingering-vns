/*
 * score.h
 *
 *  Created on: 22 Oct 2013
 *      Author: dorien
 */

#ifndef SCORE_H_
#define SCORE_H_

#include <vector>
#include "note.h"
#include "MusicInfo.h"

using namespace std;

/*class score {
public:
	score();
	virtual ~score();

	//definieer hier de functies!!!


};*/

class Notepart {

public:
	int pitch;
	int finger;
};

int comparePitch (const void * a, const void * b);

double rightScore(MusicInfo &info, vector<note> &allnotes);
double leftScore(MusicInfo &info, vector<note> &allnotes);
int getPitch (unsigned int id, vector<note> &name);
int getFingering (unsigned int id, vector<note> &name);
bool rightFeasibility(MusicInfo &info, vector<note> &allnotes);
bool leftFeasibility(MusicInfo &info, vector<note> &allnotes);

double rightScoreP(MusicInfo &info, vector<note> &allnotes, int start, int end);
double leftScoreP(MusicInfo &info, vector<note> &allnotes, int start, int end);

/*
// displays working of feasibility function
bool rightFeasibilityTrace(MusicInfo &info, vector<note> &allnotes);
bool leftFeasibilityTrace(MusicInfo &info, vector<note> &allnotes);*/

#endif /* SCORE_H_ */
