/*
 * MusicInfo.h
 *
 *  Created on: 22 Oct 2013
 *      Author: dorien
 */
#include <vector>
#include <string>
#include <iostream>
#include <string.h>
#include <fstream>

using namespace std;

#ifndef MUSICINFO_H_
#define MUSICINFO_H_

class MusicInfo {

public:
	int MinPrac [5][5];
	int MaxPrac [5][5];
	int MinComf [5][5];
	int MaxComf [5][5];
	int MinRel [5][5];
	int MaxRel [5][5];
	vector<vector<int> > right;
	vector<vector<int> > left;
	int divspm;
};

MusicInfo fingerSpecs();

#endif /* MUSICINFO_H_ */
